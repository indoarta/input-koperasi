
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/static/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/static/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/static/css/stylesheet.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/static/icon/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Le fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo Yii::app()->theme->baseUrl; ?>/static/img/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo Yii::app()->theme->baseUrl; ?>/static/img/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo Yii::app()->theme->baseUrl; ?>/static/img/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="<?php echo Yii::app()->theme->baseUrl; ?>/static/img/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/static/img/favicon.png">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/html5shiv.js"></script>
    <![endif]-->
  </head>

  <body>

    <header class="dark_grey"> <!-- Header start -->
        <a href="#" class="logo_image"><span class="hidden-480">BPWS</span></a>
        <ul class="header_actions pull-left hidden-480 hidden-768">
            <li rel="tooltip" data-placement="bottom" title="Hide/Show main navigation" ><a href="#" class="hide_navigation"><i class="icon-chevron-left"></i></a></li>
            <li rel="tooltip" data-placement="right" title="Change navigation color scheme" class="color_pick navigation_color_pick"><a class="iconic" href="#"><i class="icon-th"></i></a>
                <ul>
                    <li><a class="blue" href="#"></a></li>
                    <li><a class="light_blue" href="#"></a></li>
                    <li><a class="grey" href="#"></a></li>
                    <li><a class="dark_grey" href="#"></a></li>
                    <li><a class="pink" href="#"></a></li>
                    <li><a class="red" href="#"></a></li>
                    <li><a class="orange" href="#"></a></li>
                    <li><a class="yellow" href="#"></a></li>
                    <li><a class="green" href="#"></a></li>
                    <li><a class="dark_green" href="#"></a></li>
                    <li><a class="turq" href="#"></a></li>
                    <li><a class="dark_turq" href="#"></a></li>
                    <li><a class="purple" href="#"></a></li>
                    <li><a class="violet" href="#"></a></li>
                    <li><a class="dark_blue" href="#"></a></li>
                    <li><a class="dark_red" href="#"></a></li>
                    <li><a class="brown" href="#"></a></li>
                    <li><a class="black" href="#"></a></li>
                    <a class="dark_navigation" href="#">Dark navigation</a>
                </ul>
            </li>
        </ul>
        <ul class="header_actions">
            <li rel="tooltip" data-placement="left" title="Header color scheme" class="color_pick header_color_pick hidden-480"><a class="iconic" href="#"><i class="icon-th"></i></a>
                <ul>
                    <li><a class="blue set_color" href="#"></a></li>
                    <li><a class="light_blue set_color" href="#"></a></li>
                    <li><a class="grey set_color" href="#"></a></li>
                    <li><a class="dark_grey set_color" href="#"></a></li>
                    <li><a class="pink set_color" href="#"></a></li>
                    <li><a class="red set_color" href="#"></a></li>
                    <li><a class="orange set_color" href="#"></a></li>
                    <li><a class="yellow set_color" href="#"></a></li>
                    <li><a class="green set_color" href="#"></a></li>
                    <li><a class="dark_green set_color" href="#"></a></li>
                    <li><a class="turq set_color" href="#"></a></li>
                    <li><a class="dark_turq set_color" href="#"></a></li>
                    <li><a class="purple set_color" href="#"></a></li>
                    <li><a class="violet set_color" href="#"></a></li>
                    <li><a class="dark_blue set_color" href="#"></a></li>
                    <li><a class="dark_red set_color" href="#"></a></li>
                    <li><a class="brown set_color" href="#"></a></li>
                    <li><a class="black set_color" href="#"></a></li>
                </ul>
            </li>
            <li rel="tooltip" data-placement="bottom" title="2 new messages" class="messages"><a class="iconic" href="#"><i class="icon-envelope-alt"></i> 2</a>
                <ul class="dropdown-menu pull-right messages_dropdown">
                    <li>
                        <a href="#">
                            <img src="demo/avatar_06.png" alt="">
                            <div class="details">
                                <div class="name">Jane Doe</div>
                                <div class="message">
                                    Lorem ipsum Commodo quis nisi...
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="demo/avatar_05.png" alt="">
                            <div class="details">
                                <div class="name">Jane Doe</div>
                                <div class="message">
                                    Lorem ipsum Commodo quis nisi...
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="demo/avatar_04.png" alt="">
                            <div class="details">
                                <div class="name">Jane Doe</div>
                                <div class="message">
                                    Lorem ipsum Commodo quis nisi...
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="demo/avatar_05.png" alt="">
                            <div class="details">
                                <div class="name">Jane Doe</div>
                                <div class="message">
                                    Lorem ipsum Commodo quis nisi...
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="demo/avatar_06.png" alt="">
                            <div class="details">
                                <div class="name">Jane Doe</div>
                                <div class="message">
                                    Lorem ipsum Commodo quis nisi...
                                </div>
                            </div>
                        </a>
                    </li>
                    <a href="#" class="btn btn-block blue align_left"><span>Messages center</span></a>
                </ul>
            </li>
            <li class="dropdown"><a href="#"><img src="demo/avatar_06.png" alt="User image" class="avatar"> Bernad Delic <i class="icon-angle-down"></i></a>
                <ul>
                    <li><a href="#"><i class="icon-cog"></i> User options</a></li>
                    <li><a href="#"><i class="icon-inbox"></i> Messages</a></li>
                    <li><a href="#"><i class="icon-user"></i> Friends</a></li>
                    <li><a href="#"><i class="icon-remove"></i> Logout</a></li>
                </ul>
            </li>
            <li><a href="#"><i class="icon-signout"></i> <span class="hidden-768 hidden-480">Logout</span></a></li>
            <li class="responsive_menu"><a class="iconic" href="#"><i class="icon-reorder"></i></a></li>
        </ul>
    </header>

    <div id="main_navigation" class="dark_grey"> <!-- Main navigation start -->
        <div class="inner_navigation">
            <ul class="main">
                <li><a href="dashboard.html"><i class="icon-home"></i> Dashboard</a>
                    <ul class="sub_main">
                        <li><a href="index.html">Dashboard</a></li>
                        <li><a href="dashboard_2.html">Dashboard multimedia</a></li>
                        <li><a href="dashboard_3.html">Dashboard no sidebar</a></li>
                        <li><a href="dashboard_relative.html">Dashboard relative header</a></li>
                        <li><a href="dashboard_4.html">Dashboard top navigation</a></li>
                        <li><a href="dashboard_5.html">Dashboard fixed</a></li>
                    </ul>
                </li>
                <li class="active navAct"><a class="expand" id="current" href="#"><i class="icon-reorder"></i> Forms</a>
                    <ul class="sub_main">
                        <li class="active"><a href="forms.html">Form elements</a></li>
                        <li><a href="forms_advanced.html">Forms advanced</a></li>
                        <li><a href="forms_layout.html">Forms layout</a></li>
                        <li><a href="forms_wizard.html">Form wizards</a></li>
                        <li><a href="forms_validation.html">Form validation</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon-tasks"></i> Components</a>
                    <ul class="sub_main">
                        <li><a href="component_navigation.html">Only main navigation</a></li>
                        <li><a href="component_sidebar.html">Only sidebar</a></li>
                        <li><a href="component_top_navigation.html">Only top navigation</a></li>
                        <li><a href="component_full_width.html">Full width page</a></li>
                        <li><a href="blank.html">Blank page</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon-tasks"></i> UI elements</a>
                    <ul class="sub_main">
                        <li><a href="ui_draggable_boxes.html">Drag & drop boxes</a></li>
                        <li><a href="ui_btns.html">btns</a></li>
                        <li><a href="ui_bootstrap_elements.html">Bootstrap elements</a></li>
                        <li><a href="ui_tabs.html">Bootstrap tabs</a></li>
                        <li><a href="ui_sliders.html">UI sliders</a></li>
                        <li><a href="ui_typography.html">UI typography</a></li>
                        <li><a href="ui_gallery.html">Gallery</a></li>
                        <li><a href="ui_file_manager.html">File manager</a></li>
                    </ul>
                </li>
                <li><a href="page_calendar.html"><i class="icon-calendar"></i> Calendar</a></li>
                <li><a href="page_statistics.html"><i class="icon-signal"></i> Charts & graphs</a></li>
                <li><a href="#"><i class="icon-table"></i> Tables</a>
                    <ul class="sub_main">
                        <li><a href="tables_basic.html">Basic tables</a></li>
                        <li><a href="tables_responsive.html">Responsive tables <span class="label label-important">New!</span></a></li>
                        <li><a href="tables_datatables.html">Datatables</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon-warning-sign"></i> Error pages</a>
                    <ul class="sub_main">
                        <li><a href="error_404.html">404</a></li>
                        <li><a href="error_405.html">405</a></li>
                        <li><a href="error_406.html">406</a></li>
                        <li><a href="error_406.html">500</a></li>
                        <li><a href="error_406.html">502</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon-lock"></i> Login pages</a>
                    <ul class="sub_main">
                        <li><a href="login.html">Basic login</a></li>
                        <li><a href="login-dark-bg.html">Login dark background</a></li>
                        <li><a href="login-v4.html">Login blue background</a></li>
                        <li><a href="login-white-bg.html">Login white background</a></li>
                        <li><a href="login-v2.html">Login slide background</a></li>
                        <li><a href="login-v3.html">Login slide background opacity</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon-indent-right"></i> Example pages</a>
                    <ul class="sub_main">
                        <li><a href="page_faq.html">FAQ</a></li>
                        <li><a href="page_invoice.html">Invoice</a></li>
                        <li><a href="page_maps.html">Maps</a></li>
                        <li><a href="page_messages_center.html">Messages center</a></li>
                        <li><a href="page_price_tables.html">Pricing tables</a></li>
                        <li><a href="page_profile.html">User profile</a></li>
                        <li><a href="page_search.html">Serach results</a></li>
                        <li><a href="page_timeline.html">Timeline</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div> 

    <div id="content">
		<div class="top_bar">
            <ul class="breadcrumb">
              <li><a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i></a> <span class="divider">/</span></li>
              <li class="active"><a href="#" onclick="return false">ASDKALS DJAKSJLDKAS</a></li>
            </ul>
        </div>
		
		<div class="inner_content">
			<?php echo $content ?>
		</div>
    </div>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/jquery-1.10.2.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/jquery-ui-1.10.3.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/bootstrap.js"></script>

    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/jquery.collapsible.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/jquery.mousewheel.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/jquery.uniform.min.js"></script>

    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCL6XtCGot7S7cfxnO6tRfeZx9kLQQRMtA&amp;sensor=false"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/jquery.sparkline.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/chosen.jquery.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/jquery.easytabs.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/flot/excanvas.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/flot/jquery.flot.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/flot/jquery.flot.selection.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/flot/jquery.flot.orderBars.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/maps/jquery.vmap.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/maps/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/maps/data/jquery.vmap.sampledata.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/jquery.autosize-min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/charCount.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/jquery.minicolors.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/jquery.tagsinput.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/fullcalendar.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/footable/footable.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/footable/data-generator.js"></script>

    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/bootstrap-timepicker.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/bootstrap-datepicker.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/bootstrap-fileupload.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/jquery.inputmask.bundle.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/jquery.inputmask.bundle.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/library/bootstrap-select.js"></script>

    <script>
        jQuery(document).ready(function($) {
            $('.selectpicker').selectpicker();
            $('.selectpicker2').selectpicker({
                style: 'btn blue'
            });
            $('.selectpicker3').selectpicker({
                style: 'btn red'
            });
            $('.selectpicker4').selectpicker({
                style: 'btn orange'
            });
            $('.selectpicker5').selectpicker({
                style: 'btn turq'
            });
            $('.selectpicker6').selectpicker({
                style: 'btn yellow'
            });
            $('.selectpicker7').selectpicker({
                style: 'btn grey'
            });
            $('.selectpicker8').selectpicker({
                style: 'btn brown'
            });
            $('.selectpicker9').selectpicker({
                style: 'btn turq_dark'
            });

            $('.selectpicker10').selectpicker({
                style: 'btn grey'
            });
            $('.selectpicker11').selectpicker({
                style: 'btn brown'
            });
            $('.selectpicker12').selectpicker({
                style: 'btn turq_dark'
            });
        });
    </script>

    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/flatpoint_core.js"></script>

    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/static/js/forms.js"></script>

  </body>
</html>
