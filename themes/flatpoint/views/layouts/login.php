<!DOCTYPE html><html lang="en">
<head>
<meta charset="utf-8"/>
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/app.v1.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/font.css" type="text/css" cache="false"/>
<!--[if lt IE 9]>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/ie/respond.min.js" cache="false"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/ie/html5.js" cache="false"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/ie/fix.js" cache="false"></script>
<![endif]-->
</head>
<body>
<section id="content" class="m-t-lg wrapper-md animated fadeInUp"><a class="nav-brand" href="index.html"><?php echo Yii::app()->name; ?></a>
<div class="row m-n">
	<div class="col-md-4 col-md-offset-4 m-t-lg">
		<section class="panel"><header class="panel-heading text-center"> Sign in </header>
		<?php echo $content; ?>
		</section>
	</div>
</div>
</section>
<!-- footer -->
<footer id="footer">
<div class="text-center padder clearfix">
	<p>
		<small>Mobile first web app framework base on Bootstrap<br>
		&copy; 2013</small>
	</p>
</div>
</footer>
<!-- / footer -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/css/app.v1.js"></script>
<!-- Bootstrap -->
<!-- app -->
</body>
</html>