<?php 
$this->pageTitle=Yii::app()->name . ' - Login';

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array("class"=>"panel-body"),
)); 
?>
	<div class="form-group">
		<?php echo $form->labelEx($model,'username', array("class"=>"control-label")); ?>
		<?php echo $form->textField($model,'username', array("class"=>"form-control", "placeholder"=>"Username")); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'password', array("class"=>"control-label")); ?>
		<?php echo $form->passwordField($model,'password', array("class"=>"form-control", "placeholder"=>"Password")); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	<div class="checkbox">
		<label><?php echo $form->checkBox($model,'rememberMe'); ?> Keep me logged in </label>
	</div>
	<button type="submit" class="btn btn-info">Sign in</button>
	<div class="line line-dashed"></div>
<?php $this->endWidget(); ?>