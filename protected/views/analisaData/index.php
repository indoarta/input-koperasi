<div class="pnl" id="global" style="display: none">
	

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-table"></i> Analisa Data Global</span>
	</div>
	<div class="mws-panel-body no-padding">
		<form class="mws-form" style="width: 48%;float: left">
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<label class="mws-form-label">Kondisi Keaktifan Koperasi : </label>
					<div class="mws-form-item">
						<input type="button" id="global1" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<label class="mws-form-label">Kondisi Kelembagaan Koperasi : </label>
					<div class="mws-form-item">
						<input type="button" id="global2" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<label class="mws-form-label">Kondisi Keanggotaan Koperasi : </label>
					<div class="mws-form-item">
						<input type="button" id="global3" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<label class="mws-form-label">Struktur Modal : </label>
					<div class="mws-form-item">
						<input type="button" id="global4" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
			</div>
		</form>
		<form class="mws-form" style="width: 48%;float: left;clear: none">
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<label class="mws-form-label">Volume Usaha : </label>
					<div class="mws-form-item">
						<input type="button" id="global5" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<label class="mws-form-label">SHU : </label>
					<div class="mws-form-item">
						<input type="button" id="global6" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<label class="mws-form-label">Sarana Koperasi : </label>
					<div class="mws-form-item">
						<input type="button" id="global7" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<label class="mws-form-label">Semua Chart : </label>
					<div class="mws-form-item">
						<input type="button" id="globalAll" class="btn btn-danger" value="Tampilkan">
					</div>
				</div>
			</div>
		</form>
		<div style="clear: both"></div>
	</div>
</div>
	

<div class="mws-panel grid_4" style="display: none">
	<div class="mws-panel-header">
		<span><i class="icon-table"></i> Laporan</span>
	</div>
	<div class="mws-panel-body no-padding">
		<form class="mws-form">
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<label class="mws-form-label">Jenis Laporan : </label>
					<div class="mws-form-item">
						<select id="jenis_laporan">
							<option value="1">Laporan Tingkat Propinsi</option>
							<option value="2">Laporan Tingkat Kabupaten/Kota</option>
							<option value="3">Laporan Per Koperasi</option>
							<option value="4">Cari Laporan</option>
						</select><br>
						<input type="text" id="cari" style="display: none;width: 100%;margin: 10px 0px;">
						<input type="hidden" id="data_id" style="display: none">
					</div>
				</div>
				<div class="mws-form-row">
					<label class="mws-form-label">Download : </label>
					<div class="mws-form-item">
						<button id="chart" class="btn btn-primary">
							<i class="icon-chart"></i>
							Grafik
						</button>
						<button id="preview" class="btn btn-info">
							<i class="icon-search"></i>
							Pra-tampil
						</button>
						<button id="download" class="btn btn-warning">
							<i class="icon-download"></i>
							Download
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	$("#global1").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=1");
	});
	$("#global2").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=2");
	});
	$("#global3").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=3");
	});
	$("#global4").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=4");
	});
	$("#global5").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=5");
	});
	$("#global6").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=6");
	});
	$("#global7").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=7");
	});
	$("#globalAll").click(function(){
		$("#output").html(
			"<div id='opt1'></div>"+
			"<div id='opt2'></div>"+
			"<div id='opt3'></div>"+
			"<div id='opt4'></div>"+
			"<div id='opt5'></div>"+
			"<div id='opt6'></div>"+
			"<div id='opt7'></div>"
		)
		$("#opt1").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=1");
		$("#opt2").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=2");
		$("#opt3").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=3");
		$("#opt4").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=4");
		$("#opt5").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=5");
		$("#opt6").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=6");
		$("#opt7").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisPertama/?tipe=7");
	});
</script>

<script>
	var currentName = "";
	$(document).ready(function(){
		$("#cari").autocomplete({
			source: "<?php echo Yii::app()->request->baseUrl; ?>/ajax/GetNamaKoperasiAutoComplete/",
			minLength: 1,
			focus: function( event, ui ) {
				$( "#data_id" ).val( ui.item.id );
				currentName = ui.item.value;
				return false;
			},
			select: function( event, ui ) {
				$( "#data_id" ).val(ui.item.id);
				currentName = ui.item.value;
				return false;
			},
			close: function( event, ui ) {
				//jika oke, tampilkan tanggal.
				$("#cari").val(currentName);
			}
		});
	});
	$("#jenis_laporan").change(function(){
		var jenis = $("#jenis_laporan").val();
		if(jenis == 4){
			$("#cari").show();
			$("#data_id").show();
		}else{
			$("#cari").hide();
			$("#data_id").hide();
		}
		
		if(jenis == 1 || jenis == 2){
			$("#chart").show();
		}else{
			$("#chart").hide();
		}
	});
	$("#chart").click(function(){
		var jenis = $("#jenis_laporan").val();
		if(jenis == 1){
			//download laporan propinsi
			$("#output").load("<?php echo Yii::app()->request->baseUrl; ?>/ajax/propinsi");
		}else if(jenis == 2){
			//download laporan kabupaten/kota
			$("#output").load("<?php echo Yii::app()->request->baseUrl; ?>/ajax/kabupaten");
		}
		return false;
	});
	$("#preview").click(function(){
		var jenis = $("#jenis_laporan").val();
		if(jenis == 1){
			//download laporan propinsi
			$("#output").load("<?php echo Yii::app()->request->baseUrl; ?>/laporan/preview/?type="+jenis);
		}else if(jenis == 2){
			//download laporan kabupaten/kota
			$("#output").load("<?php echo Yii::app()->request->baseUrl; ?>/laporan/preview/?type="+jenis);
		}else if(jenis == 3){
			//download laporan per koperasi
			$("#output").load("<?php echo Yii::app()->request->baseUrl; ?>/laporan/preview/?type="+jenis);
		}else if(jenis == 4){
			//download laporan cari laporan
			if($("#data_id").val()!=""){
				$("#output").load("<?php echo Yii::app()->request->baseUrl; ?>/laporan/preview/?type="+jenis+"&data_id="+$("#data_id").val());
			}else{
				alert("Mohon cari data koperasi terlebih dahulu.")
			}
		}
		return false;
	});
	
	$("#download").click(function(){
		var jenis = $("#jenis_laporan").val();
		if(jenis == 1){
			//download laporan propinsi
			window.location = "<?php echo Yii::app()->request->baseUrl; ?>/laporan/laporan1/"
		}else if(jenis == 2){
			//download laporan kabupaten/kota
			window.location = "<?php echo Yii::app()->request->baseUrl; ?>/laporan/laporan2/"
		}else if(jenis == 3){
			//download laporan per koperasi
			window.location = "<?php echo Yii::app()->request->baseUrl; ?>/laporan/laporan3/"
		}else if(jenis == 4){
			//download laporan cari laporan
			if($("#data_id").val()!=""){
				window.location = "<?php echo Yii::app()->request->baseUrl; ?>/laporan/laporan4?data_id="+$("#data_id").val();
			}else{
				alert("Mohon cari data koperasi terlebih dahulu.")
			}
		}
		return false;
	});
</script>

</div>

<div class="pnl" id="persektor" style="display: none">
	<div class="mws-panel grid_8">
		<div class="mws-panel-header">
			<span><i class="icon-table"></i> Pilih Sektor</span>
		</div>
		<div class="mws-panel-body no-padding">
			<form class="mws-form">
				<div class="mws-form-inline">
					<div class="mws-form-row">
						<label class="mws-form-label">Sektor : </label>
						<div class="mws-form-item">
							<select id="sektor_id">
								<?php
								$sektors = Sektor::model()->findAll("jenis = 'sektor'");
								foreach($sektors as $sektor){
									echo "<option value='".$sektor->sektor_id."'>".$sektor->nama."</option>";
								}
								?>
							</select> 
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="mws-panel grid_8">
		<div class="mws-panel-header">
			<span><i class="icon-table"></i> Analisa Per Sektor</span>
		</div>
		<div class="mws-panel-body no-padding">
			<form class="mws-form" style="width: 48%;float: left;clear: none">
				<div class="mws-form-inline">
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Potensi Per Sektor : </label>
							<div class="mws-form-item">
								<input type="button" id="persektor1" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Kondisi Keaktifan Koperasi : </label>
							<div class="mws-form-item">
								<input type="button" id="persektor2" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Kondisi Kelembagaan Koperasi : </label>
							<div class="mws-form-item">
								<input type="button" id="persektor3" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Kondisi Keanggotaan Koperasi : </label>
							<div class="mws-form-item">
								<input type="button" id="persektor4" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Struktur Modal : </label>
							<div class="mws-form-item">
								<input type="button" id="persektor5" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
				</div>
			</form>
			<form class="mws-form" style="width: 48%;float: left;clear: none">
				<div class="mws-form-inline">
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Volume Usaha : </label>
							<div class="mws-form-item">
								<input type="button" id="persektor6" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">SHU : </label>
							<div class="mws-form-item">
								<input type="button" id="persektor7" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Sarana Koperasi : </label>
							<div class="mws-form-item">
								<input type="button" id="persektor8" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Sarana Sektor : </label>
							<div class="mws-form-item">
								<input type="button" id="persektor9" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Semua Chart : </label>
							<div class="mws-form-item">
								<input type="button" id="persektorAll" class="btn btn-danger" value="Tampilkan">
							</div>
						</div>
					</div>
				</div>
			</form>
				<div style="clear: both"></div>
		</div>
	</div>
	<script>
		$("#persektor1").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=1&sektor_id="+$("#sektor_id").val());
		});
		$("#persektor2").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=2&sektor_id="+$("#sektor_id").val());
		});
		$("#persektor3").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=3&sektor_id="+$("#sektor_id").val());
		});
		$("#persektor4").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=4&sektor_id="+$("#sektor_id").val());
		});
		$("#persektor5").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=5&sektor_id="+$("#sektor_id").val());
		});
		$("#persektor6").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=6&sektor_id="+$("#sektor_id").val());
		});
		$("#persektor7").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=7&sektor_id="+$("#sektor_id").val());
		});
		$("#persektor8").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=8&sektor_id="+$("#sektor_id").val());
		});
		$("#persektor9").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=9&sektor_id="+$("#sektor_id").val());
		});
		$("#persektorAll").click(function(){
			$("#output").html(
				"<div id='opt1'></div>"+
				"<div id='opt2'></div>"+
				"<div id='opt3'></div>"+
				"<div id='opt4'></div>"+
				"<div id='opt5'></div>"+
				"<div id='opt6'></div>"+
				"<div id='opt7'></div>"+
				"<div id='opt8'></div>"+
				"<div id='opt9'></div>"
			)
			$("#opt1").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=1&sektor_id="+$("#sektor_id").val());
			$("#opt2").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=2&sektor_id="+$("#sektor_id").val());
			$("#opt3").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=3&sektor_id="+$("#sektor_id").val());
			$("#opt4").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=4&sektor_id="+$("#sektor_id").val());
			$("#opt5").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=5&sektor_id="+$("#sektor_id").val());
			$("#opt6").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=6&sektor_id="+$("#sektor_id").val());
			$("#opt7").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=7&sektor_id="+$("#sektor_id").val());
			$("#opt8").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=8&sektor_id="+$("#sektor_id").val());
			$("#opt9").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKedua/?tipe=9&sektor_id="+$("#sektor_id").val());
		});
	</script>
</div>

<div class="pnl" id="seluruhsektor" style="display: none">
	<div class="mws-panel grid_8">
		<div class="mws-panel-header">
			<span><i class="icon-table"></i> Pilih Kabupaten</span>
		</div>
		<div class="mws-panel-body no-padding">
			<form class="mws-form">
				<div class="mws-form-inline">
					<div class="mws-form-row">
						<label class="mws-form-label">Kabupaten : </label>
						<div class="mws-form-item">
							<select id="kabupaten_id">
								<?php
								$kabs = Kabupaten::model()->findAll("1 ORDER BY nama");
								foreach($kabs as $sektor){
									echo "<option value='".$sektor->kabupaten_id."'>".$sektor->nama."</option>";
								}
								?>
							</select> 
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="mws-panel grid_8">
		<div class="mws-panel-header">
			<span><i class="icon-table"></i> Analisa Data Per Kabupaten</span>
		</div>
		<div class="mws-panel-body no-padding">
			<form class="mws-form" style="width: 48%;float: left;clear: none">
				<div class="mws-form-inline">
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Potensi Per Kabupaten : </label>
							<div class="mws-form-item">
								<input type="button" id="perkab1" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Kondisi Keaktifan Koperasi : </label>
							<div class="mws-form-item">
								<input type="button" id="perkab2" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Kondisi Kelembagaan Koperasi : </label>
							<div class="mws-form-item">
								<input type="button" id="perkab3" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Kondisi Keanggotaan Koperasi : </label>
							<div class="mws-form-item">
								<input type="button" id="perkab4" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Struktur Modal : </label>
							<div class="mws-form-item">
								<input type="button" id="perkab5" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
				</div>
			</form>
			<form class="mws-form" style="width: 48%;float: left;clear: none">
				<div class="mws-form-inline">
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Volume Usaha : </label>
							<div class="mws-form-item">
								<input type="button" id="perkab6" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">SHU : </label>
							<div class="mws-form-item">
								<input type="button" id="perkab7" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Sarana Koperasi : </label>
							<div class="mws-form-item">
								<input type="button" id="perkab8" class="btn btn-primary" value="Tampilkan">
							</div>
						</div>
					</div>
					<div class="mws-form-inline">
						<div class="mws-form-row">
							<label class="mws-form-label">Semua Chart : </label>
							<div class="mws-form-item">
								<input type="button" id="perkabAll" class="btn btn-danger" value="Tampilkan">
							</div>
						</div>
					</div>
				</div>
			</form>
			<div style="clear: both"></div>
		</div>
	</div>
	<script>
		$("#perkab1").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=1&kabupaten_id="+$("#kabupaten_id").val());
		});
		$("#perkab2").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=2&kabupaten_id="+$("#kabupaten_id").val());
		});
		$("#perkab3").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=3&kabupaten_id="+$("#kabupaten_id").val());
		});
		$("#perkab4").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=4&kabupaten_id="+$("#kabupaten_id").val());
		});
		$("#perkab5").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=5&kabupaten_id="+$("#kabupaten_id").val());
		});
		$("#perkab6").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=6&kabupaten_id="+$("#kabupaten_id").val());
		});
		$("#perkab7").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=7&kabupaten_id="+$("#kabupaten_id").val());
		});
		$("#perkab8").click(function(){
			$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=8&kabupaten_id="+$("#kabupaten_id").val());
		});
		
		$("#perkabAll").click(function(){
			$("#output").html(
				"<div id='opt1'></div>"+
				"<div id='opt2'></div>"+
				"<div id='opt3'></div>"+
				"<div id='opt4'></div>"+
				"<div id='opt5'></div>"+
				"<div id='opt6'></div>"+
				"<div id='opt7'></div>"+
				"<div id='opt8'></div>"
			)
			$("#opt1").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=1&kabupaten_id="+$("#kabupaten_id").val());
			$("#opt2").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=2&kabupaten_id="+$("#kabupaten_id").val());
			$("#opt3").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=3&kabupaten_id="+$("#kabupaten_id").val());
			$("#opt4").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=4&kabupaten_id="+$("#kabupaten_id").val());
			$("#opt5").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=5&kabupaten_id="+$("#kabupaten_id").val());
			$("#opt6").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=6&kabupaten_id="+$("#kabupaten_id").val());
			$("#opt7").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=7&kabupaten_id="+$("#kabupaten_id").val());
			$("#opt8").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jenisKetiga/?tipe=8&kabupaten_id="+$("#kabupaten_id").val());
		});
	</script>
</div>

<div id="output"></div>

<script>
	var lokasi = window.location.toString().split("#");
	$("#"+lokasi[1]).show();
	
	$(".submenu").click(function(){
		$(".pnl").hide();
		var data = $(this).attr("href").split("#")[1];
		$("#"+data).show();
		$("#output").html("");
	})
	
	$("#showPerSector").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/persektor?sektor_id="+$("#sektor_id").val())
	});
	$("#showAktifPasif").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/aktifpasif?sektor_id="+$("#sektor_id").val())
	});
	$("#showStrukturModal").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/strukturModal")
	});
	$("#showJumlahAnggota").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jumlahanggota")
	});
	$("#showVol").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/volusaha")
	});
	$("#showSHU").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/shu")
	});
</script>