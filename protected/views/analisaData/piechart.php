<?php
$uniqueID = substr(md5(rand(0, 10000)), 0, 10);
?>
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-graph"></i> <?php echo $title; ?></span>
	</div>
	<div class="mws-panel-body">
		<div id="chart1<?php echo $uniqueID; ?>" style="height: 222px;"></div>
		<?php if(isset($data2)){ ?>
		<div id="chart2<?php echo $uniqueID; ?>" style="height: 222px;"></div>
		<?php }
		if(isset($data3)){ ?>
		<div id="chart3<?php echo $uniqueID; ?>" style="height: 222px;"></div>
		<?php } ?>
		<?php 
		if(count($data) == 0){
			echo "Data Kosong.";
		}
		?>
		<div style="clear: both"></div>
		<?php if(isset($data2) || isset($data3)){ ?>
		<h5><?php echo $judul; ?></h5>
		<?php
		}
		$this->showTable($data, $satuan, !isset($title)?$judul:$title);
		?>
		<?php if(isset($data2)){ ?>
		<h5><?php echo $judul2; ?></h5>
		<?php
		$this->showTable($data2, $satuan, $judul2);
		?>
		<?php }
		if(isset($data3)){ ?>
		<h5><?php echo $judul3; ?></h5>
		<?php
		$this->showTable($data3, $satuan, $judul3);
		?>
		<?php } ?>
	</div>
</div>

<div>
	<img id="gambar" src="" />
</div>

<script>
	Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
		var n = this,
		decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
		decSeparator = decSeparator == undefined ? "." : decSeparator,
		thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
		sign = n < 0 ? "-" : "",
		i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
		return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
	};
	
	function pieHover(event, pos, obj) 
	{
		if (!obj)
			return;
		percent = parseFloat(obj.series.percent).toFixed(2);
		$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
	}

	function pieClick(event, pos, obj) 
	{
		if (!obj)
			return;
		var angka = obj.series.data[0][1];
		var satuan = "<?php echo $satuan; ?>";
		if(satuan == "Rupiah"){
			angka = angka.formatMoney(0,'.',',');
			alert(obj.series.label + " : Rp " + angka + ",-");
		}else{
			alert(obj.series.label + " : " + angka + " " + satuan);
		}
	}

	var jumlah = 1;
	
	$(document).ready(function() {
		if( $.plot ) {
			var data = [
				<?php
				$output = array();
				foreach ($data as $k => $v) {
					$output[] = "{ label: \"" . $k . "\",  data: " . $v . "}";
				}
				echo implode(",\n", $output);
				?>
			];

			var plot1 = $.plot($("#chart1<?php echo $uniqueID; ?>"), data,
			{
				series: {
					pie: { 
						show: true,
						radius: 1,
						label: {
							show: true,
							radius: 1,
							formatter: function(label, series){
								return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'+label+'<br/>'+Math.round(series.percent)+'%</div>';
							},
							background: { opacity: 0.8 }
						}
					}
				},
				grid: {
					hoverable: true,
					clickable: true
				}
			});
			$("#chart1<?php echo $uniqueID; ?>").bind("plothover", pieHover);
			$("#chart1<?php echo $uniqueID; ?>").bind("plotclick", pieClick);
			
			<?php if(isset($data2)){ ?>
			var data2 = [
				<?php
				$output = array();
				foreach ($data2 as $k => $v) {
					$output[] = "{ label: \"" . $k . "\",  data: " . $v . "}";
				}
				echo implode(",\n", $output);
				?>
			];

			$.plot($("#chart2<?php echo $uniqueID; ?>"), data2,
			{
				series: {
					pie: { 
						show: true,
						radius: 1,
						label: {
							show: true,
							radius: 1,
							formatter: function(label, series){
								return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'+label+'<br/>'+Math.round(series.percent)+'%</div>';
							},
							background: { opacity: 0.8 }
						}
					}
				},
				grid: {
					hoverable: true,
					clickable: true
				}
			});
			$("#chart2<?php echo $uniqueID; ?>").bind("plothover", pieHover);
			$("#chart2<?php echo $uniqueID; ?>").bind("plotclick", pieClick);
			
			jumlah ++;
			
			<?php } ?>

			<?php if(isset($data3)){ ?>
			var data3 = [
				<?php
				$output = array();
				foreach ($data3 as $k => $v) {
					$output[] = "{ label: \"" . $k . "\",  data: " . $v . "}";
				}
				echo implode(",\n", $output);
				?>
			];

			$.plot($("#chart3<?php echo $uniqueID; ?>"), data3,
			{
				series: {
					pie: { 
						show: true,
						radius: 1,
						label: {
							show: true,
							radius: 1,
							formatter: function(label, series){
								return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'+label+'<br/>'+Math.round(series.percent)+'%</div>';
							},
							background: { opacity: 0.8 }
						}
					}
				},
				grid: {
					hoverable: true,
					clickable: true
				}
			});
			$("#chart3<?php echo $uniqueID; ?>").bind("plothover", pieHover);
			$("#chart3<?php echo $uniqueID; ?>").bind("plotclick", pieClick);
			
			jumlah ++;
			
			<?php } ?>

			if(jumlah == 2){
				$("#chart1<?php echo $uniqueID; ?>").css("width", "50%");
				$("#chart2<?php echo $uniqueID; ?>").css("width", "50%");
				$("#chart1<?php echo $uniqueID; ?>").css("float", "left");
				$("#chart2<?php echo $uniqueID; ?>").css("float", "left");
			}else if(jumlah == 3){
				$("#chart1<?php echo $uniqueID; ?>").css("width", "33%");
				$("#chart2<?php echo $uniqueID; ?>").css("width", "33%");
				$("#chart3<?php echo $uniqueID; ?>").css("width", "33%");
				$("#chart1<?php echo $uniqueID; ?>").css("float", "left");
				$("#chart2<?php echo $uniqueID; ?>").css("float", "left");
				$("#chart3<?php echo $uniqueID; ?>").css("float", "left");
			}
		}
	});
</script>