<?php 
$data['values'] = array(55,80,46,71,95);
$data['fillcolor']='lightblue';
$datas=array($data);
$datalabel=array("Jan","Feb","Mar","Apr","May");

//comment the image name to show on page
$image_name=Yii::app()->basePath."/../graphimage/jpgraph-radar-example5.jpg";
$this->widget("ext.jpgraphWidget.jpgraphWidget",array(
    'type'=>'radar',
    'width'=>250,
    'height'=>250,   
    'data'=>$datas,
    'datalabel'=>$datalabel,      
    //'saveas'=>$image_name,//comment to show on page
    'properties'=>array( 
            //'graph_color'=>'',                       
            'grid_linestyle'=>'dotted',
            'grid_color'=>'darkred',
        //    'grid_size'=>'0.6',
            'axis_color'=>'red',
            'axis_size'=>'0.6',
            ),                                            
));
?>