<style>
	#chartpersektor .flot-x-axis .tickLabel{
		-o-transform:rotate(-90deg);
		-moz-transform: rotate(-90deg);
		-webkit-transform:rotate(-90deg);
		 filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
		 width: 200px !important;
		 max-width: 200px !important;
		 text-align: right !important;
		 top: 292px !important;
	}
</style>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-graph"></i> Chart Perbandingan Struktur Modal</span>
	</div>
	<div class="mws-panel-body">
		<div id="chartpersektor" style="height: 222px;"></div>
	</div>
</div>

<script>
	<?php
	$jumlahSendiri = array();
	$jumlahLuar = array();
	$kabupatenArray = array();
	$kabs = Kabupaten::model()->findAll();
	foreach($kabs as $kab){
		$kabupatenArray[$kab->kabupaten_id] = $kab->nama;
		$jumlahSendiri[$kab->kabupaten_id] = 0;
		$jumlahLuar[$kab->kabupaten_id] = 0;
	}
	
	$dataUtamas = DataUtama::model()->findAll();
	foreach($dataUtamas as $du){
		$jmlSendiri = $du->ms_simpanan_pokok + $du->ms_simpanan_wajib + 
			$du->ms_bantuan_hibah + $du->ms_bantuan_sosial + $du->ms_cadangan;
		$jmlLuar = $du->ml_pinjaman_bank + $du->ml_lemb_non_bank + $du->ml_pinjaman_pihak_iii;
		$jumlahSendiri[$du->kabupaten_id] += $jmlSendiri;
		$jumlahLuar[$du->kabupaten_id] += $jmlLuar;
	}
	
	
	$output = array();
	foreach($jumlahSendiri as $key => $val){
		$output[] = '["'.$kabupatenArray[$key].'", '.$val.']';
		//$output[] = '["'.$key.'", '.$val.']';
	}
	
	$output2 = array();
	foreach($jumlahLuar as $key => $val){
		$output2[] = '["'.$kabupatenArray[$key].'", '.$val.']';
		//$output2[] = '["'.$key.'", '.$val.']';
	}
	
	?>
		
	$(document).ready(function() {
		if( $.plot ) {
			var data = [ 
				<?php
				echo implode(", ", $output);
				?> 
			];
			
			var data2 = [ 
				<?php
				echo implode(", ", $output2);
				?> 
			];

			$.plot("#chartpersektor", 
			[ 
				{
					data: data,
					label: "Modal Sendiri",
					color: "#c75d7b"
				}, {
					data: data2,
					label: "Modal Luar",
					color: "#c5d52b"
				}
			], 
			{
				series: {
					stack: true,
					bars: {
						show: true,
						barWidth: 0.6,
						align: "center"
					}
					//,color: "#C20288"
				},
				
				xaxis: {
					mode: "categories",
					tickLength: 0,
					labelWidth: 30
				},
				tooltip: true,
				tooltipOpts: {
					content: "Jumlah: %y unit"
				},
                grid: {
                    hoverable: true,
                    borderWidth: 0
                }
			});
        }
    });
</script>