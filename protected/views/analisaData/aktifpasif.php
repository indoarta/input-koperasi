<style>
	#chartpersektor .flot-x-axis .tickLabel{
		-o-transform:rotate(-90deg);
		-moz-transform: rotate(-90deg);
		-webkit-transform:rotate(-90deg);
		 filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
		 width: 200px !important;
		 max-width: 200px !important;
		 text-align: right !important;
		 top: 292px !important;
	}
</style>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-graph"></i> Chart Perbandingan Jumlah Koperasi Aktif/Pasif</span>
	</div>
	<div class="mws-panel-body">
		<div id="chartpersektor" style="height: 222px;"></div>
	</div>
</div>

<script>
	<?php
	//aktif
	$list = Yii::app()->db->createCommand("
		select du.kabupaten_id, ka.nama, count(*) as total
		from data_utama du, kabupaten ka
		where 
		ka.kabupaten_id = du.kabupaten_id
		AND du.kondisi_koperasi = '1'
		group by du.kabupaten_id")->queryAll();

	$kabOutput = array();
	$kabs = Kabupaten::model()->findAll();
	foreach($kabs as $kab){
		$kabOutput[$kab->nama] = "0";
	}
	
	foreach($list as $item){
		$kabOutput[$item['nama']] = $item["total"];
	}
	
	$outputPerKab = array();
	foreach($kabOutput as $key => $val){
		$outputPerKab[] = '["'.$key.'", '.$val.']';
	}
	
	//Pasif
	$list = Yii::app()->db->createCommand("
		select du.kabupaten_id, ka.nama, count(*) as total
		from data_utama du, kabupaten ka
		where 
		ka.kabupaten_id = du.kabupaten_id
		AND du.kondisi_koperasi = '0'
		group by du.kabupaten_id")->queryAll();

	$kabOutput = array();
	$kabs = Kabupaten::model()->findAll();
	foreach($kabs as $kab){
		$kabOutput[$kab->nama] = "0";
	}
	
	foreach($list as $item){
		$kabOutput[$item['nama']] = $item["total"];
	}
	
	$outputPerKab2 = array();
	foreach($kabOutput as $key => $val){
		$outputPerKab2[] = '["'.$key.'", '.$val.']';
	}
	
	?>
		
	$(document).ready(function() {
		if( $.plot ) {
			var data = [ 
				<?php
				echo implode(", ", $outputPerKab);
				?> 
			];
			
			var data2 = [ 
				<?php
				echo implode(", ", $outputPerKab2);
				?> 
			];

			$.plot("#chartpersektor", [ 
					{
						data: data,
						label: "Aktif",
						color: "#c75d7b"
					}, {
						data: data2,
						label: "Pasif",
						color: "#c5d52b"
					}
				], 
			{
				stack: 0,
				series: {
					bars: {
						show: true,
						barWidth: 0.6,
						align: "center"
					}
					//,color: "#C20288"
				},
				
				xaxis: {
					mode: "categories",
					tickLength: 0,
					labelWidth: 30
				},
				tooltip: true,
				tooltipOpts: {
					content: "Jumlah: %y unit"
				},
                grid: {
                    hoverable: true,
                    borderWidth: 0
                }
			});
        }
    });
</script>