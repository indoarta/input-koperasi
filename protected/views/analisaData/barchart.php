<?php
$uniqueID = substr(md5(rand(0, 10000)), 0, 10);
if($rotate == TRUE){
?>
<style>
	#chartpersektor<?php echo $uniqueID; ?> .flot-x-axis .tickLabel{
		-o-transform:rotate(-90deg);
		-moz-transform: rotate(-90deg);
		-webkit-transform:rotate(-90deg);
		 filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
		 width: 200px !important;
		 max-width: 200px !important;
		 text-align: right !important;
		 top: 292px !important;
	}
</style>
<?php
}
?>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-graph"></i> <?php echo $title; ?></span>
	</div>
	<div class="mws-panel-body">
		<div id="chartpersektor<?php echo $uniqueID; ?>" style="height: 222px;margin-bottom: 50px"></div>
		<?php
		$this->showTable($data, $satuan, $title);
		?>
	</div>
</div>

<script>
	Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
		var n = this,
		decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
		decSeparator = decSeparator == undefined ? "." : decSeparator,
		thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
		sign = n < 0 ? "-" : "",
		i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
		return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
	};
	
	function pieHover(event, pos, obj) 
	{
		if (!obj)
			return;
		percent = parseFloat(obj.series.percent).toFixed(2);
		$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
	}

	function pieClick(event, pos, obj) 
	{
		if (!obj)
			return;
		percent = parseFloat(obj.series.percent).toFixed(2);
		alert(''+obj.series.label+': '+percent+'%');
	}

	
	$(document).ready(function() {
		if( $.plot ) {
			var data = [
<?php
$output = array();
foreach ($data as $k => $v) {
	$output[] = '["'.$k.'", '. $v.']';
}
echo implode(",\n", $output);
?>
			];

			$.plot("#chartpersektor<?php echo $uniqueID; ?>", 
			[ 
				{
					data: data,
					label: "<?php echo $title; ?>",
					color: "#c75d7b"
				}
			], 
			{
				series: {
					stack: true,
					bars: {
						show: true,
						barWidth: 0.6,
						align: "center"
					}
					//,color: "#C20288"
				},
				
				xaxis: {
					mode: "categories",
					tickLength: 0,
					labelWidth: 30
				},
				tooltip: true,
				tooltipOpts: {
					content: function(label, xval, yval) {
						var content = "";
						if("<?php echo $satuan; ?>" == "Rupiah"){
							yval = yval.formatMoney(0,'.',',');
							content = "Jumlah : Rp " + yval + ",-";
						}else{
							content = "Jumlah : " + yval + " <?php echo $satuan; ?>";
						}
						return content;
					}
						//"Jumlah: %y <?php echo $satuan; ?>"
				},
                grid: {
                    hoverable: true,
                    borderWidth: 0
                }
			});
			$("#chartpersektor<?php echo $uniqueID; ?>").bind("plothover", pieHover);
			$("#chartpersektor<?php echo $uniqueID; ?>").bind("plotclick", pieClick);

		}
	});
</script>