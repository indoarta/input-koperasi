<style>
	#chartpersektor .flot-x-axis .tickLabel{
		-o-transform:rotate(-90deg);
		-moz-transform: rotate(-90deg);
		-webkit-transform:rotate(-90deg);
		 filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
		 width: 200px !important;
		 max-width: 200px !important;
		 text-align: right !important;
		 top: 292px !important;
	}
</style>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-graph"></i> Chart Per Sektor</span>
	</div>
	<div class="mws-panel-body">
		<div id="chartpersektor" style="height: 222px;"></div>
	</div>
</div>

<script>
	<?php
	$list = Yii::app()->db->createCommand("
		select du.kabupaten_id, ka.nama, sum(su.jumlah_unit) as total
		from sarana_usaha su, data_utama du, sarana sa, sektor se, kabupaten ka
		where su.data_id = du.data_id AND su.sarana_id = sa.sarana_id AND sa.sektor_id = se.sektor_id AND se.sektor_id = '".$sektor_id."'
		AND ka.kabupaten_id = du.kabupaten_id
		group by du.kabupaten_id")->queryAll();

	$kabOutput = array();
	$kabs = Kabupaten::model()->findAll();
	foreach($kabs as $kab){
		$kabOutput[$kab->nama] = "0";
	}
	
	foreach($list as $item){
		$kabOutput[$item['nama']] = $item["total"]*1;
	}
	
	$outputPerKab = array();
	foreach($kabOutput as $key => $val){
		$outputPerKab[] = '["'.$key.'", '.$val.']';
	}
	?>
		
	$(document).ready(function() {
		if( $.plot ) {
			var data = [ 
				<?php
				echo implode(", ", $outputPerKab);
				?> 
			];

			$.plot("#chartpersektor", [ data ], {
				series: {
					bars: {
						show: true,
						barWidth: 0.6,
						align: "center"
					}
					,color: "#C20288"
				},
				
				xaxis: {
					mode: "categories",
					tickLength: 0,
					labelWidth: 30
				},
				tooltip: true,
				tooltipOpts: {
					content: "Jumlah: %y unit"
				},
                grid: {
                    hoverable: true,
                    borderWidth: 0
                }
			});
        }
    });
</script>