
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-table"></i> Analisa Data</span>
	</div>
	<div class="mws-panel-body no-padding">
		<form class="mws-form">
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<label class="mws-form-label">Persebaran Per Sektor : </label>
					<div class="mws-form-item">
						<select id="sektor_id">
							<?php
							$sektors = Sektor::model()->findAll("jenis = 'sektor'");
							foreach($sektors as $sektor){
								echo "<option value='".$sektor->sektor_id."'>".$sektor->nama."</option>";
							}
							?>
						</select> 
						<input type="button" id="showPerSector" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
				<div class="mws-form-row">
					<label class="mws-form-label">Persebaran Aktif/Pasif : </label>
					<div class="mws-form-item">
						<input type="button" id="showAktifPasif" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
				<div class="mws-form-row">
					<label class="mws-form-label">Struktur Modal : </label>
					<div class="mws-form-item">
						<input type="button" id="showStrukturModal" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
				<div class="mws-form-row">
					<label class="mws-form-label">Jumlah Anggota : </label>
					<div class="mws-form-item">
						<input type="button" id="showJumlahAnggota" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
				<div class="mws-form-row">
					<label class="mws-form-label">Volume Usaha (s/d Des 2012) : </label>
					<div class="mws-form-item">
						<input type="button" id="showVol" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
				<div class="mws-form-row">
					<label class="mws-form-label">SHU (s/d Des 2012) : </label>
					<div class="mws-form-item">
						<input type="button" id="showSHU" class="btn btn-primary" value="Tampilkan">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div id="output"></div>

<script>
	$("#showPerSector").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/persektor?sektor_id="+$("#sektor_id").val())
	});
	$("#showAktifPasif").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/aktifpasif?sektor_id="+$("#sektor_id").val())
	});
	$("#showStrukturModal").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/strukturModal")
	});
	$("#showJumlahAnggota").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/jumlahanggota")
	});
	$("#showVol").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/volusaha")
	});
	$("#showSHU").click(function(){
		$("#output").load("<?php echo Yii::app()->request->baseUrl ?>/analisaData/shu")
	});
</script>