<style>
	#chartpersektor .flot-x-axis .tickLabel{
		-o-transform:rotate(-90deg);
		-moz-transform: rotate(-90deg);
		-webkit-transform:rotate(-90deg);
		 filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
		 width: 200px !important;
		 max-width: 200px !important;
		 text-align: right !important;
		 top: 292px !important;
	}
</style>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-graph"></i> Chart Perbandingan Jumlah Anggota</span>
	</div>
	<div class="mws-panel-body">
		<div id="chartpersektor" style="height: 222px;"></div>
	</div>
</div>

<script>
	<?php
	$jumlahAnggota = array();
	$kabupatenArray = array();
	$kabs = Kabupaten::model()->findAll();
	foreach($kabs as $kab){
		$kabupatenArray[$kab->kabupaten_id] = $kab->nama;
		$jumlahAnggota[$kab->kabupaten_id] = 0;
	}
	
	$dataUtamas = DataUtama::model()->findAll();
	foreach($dataUtamas as $du){
		$jumlahAnggota[$du->kabupaten_id] += $du->jumlah_anggota;
	}
	
	
	$output = array();
	foreach($jumlahAnggota as $key => $val){
		$output[] = '["'.$kabupatenArray[$key].'", '.$val.']';
	}
	
	?>
		
	$(document).ready(function() {
		if( $.plot ) {
			var data = [ 
				<?php
				echo implode(", ", $output);
				?> 
			];
			

			$.plot("#chartpersektor", 
			[ 
				{
					data: data,
					label: "Jumlah Anggota",
					color: "#c75d7b"
				}
			], 
			{
				series: {
					stack: true,
					bars: {
						show: true,
						barWidth: 0.6,
						align: "center"
					}
					//,color: "#C20288"
				},
				
				xaxis: {
					mode: "categories",
					tickLength: 0,
					labelWidth: 30
				},
				tooltip: true,
				tooltipOpts: {
					content: "Jumlah: %y orang"
				},
                grid: {
                    hoverable: true,
                    borderWidth: 0
                }
			});
        }
    });
</script>