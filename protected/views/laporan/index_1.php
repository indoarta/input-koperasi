
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-table"></i> Laporan</span>
	</div>
	<div class="mws-panel-body no-padding">
		<form class="mws-form">
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<label class="mws-form-label">Jenis Laporan : </label>
					<div class="mws-form-item">
						<select id="jenis_laporan">
							<option value="1">Laporan Tingkat Propinsi</option>
							<option value="2">Laporan Tingkat Kabupaten/Kota</option>
							<option value="3">Laporan Per Koperasi</option>
							<option value="4">Cari Laporan</option>
						</select><br>
						<input type="text" id="cari" style="display: none;width: 100%;margin: 10px 0px;">
						<input type="hidden" id="data_id" style="display: none">
					</div>
				</div>
				<div class="mws-form-row">
					<label class="mws-form-label">Download : </label>
					<div class="mws-form-item">
						<button id="chart" class="btn btn-primary">
							<i class="icon-chart"></i>
							Grafik
						</button>
						<button id="preview" class="btn btn-info">
							<i class="icon-search"></i>
							Pra-tampil
						</button>
						<button id="download" class="btn btn-warning">
							<i class="icon-download"></i>
							Download
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div id="output"></div>

<script>
	var currentName = "";
	$(document).ready(function(){
		$("#cari").autocomplete({
			source: "<?php echo Yii::app()->request->baseUrl; ?>/ajax/GetNamaKoperasiAutoComplete/",
			minLength: 1,
			focus: function( event, ui ) {
				$( "#data_id" ).val( ui.item.id );
				currentName = ui.item.value;
				return false;
			},
			select: function( event, ui ) {
				$( "#data_id" ).val(ui.item.id);
				currentName = ui.item.value;
				return false;
			},
			close: function( event, ui ) {
				//jika oke, tampilkan tanggal.
				$("#cari").val(currentName);
			}
		});
	});
	$("#jenis_laporan").change(function(){
		var jenis = $("#jenis_laporan").val();
		if(jenis == 4){
			$("#cari").show();
			$("#data_id").show();
		}else{
			$("#cari").hide();
			$("#data_id").hide();
		}
		
		if(jenis == 1 || jenis == 2){
			$("#chart").show();
		}else{
			$("#chart").hide();
		}
	});
	$("#chart").click(function(){
		var jenis = $("#jenis_laporan").val();
		if(jenis == 1){
			//download laporan propinsi
			$("#output").load("<?php echo Yii::app()->request->baseUrl; ?>/ajax/propinsi");
		}else if(jenis == 2){
			//download laporan kabupaten/kota
			$("#output").load("<?php echo Yii::app()->request->baseUrl; ?>/ajax/kabupaten");
		}
		return false;
	});
	$("#preview").click(function(){
		var jenis = $("#jenis_laporan").val();
		if(jenis == 1){
			//download laporan propinsi
			$("#output").load("<?php echo Yii::app()->request->baseUrl; ?>/laporan/preview/?type="+jenis);
		}else if(jenis == 2){
			//download laporan kabupaten/kota
			$("#output").load("<?php echo Yii::app()->request->baseUrl; ?>/laporan/preview/?type="+jenis);
		}else if(jenis == 3){
			//download laporan per koperasi
			$("#output").load("<?php echo Yii::app()->request->baseUrl; ?>/laporan/preview/?type="+jenis);
		}else if(jenis == 4){
			//download laporan cari laporan
			if($("#data_id").val()!=""){
				$("#output").load("<?php echo Yii::app()->request->baseUrl; ?>/laporan/preview/?type="+jenis+"&data_id="+$("#data_id").val());
			}else{
				alert("Mohon cari data koperasi terlebih dahulu.")
			}
		}
		return false;
	});
	
	$("#download").click(function(){
		var jenis = $("#jenis_laporan").val();
		if(jenis == 1){
			//download laporan propinsi
			window.location = "<?php echo Yii::app()->request->baseUrl; ?>/laporan/laporan1/"
		}else if(jenis == 2){
			//download laporan kabupaten/kota
			window.location = "<?php echo Yii::app()->request->baseUrl; ?>/laporan/laporan2/"
		}else if(jenis == 3){
			//download laporan per koperasi
			window.location = "<?php echo Yii::app()->request->baseUrl; ?>/laporan/laporan3/"
		}else if(jenis == 4){
			//download laporan cari laporan
			if($("#data_id").val()!=""){
				window.location = "<?php echo Yii::app()->request->baseUrl; ?>/laporan/laporan4?data_id="+$("#data_id").val();
			}else{
				alert("Mohon cari data koperasi terlebih dahulu.")
			}
		}
		return false;
	});
</script>