<div class="mws-panel grid_4">
	<div class="mws-panel-header">
    	<span><i class="icon-file"></i> Laporan</span>
    </div>
    <div class="mws-panel-body no-padding">
		<form class="mws-form">
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<div class="mws-form-label">Kabupaten</div>
					<div class="mws-form-item">
						<select id="kabupaten">
							<option value="">( Semua )</option>
							<?php
							$kabs = Kabupaten::model()->findAll();
							foreach($kabs as $kab){
								echo "<option value='".$kab->kabupaten_id."'>".$kab->nama."</option>";
							}
							?>
						</select>
					</div>
				</div>
				<div class="mws-form-row">
					<div class="mws-form-label">Sektor</div>
					<div class="mws-form-item">
						<select id="sektor">
							<option value="">( Semua )</option>
							<?php
							$sekts = Sektor::model()->findAll("jenis = 'sektor'");
							foreach($sekts as $sektor){
								echo "<option value='".$sektor->sektor_id."'>".$sektor->nama."</option>";
							}
							?>
						</select>
					</div>
				</div>
				<div class="mws-form-row">
					<div class="mws-form-label">Kondisi</div>
					<div class="mws-form-item">
						<select id="kondisi">
							<option value="">( Semua )</option>
							<option value="aktif">Aktif</option>
							<option value="pasif">Pasif</option>
						</select>
					</div>
				</div>
				<div class="mws-form-row">
					<div class="mws-form-label">Nama Koperasi</div>
					<div class="mws-form-item">
						<input type="text" id="nama" >
					</div>
				</div>
				<div class="mws-form-row">
					<div class="mws-form-label"></div>
					<div class="mws-form-item">
						<button id="preview" class="btn btn-info">
							<i class="icon-search"></i> Pratampil
						</button>
						<button id="downloadExcel" class="btn btn-warning">
							<i class="icon-download-2"></i> Download
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>    	
</div>

<div class="mws-panel grid_4">
	<div class="mws-panel-header">
    	<span><i class="icon-file"></i> Rekapitulasi</span>
    </div>
    <div class="mws-panel-body no-padding">
		<form class="mws-form">
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<div class="mws-form-label">Per Sektor</div>
					<div class="mws-form-item">
						<select id="sektor_">
							<?php
							$sekts = Sektor::model()->findAll("jenis = 'sektor'");
							foreach($sekts as $sektor){
								echo "<option value='".$sektor->sektor_id."'>".$sektor->nama."</option>";
							}
							?>
						</select>
					</div>
				</div>
				<div class="mws-form-row">
					<div class="mws-form-label"></div>
					<div class="mws-form-item">
						<button id="preview1" class="btn btn-info">
							<i class="icon-search"></i> Pratampil
						</button>
						<button id="download1" class="btn btn-warning">
							<i class="icon-download-2"></i> Download
						</button>
					</div>
				</div>
				<div class="mws-form-row">
					<div class="mws-form-label">Kabupaten</div>
					<div class="mws-form-item">
						<select id="kabupaten_">
							<?php
							$kabs = Kabupaten::model()->findAll();
							foreach($kabs as $kab){
								echo "<option value='".$kab->kabupaten_id."'>".$kab->nama."</option>";
							}
							?>
						</select>
					</div>
				</div>
				<div class="mws-form-row">
					<div class="mws-form-label"></div>
					<div class="mws-form-item">
						<button id="preview2" class="btn btn-info">
							<i class="icon-search"></i> Pratampil
						</button>
						<button id="download2" class="btn btn-warning">
							<i class="icon-download-2"></i> Download
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>    	
</div>

<div id="output"></div>

<script>
	$("#preview").click(function(){
		var kabupaten = $("#kabupaten").val();
		var sektor = $("#sektor").val();
		var kondisi = $("#kondisi").val();
		var nama = $("#nama").val();
		
		$.ajax({
			url : "<?php echo Yii::app()->request->baseUrl ?>/laporan/preview/",
			data : {
				kabupaten : kabupaten,
				sektor : sektor,
				kondisi : kondisi,
				nama : nama,
				type : 11
			},
			type : "get",
			dataType : "html",
			success : function(msg){
				$("#output").html(msg);
			}
		});
		
		return false;
	});
	
	$("#downloadExcel").click(function(){
		var kabupaten = $("#kabupaten").val();
		var sektor = $("#sektor").val();
		var kondisi = $("#kondisi").val();
		var nama = $("#nama").val();
		
		window.location = "<?php echo Yii::app()->request->baseUrl ?>/laporan/laporan11/?type=11"+
			"&kabupaten=" + kabupaten +
			"&sektor=" + sektor +
			"&kondisi=" + kondisi +
			"&nama=" + encodeURIComponent(nama);
		
		return false;
	});
	
	$("#preview1").click(function(){
		var sektor = $("#sektor_").val();
		
		$.ajax({
			url : "<?php echo Yii::app()->request->baseUrl ?>/laporan/preview/",
			data : {
				sektor : sektor,
				type : 20
			},
			type : "get",
			dataType : "html",
			success : function(msg){
				$("#output").html(msg);
			}
		});
		
		return false;
	});
	$("#preview2").click(function(){
		var kabupaten = $("#kabupaten_").val();
		
		$.ajax({
			url : "<?php echo Yii::app()->request->baseUrl ?>/laporan/preview/",
			data : {
				kabupaten : kabupaten,
				type : 21
			},
			type : "get",
			dataType : "html",
			success : function(msg){
				$("#output").html(msg);
			}
		});
		
		return false;
	});
	
	$("#download1").click(function(){
		var sektor = $("#sektor_").val();
		
		window.location = "<?php echo Yii::app()->request->baseUrl ?>/laporan/laporan20/?"+
			"sektor=" + sektor;
		
		return false;
	});
	$("#download2").click(function(){
		var kabupaten = $("#kabupaten_").val();
		
		window.location = "<?php echo Yii::app()->request->baseUrl ?>/laporan/laporan21/?"+
			"kabupaten=" + kabupaten;
		
		return false;
	});
</script>