<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-search"></i> Preview</span>
	</div>
	<div class="mws-panel-body no-padding">
		<?php
			//echo "$filename";
			$dir = Yii::getPathOfAlias('ext.phpexcelreader');
			$filepath = $dir . DIRECTORY_SEPARATOR . 'excelreader.php';
			include($filepath);
			$data = new Spreadsheet_Excel_Reader($filename);
			echo $data->dump(true,true);
		?>
	</div>
</div>
<script>
	$(".excel").addClass("mws-table");
</script>