<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">

<!-- Viewport Metatag -->
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/bootstrap/css/bootstrap.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/fonts/ptsans/stylesheet.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/fonts/icomoon/style.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/login.min.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/mws-theme.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/login.css" media="screen">

<title>Halaman Login - Input Data Koperasi</title>

</head>

<body>

    <div id="judul_jatim">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/static/images/logoatas.png">
    </div>

    <div id="judul_header">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/static/images/header.png">
    </div>

    <div id="bulatan">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/static/images/b1.png" style="width:100px">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/static/images/b2.png" style="width:100px">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/static/images/b3.png" style="width:100px">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/static/images/b4.png" style="width:100px">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/static/images/b5.png" style="width:100px">
    </div>

    <?php
	echo $content;
	?>

    <div id="footer">
    Copyright &copy; <?php echo date("Y"); ?> Dinas Koperasi &amp; Usaha Mikro, Kecil dan Menengah Provinsi Jawa Timur
    </div>

    <!-- JavaScript Plugins -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/libs/jquery-1.8.3.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/libs/jquery.placeholder.min.js"></script>
    
    <!-- jQuery-UI Dependent Scripts -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/jui/js/jquery-ui-effects.min.js"></script>

    <!-- Plugin Scripts -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/validate/jquery.validate-min.js"></script>
</body>
</html>
