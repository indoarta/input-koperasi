<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Sign in &middot; Koperasi</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <!--<link href="<?php echo Yii::app()->request->baseUrl; ?>/static/login/css/bootstrap.css" rel="stylesheet">-->
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/static/login/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <!-- MAIN JAVASCRIPT -->
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/static/login/css/font-awesome.min.css">

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/login/js/jquery-1.7.2.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/login/js/flux.js"></script>

        <style type="text/css">
            body {
                padding-top: 30px;
                padding-bottom: 50px;
                /*background-color: #f5f5f5;*/
                background:url(<?php echo Yii::app()->request->baseUrl; ?>/static/login/img/background.png) center center no-repeat;
                background-size: 100% 120%;

            }

            .form-signin {
                max-width: 400px;
                height: 377px;
                padding: 10px 19px 19px;
                margin: 0 auto 20px;
                background-color: #2B525C;
                color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            /*
            .form-signin:hover{
                opacity:1;
            }
            .form-signin input[type="text"],
            .form-signin input[type="password"] {
              font-size: 16px;
              height: auto;
              margin-bottom: 15px;
              padding: 7px 9px;
            }*/
            #video_background {
                width:100%;
                max-width:100%;
                height:auto;
                margin:0 auto 1.15em;
                position: fixed;
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
                z-index: -1000;
            }


            /* Prevents slides from flashing */
            #slides {
                display:none;
            }

            #hijau{
                height: 5px;
                border-bottom: #00CC33 solid 55px;	
            }

            /*warna login 2B525C*/

        </style>
        <!--
        <script type="text/javascript">
        $(document).ready(function(){
                                   
            $('.fadeHover').hover(
            function(){
                $(this).stop().fadeTo('slow',1);
            },
            function(){
                $(this).stop().fadeTo('slow',0.4);
            });
            
        });
        </script>
        -->

        <script type="text/javascript">
            $(document).ready(function() {
                window.myFlux = new flux.slider('#slider');
            });


        </script>
    </head>

    <body>
        <div class="navbar navbar-fixed-top">
             <h1><img src="<?php echo Yii::app()->request->baseUrl; ?>/static/login/img/logo.png" width="500"><h1>
        </div>
        <h1 id="hijau"></h1>

        <div class="container">
            <br /><br />
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel" style="margin-right:-15px;">

                        <div id="slider" style="height:340px;">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/static/login/img/slider1.jpg" />
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/static/login/img/slider2.jpg" />
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/static/login/img/slider3.jpg" />
                        </div>
                    </div>

                </div><!-- col lg 8-->
                <div class="col-lg-4">
                    <?php
                    echo $content;
                    ?>
                    
                </div><!--./ row-->

                <br />

                <style type="text/css">
                    .simpleGlow {
                        font-family: 'Pontano Sans', Arial, Helvetica, sans-serif;
                        text-shadow: 0px 0px 2px #4d4d4d, 0px 5px 10px #aeaeae;
                        color: #FFFFFF;
                    }		
                    .sharpGlow {
                        text-shadow: 1px 0px 0px #686868 , 0px 1px 0px #686868, -1px 0px 0px #686868, 
                            0px -1px 0px #686868, 1px 4px 5px #aeaeae;
                    }		
                    .sharperGlow {
                        text-shadow: 1px 0px 0px #686868, 1px 1px 0px #686868, 0px 1px 0px #686868, 
                            -1px 1px 0px #686868, -1px 0px 0px #686868, -1px -1px 0px #686868, 
                            0px -1px 0px #686868, 1px -1px 0px #686868, 1px 4px 5px #aeaeae;
                    }
                </style>
            </div> <!-- /container -->

            <div class="navbar navbar-fixed-bottom" style="bottom:-15px; padding-top:7px; ">
                <!--
                <ul class="nav navbar-nav">
                    <li><a>Copyright © 2013 Dinas Koperasi & Usaha Mikro, Kecil dan Menengah Provinsi Jawa Timur</a></li>
                </ul>
                -->
                <font color="#CCC">Copyright © <?php echo date("Y") ?> Dinas Koperasi & Usaha Mikro, Kecil dan Menengah Provinsi Jawa Timur</font>

            </div>
        </div>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/login/js/jquery.js"></script>
    </body>
</html>
