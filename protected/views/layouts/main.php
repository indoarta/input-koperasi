
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">

<!-- Viewport Metatag -->
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<!-- Plugin Stylesheets first to ease overrides -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/colorpicker/colorpicker.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/custom-plugins/picklist/picklist.css" media="screen">

<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/bootstrap/css/bootstrap.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/fonts/ptsans/stylesheet.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/fonts/icomoon/style.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/mws-style.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/icons/icol16.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/icons/icol32.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/select2/select2.css" media="screen">

<!-- Demo Stylesheet -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/demo.css" media="screen">

<!-- jQuery-UI Stylesheet -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/jui/css/jquery.ui.all.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/jui/jquery-ui.custom.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/jui/css/jquery.ui.timepicker.css" media="screen">

<!-- Theme Stylesheet -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/mws-theme.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/static/css/themer.css" media="screen">

<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/libs/jquery-1.8.3.min.js"></script>

<style>
	li.active{
		background: #1B1B1B;
	}
</style>

<title>Aplikasi Input Data Koperasi</title>

</head>

<body>

	<!-- Themer (Remove if not needed) 
	<div id="mws-themer">
        <div id="mws-themer-content">
        	<div id="mws-themer-ribbon"></div>
            <div id="mws-themer-toggle">
                <i class="icon-bended-arrow-left"></i> 
                <i class="icon-bended-arrow-right"></i>
            </div>
        	<div id="mws-theme-presets-container" class="mws-themer-section">
	        	<label for="mws-theme-presets">Color Presets</label>
            </div>
            <div class="mws-themer-separator"></div>
        	<div id="mws-theme-pattern-container" class="mws-themer-section">
	        	<label for="mws-theme-patterns">Background</label>
            </div>
            <div class="mws-themer-separator"></div>
            <div class="mws-themer-section">
                <ul>
                    <li class="clearfix"><span>Base Color</span> <div id="mws-base-cp" class="mws-cp-trigger"></div></li>
                    <li class="clearfix"><span>Highlight Color</span> <div id="mws-highlight-cp" class="mws-cp-trigger"></div></li>
                    <li class="clearfix"><span>Text Color</span> <div id="mws-text-cp" class="mws-cp-trigger"></div></li>
                    <li class="clearfix"><span>Text Glow Color</span> <div id="mws-textglow-cp" class="mws-cp-trigger"></div></li>
                    <li class="clearfix"><span>Text Glow Opacity</span> <div id="mws-textglow-op"></div></li>
                </ul>
            </div>
            <div class="mws-themer-separator"></div>
            <div class="mws-themer-section">
	            <button class="btn btn-danger small" id="mws-themer-getcss">Get CSS</button>
            </div>
        </div>
        <div id="mws-themer-css-dialog">
        	<form class="mws-form">
            	<div class="mws-form-row">
		        	<div class="mws-form-item">
                    	<textarea cols="auto" rows="auto" readonly="readonly"></textarea>
                    </div>
                </div>
            </form>
        </div>
    </div>
   -->

	<!-- Header -->
	<div id="mws-header" class="clearfix">
    
    	<!-- Logo Container -->
    	<div id="mws-logo-container">
        
        	<!-- Logo Wrapper, images put within this wrapper will always be vertically centered -->
        	<div id="mws-logo-wrap">
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/">
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/static/images/mws-logo.png" 
						 alt="Logo"
						 style="max-width: 400px; height: 100px;"
						 >
				</a>
			</div>
        </div>
        
        <!-- User Tools (notifications, logout, profile, change password) -->
        <div id="mws-user-tools" class="clearfix">
            <!-- User Information and functions section -->
            <div id="mws-user-info" class="mws-inset">
            
            	<!-- User Photo -->
            	<div id="mws-user-photo">
					<a href="<?php echo Yii::app()->request->baseUrl; ?>/site/profile">
						<img src="<?php echo Yii::app()->request->baseUrl; ?>/static/images/one.png" alt="User Photo">
					</a>
                </div>
                
				<?php
				$user = User::model()->findByPk(Yii::app()->session["userid"]);
				?>
				
                <!-- Username and Functions -->
                <div id="mws-user-functions">
                    <div id="mws-username">
                        Halo, <?php echo $user->nama_lengkap; ?>
                    </div>
                    <ul>
                    	<li><a href="<?php echo Yii::app()->request->baseUrl ?>/profile">Profil</a></li>
                        <li><a href="<?php echo Yii::app()->request->baseUrl ?>/changepassword">Ganti Password</a></li>
                        <li><a href="<?php echo Yii::app()->request->baseUrl ?>/site/logout">Keluar</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Start Main Wrapper -->
    <div id="mws-wrapper">
    
    	<!-- Necessary markup, do not remove -->
		<div id="mws-sidebar-stitch"></div>
		<div id="mws-sidebar-bg"></div>
        
        <!-- Sidebar Wrapper -->
        <div id="mws-sidebar">
        
            <!-- Hidden Nav Collapse Button -->
            <div id="mws-nav-collapse">
                <span></span>
                <span></span>
                <span></span>
            </div>
            
        	<!-- Searchbox 
        	<div id="mws-searchbox" class="mws-inset">
            	<form action="typography.html">
                	<input type="text" class="mws-search-input" placeholder="Search...">
                    <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
                </form>
            </div>
			-->
            
            <!-- Main Navigation -->
            <div id="mws-navigation">
                <ul>
					<?php
					$userLevel = $user->level;
					$modules = Module::model()->findAll("parent_id = '0' AND type = '1' ORDER BY `order` ASC");
					$ctrl = Yii::app()->controller->id;
					$act = Yii::app()->controller->action->id;
					foreach($modules as $m){
						$moduleLevel = $m->level;
						if($userLevel <= $moduleLevel ){
							if($ctrl . "/" . $act == $m->controller ){
								echo '<li class="active"><a href="'.Yii::app()->request->baseUrl."/".$m->controller.'"><i class="'.$m->icon.'"></i> '.$m->nama.'</a>';
								echo "</li>";
							}else{
								echo '<li '.($ctrl == $m->controller?"class='active'":"").'>
									<a href="'.Yii::app()->request->baseUrl."/".$m->controller.'"><i class="'.$m->icon.'"></i> '.$m->nama.'</a>';
								$child = Module::model()->findAll("parent_id = '".$m->module_id."' AND type = '1' ORDER BY `order` ASC");
								if(count($child) > 0){
									echo "<ul>";
									foreach($child as $c){
										echo "<li><a class='submenu' href='".Yii::app()->request->baseUrl."/".$m->controller.$c->controller."'>".$c->nama."</a></li>";
									}
									echo "</ul>";
								}
								echo '</li>';
							}
						}
					}
					?>
                </ul>
            </div>
        </div>
        
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
				
				<?php echo $content; ?>
				
            </div>
            <!-- Inner Container End -->
                       
            <!-- Footer -->
            <div id="mws-footer">
				Copyright &copy; <?php echo date("Y") ?>. Input Koperasi
            </div>
            
        </div>
        <!-- Main Container End -->
        
    </div>

    <!-- JavaScript Plugins -->
    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/libs/jquery.mousewheel.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/libs/jquery.price.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/libs/jquery.placeholder.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/custom-plugins/fileinput.min.js"></script>

    <!-- jQuery-UI Dependent Scripts -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/jui/js/jquery-ui-1.9.2.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/jui/jquery-ui.custom.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/jui/js/jquery.ui.touch-punch.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/jui/js/timepicker/jquery-ui-timepicker.min.js"></script>

    <!-- Plugin Scripts -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/select2/select2.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/custom-plugins/picklist/picklist.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/colorpicker/colorpicker-min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/validate/jquery.validate-min.js"></script>
	<!--[if lt IE 9]>
    <script src="js/libs/excanvas.min.js"></script>
    <![endif]-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/flot/jquery.flot.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/flot/plugins/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/flot/plugins/jquery.flot.pie.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/flot/plugins/jquery.flot.resize.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/flot/plugins/jquery.flot.categories.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/plugins/flot/plugins/jquery.flot.stack.min.js"></script>
	
    <!-- Core Script -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/core/mws.js"></script>

    <!-- Themer Script (Remove if not needed) -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/core/themer.js"></script>

    <!-- Demo Scripts (remove if not needed) -->
	<script>
	$(document).ready(function(){
		if( $.fn.select2 ) {
            $("select.mws-select").select2();
        }
	})
	</script>
</body>
</html>