
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'user-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array('class'=>'mws-form')
));
?>	<div class="mws-form-inline">
		<div class="mws-form-row">
			<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
			<?php echo $form->errorSummary($model); ?>
		</div>

		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'username'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'username', array('maxlength' => 50)); ?>
				<?php echo $form->error($model,'username'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'password'); ?></div>
			<div class="mws-form-item">
				<input maxlength="50" name="User[password]" id="User_password" type="password" value="">
				<?php //echo $form->passwordField($model, 'password', array('maxlength' => 50)); ?>
				<?php echo $form->error($model,'password'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'nama_lengkap'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'nama_lengkap', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'nama_lengkap'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'nama_divisi'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'nama_divisi', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'nama_divisi'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'email'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'email', array('maxlength' => 100)); ?>
				<?php echo $form->error($model,'email'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'level'); ?></div>
			<div class="mws-form-item">
				<?php
				$static = array(
					'1'     => Yii::t('fim','Admin'), 
					'2' => Yii::t('fim','Operator'), 
					'3'   => Yii::t('fim','Mahasiswa'),
				);

				echo $form->dropDownList($model, 'level', $static);
				?>
				<?php echo $form->error($model,'level'); ?>
			</div>
		</div>
		<?php echo $form->hiddenField($model, 'last_login', array('maxlength' => 100)); ?>
		<?php echo $form->hiddenField($model, 'last_logout', array('maxlength' => 100)); ?>

		<div class="mws-form-row">
			<input type="submit" value="Save" class="btn btn-primary">
		</div>
	</div>
<?php
$this->endWidget();
?>
