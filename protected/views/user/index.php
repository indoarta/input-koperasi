<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-user"></i> Daftar User</span>
	</div>
	<div class="mws-panel-toolbar">
		<div class="btn-toolbar">
			<div class="btn-group">
				<a href="<?php echo Yii::app()->request->baseUrl ?>/user/create/" class="btn"><i class="icol-add"></i> Tambah User</a>
			</div>
		</div>
	</div>
	<div class="mws-panel-body no-padding">
		<table id="tabelUser" class="mws-datatable-fn mws-table">
			<thead>
				<tr>
					<th>Nama</th>
					<th>Level</th>
					<th>#</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$User = User::model()->findAll();
				foreach($User as $m){
					echo "<tr>
						<td>".$m->nama_lengkap."</td>
						<td style='text-align:center'>".$m->level."</td>
						<td style='text-align:center'><a href='".Yii::app()->request->baseUrl."/user/update/".$m->user_id."' class='btn btn-primary'>Edit</a></td>
					</tr>";
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<script>
	$(document).ready(function(){
		$("#tabelUser").dataTable({
			sPaginationType: "full_numbers"
		});
	});
</script>