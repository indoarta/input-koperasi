<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-table"></i> Profil</span>
	</div>
	<div class="mws-panel-body no-padding">
		<form class="mws-form" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/profile/save">
			<?php
			if(isset($oke)){
			?>
			<div class="mws-form-message success">
				Data berhasil diperbaharui
			</div>
			<?php
			}
			?>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<div class="mws-form-label">
						Username :
					</div>
					<div class="mws-form-item">
						<input type="text" id="username" name="username" readonly value="<?php echo $model->username; ?>" >
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<div class="mws-form-label">
						Nama Lengkap :
					</div>
					<div class="mws-form-item">
						<input type="text" id="nama_lengkap" name="nama_lengkap" value="<?php echo $model->nama_lengkap; ?>" >
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<div class="mws-form-label">
						Nama Divisi :
					</div>
					<div class="mws-form-item">
						<input type="text" id="nama_divisi" name="nama_divisi" value="<?php echo $model->nama_divisi; ?>" >
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<div class="mws-form-label">
						Email :
					</div>
					<div class="mws-form-item">
						<input type="email" id="email" name="email" value="<?php echo $model->email; ?>" >
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<div class="mws-form-label">
						
					</div>
					<div class="mws-form-item">
						<button class="btn btn-primary">Simpan</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>