
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'module-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array('class'=>'mws-form')
));
?>	<div class="mws-form-inline">
		<div class="mws-form-row">
			<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
			<?php echo $form->errorSummary($model); ?>
		</div>

		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'nama'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'nama', array('maxlength' => 20)); ?>
				<?php echo $form->error($model,'nama'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'controller'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'controller', array('maxlength' => 20)); ?>
				<?php echo $form->error($model,'controller'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'icon'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'icon', array('maxlength' => 20)); ?>
				<?php echo $form->error($model,'icon'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'level'); ?></div>
			<div class="mws-form-item">
				<?php
				$static = array(
					'1'     => Yii::t('fim','Admin'), 
					'2' => Yii::t('fim','Operator'), 
					'3'   => Yii::t('fim','Mahasiswa'),
				);

				echo $form->dropDownList(
					$model,
					'level',
					$static);
				?>
				<?php echo $form->error($model,'level'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'type'); ?></div>
			<div class="mws-form-item">
				<?php
				$static = array(
					'1'     => Yii::t('fim','Sidebar'), 
					'2' => Yii::t('fim','Other'), 
				);

				echo $form->dropDownList(
					$model,
					'type',
					$static);
				?>
				<?php echo $form->error($model,'type'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'order'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'order'); ?>
				<?php echo $form->error($model,'order'); ?>
			</div>
		</div>


		<div class="mws-form-row">
			<input type="submit" value="Save" class="btn btn-primary">
		</div>
	</div>
<?php
$this->endWidget();
?>
