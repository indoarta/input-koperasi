<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-table"></i> Manajemen Modul</span>
	</div>
	<div class="mws-panel-toolbar">
		<div class="btn-toolbar">
			<div class="btn-group">
				<a href="<?php echo Yii::app()->request->baseUrl ?>/module/create" 
				   class="btn"><i class="icol-add"></i> Tambah Modul</a>
				<a id="simpanBtn" href="#" 
				   class="btn"><i class="icol-disk"></i> Simpan</a>
			</div>
		</div>
	</div>
	<div class="mws-panel-body no-padding">
		<table class="mws-table" id="tbl">
			<thead>
				<tr>
					<th>Nama Modul</th>
					<th>Link</th>
					<th>Ikon</th>
					<th>#</th>
				</tr>
			</thead>
			<tbody id="tbody">
				<?php
				$modules = Module::model()->findAll('1 ORDER BY `order` ASC');
				foreach($modules as $mod){
				?>
				<tr kode="<?php echo $mod->module_id; ?>">
					<td><input type="text" class="nama" style="width: 90%" value="<?php echo $mod->nama; ?>"></td>
					<td><input type="text" class="controller" style="width: 90%" value="<?php echo $mod->controller; ?>"></td>
					<td><input type="text" class="icon" style="width: 90%" value="<?php echo $mod->icon; ?>"></td>
					<td style="text-align: center">
						<div class="btn-group">
							<button class="btn btn-primary up">
								<i class="icon-arrow-up"></i>
							</button>
							<button class="btn btn-primary down">
								<i class="icon-arrow-down"></i>
							</button>
							<a href="<?php echo Yii::app()->request->baseUrl; ?>/module/update/<?php echo $mod->module_id; ?>" class="btn btn-primary edit">
								<i class="icon-edit"></i>
							</a>
						</div>
					</td>
				</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
</div>
<script>
	$.fn.extend({ 
		moveRow: function(oldPosition, newPosition) { 
		  return this.each(function(){ 
			var row = $(this).find('tr').eq(oldPosition).remove(); 
			$(this).find('tr').eq(newPosition).before(row); 
		  }); 
		 } 
	}); 
	
	$(document).on("click", ".up", function(){
		var indeks = $(".up").index($(this));
		$("#tbody").moveRow(indeks, indeks-1);
	});
	
	$(document).on("click", ".down", function(){
		var indeks = $(".down").index($(this));
		$("#tbody").moveRow(indeks, indeks+1);
	});
	
	$("#simpanBtn").click(function(){
		var arr = new Array();
		$("#tbody tr").each(function(){
			var ini = $(this);
			var kode = ini.attr("kode");
			var nama = ini.find(".nama").val();
			var controller = ini.find(".controller").val();
			var icon = ini.find(".icon").val();
			arr.push(kode+"||"+nama+"||"+controller+"||"+icon);
		})
		var q = arr.join(";;");
		$.ajax({
			url : "<?php echo Yii::app()->request->baseUrl; ?>/ajax/savemodule",
			data : "q="+q,
			type : "post",
			success : function(e){
				alert("Data berhasil disimpan")
			}
		});
		return false;
	});
</script>