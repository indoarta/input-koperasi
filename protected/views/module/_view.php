<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('module_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->module_id), array('view', 'id' => $data->module_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nama')); ?>:
	<?php echo GxHtml::encode($data->nama); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('controller')); ?>:
	<?php echo GxHtml::encode($data->controller); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('icon')); ?>:
	<?php echo GxHtml::encode($data->icon); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('level')); ?>:
	<?php echo GxHtml::encode($data->level); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('type')); ?>:
	<?php echo GxHtml::encode($data->type); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('order')); ?>:
	<?php echo GxHtml::encode($data->order); ?>
	<br />

</div>