<?php
$sektor = Sektor::model()->findByPk($sektor_id);
?>
<div class="grid_8" style="margin-bottom: 20px">
	<a href="javascript:window.history.back()" class="btn btn-warning">
		<i class="icon-chevron-left"></i> Kembali
	</a>
</div>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-graph"></i> Daftar Koperasi yang mencakup Sektor <?php echo $sektor->nama; ?></span>
	</div>
	<div >
		<table class="mws-table">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Koperasi</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 1;
				$kops = DataUtama::model()->findAll("sektor_id = '".$sektor_id."'");
				foreach ($kops as $kop) {
					$jml = 0;
					echo "<tr>
						<td>".$i."</td>
						<td>".$kop->nama_koperasi."</td>
						<td style='text-align:center'><a href='".Yii::app()->request->baseUrl."/site/perkoperasi/?data_id=".$kop->data_id."' class='btn btn-primary'>Lihat</a></td>
					</tr>";
					$i++;
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<script>
	<?php
	
	$list = Yii::app()->db->createCommand("
select se.sektor_id, se.nama, sum(su.jumlah_unit) as total
from sarana_usaha su, data_utama du, sarana sa, sektor se
where su.data_id = du.data_id AND su.sarana_id = sa.sarana_id AND sa.sektor_id = se.sektor_id
AND du.kabupaten_id = '".$kabupaten_id."'
group by se.sektor_id")->queryAll();

	
	?>
	$(document).ready(function() {
		
    });
</script>