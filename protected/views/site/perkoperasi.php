<?php
$model = DataUtama::model()->findByPk($data_id);
//$model = new DataUtama();
?>
<style>
	.mws-summary>li .key {
		width: 211px;
	}
</style>
<div class="grid_8" style="margin-bottom: 20px">
	<a href="javascript:window.history.back()" class="btn btn-warning">
		<i class="icon-chevron-left"></i> Kembali
	</a>
	<a href="<?php echo Yii::app()->request->baseUrl ?>/inputData/update/<?php echo $data_id ?>" class="btn btn-primary">
		<i class="icon-edit"></i> Update
	</a>
</div>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-graph"></i> Detail Koperasi</span>
	</div>
	<div class="mws-panel-body no-padding">
		<ul class="mws-summary clearfix">
			<li>
				<span class="key"><i class="icon-minus"></i> Nama Koperasi</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->nama_koperasi
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Sektor</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$sektor = Sektor::model()->findByPk($model->sektor_id);
						echo $sektor->nama;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Badan Hukum</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$arr = array();
						if($model->tanggal_badan_hukum != NULL){
							$arr[] = "Tgl. ".date("d-M-Y", strtotime($model->tanggal_badan_hukum));
						}
						if($model->no_badan_hukum != NULL){
							$arr[] = "No. ".$model->no_badan_hukum;
						}
						echo implode(", ", $arr);
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Perubahan Anggaran Dasar</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$arr = array();
						if($model->tanggal_perubahan_anggaran_dasar != NULL){
							$arr[] = "Tgl. ".date("d-M-Y", strtotime($model->tanggal_perubahan_anggaran_dasar));
						}
						if($model->no_perubahan_anggaran_dasar != NULL){
							$arr[] = "No. ".$model->no_perubahan_anggaran_dasar;
						}
						echo implode(", ", $arr);
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Anggaran Dasar</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->anggaran_dasar == 1 ? "Ada" : "Tidak Ada";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Akte Pendirian</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->akte_pendirian == 1 ? "Ada" : "Tidak Ada";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Ijin Usaha</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "TDP : " . ($model->izin_usaha_tdp == 1 ? "Ada" : "Tidak Ada") . "<br>";
						echo "SIUP : " . ($model->izin_usaha_siup == 1 ? "Ada" : "Tidak Ada") . "<br>";
						echo "HO : " . ($model->izin_usaha_ho == 1 ? "Ada" : "Tidak Ada") . "<br>";
						echo "Lainnya : " . ($model->izin_usaha_lainnya == 1 ? "Ada" : "Tidak Ada") . "<br>";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Tahun Berdiri</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->tahun_berdiri;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Alamat Koperasi</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$kab = Kabupaten::model()->findByPk($model->kabupaten_id);
						echo $model->jalan.", ".$model->desa.", ".$model->kecamatan.", ".$kab->nama;
						echo "<br>Telp : ".$model->telepon.", Fax".$model->fax.", Email : ".$model->email;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Status Kantor</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						if($model->status_kantor_id == 3){
							echo $model->status_kantor_lainnya;
						}else{
							$statusK = StatusKantor::model()->findByPk($model->status_kantor_id);
							echo $statusK->nama;
						}
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Kondisi Koperasi</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->kondisi_koperasi == 1?"Aktif":"Tidak Aktif";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Status Binaan</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$statusB = StatusBinaan::model()->findByPk($model->status_binaan_id);
						echo $statusB->nama;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Unit Usaha</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$sektors = BidangUsaha::model()->findAll("data_id = '".$model->data_id."'");
						foreach($sektors as $sektor){
							echo "<div>".$sektor->nama."</div>";
						}
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Unit Usaha Inti</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$sektors = BidangUsahaInti::model()->findAll("data_id = '".$model->data_id."'");
						foreach($sektors as $sektor){
							echo "<div>".$sektor->nama."</div>";
						}
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Usaha Kementrian</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "BUMS : ".$model->usaha_kemitraan_bums . "<br>";
						echo "BUMD : ".$model->usaha_kemitraan_bumd . "<br>";
						echo "BUMN : ".$model->usaha_kemitraan_bumn . "<br>";
						echo "Antar Koperasi : ".$model->usaha_kemitraan_antar_koperasi . "<br>";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Jumlah Anggota</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->jumlah_anggota . " Orang";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Jumlah Calon Anggota</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->jumlah_calon_anggota . " Orang";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Jumlah Masyarakat Terlayani</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->jumlah_masyarakat_terlayani . " Orang";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Periode</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->periode_1 . " - " . $model->periode_2;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Ketua</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->ketua . " &raquo; " . $model->ketua_cp;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Sekretaris</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->sekretaris . " &raquo; " . $model->sekretaris_cp;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Bendahara</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->bendahara . " &raquo; " . $model->bendahara_cp;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Pembantu Umum</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->pembantu_umum . " &raquo; " . $model->pembantu_umum_cp;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Koordinator</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->koordinator . " &raquo; " . $model->koordinator_cp;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Anggota</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo $model->anggota . " &raquo; " . $model->anggota_cp;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Pengelola</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$sektors = Pengelola::model()->findAll("data_id = '".$model->data_id."'");
						foreach($sektors as $sektor){
							$jp = JenisPengelola::model()->findByPk($sektor->jenis_pengelola_id);
							echo "<div>".$sektor->nama.", ".$sektor->nama_bagian.", ".$jp->nama."</div>";
						}
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Modal Sendiri</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->ms_simpanan_pokok + $model->ms_simpanan_wajib
								+ $model->ms_bantuan_hibah + $model->ms_bantuan_sosial
								+ $model->ms_cadangan, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li style="margin-left: 11px">
				<span class="key"><i class="icon-chevron-right"></i> Simpanan Pokok</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->ms_simpanan_pokok, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li style="margin-left: 11px">
				<span class="key"><i class="icon-chevron-right"></i> Simpanan Wajib</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->ms_simpanan_wajib, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li style="margin-left: 11px">
				<span class="key"><i class="icon-chevron-right"></i> Bantuan Hibah</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->ms_bantuan_hibah, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li style="margin-left: 11px">
				<span class="key"><i class="icon-chevron-right"></i> Bantuan Sosial</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->ms_bantuan_sosial, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li style="margin-left: 11px">
				<span class="key"><i class="icon-chevron-right"></i> Cadangan</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->ms_cadangan, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Modal Luar</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->ml_pinjaman_bank + 
								$model->ml_lemb_non_bank + $model->ml_pinjaman_pihak_iii, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li style="margin-left: 11px">
				<span class="key"><i class="icon-chevron-right"></i> Pinjaman Bank</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->ml_pinjaman_bank, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li style="margin-left: 11px">
				<span class="key"><i class="icon-chevron-right"></i> Lembaga Non Bank</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->ml_lemb_non_bank, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li style="margin-left: 11px">
				<span class="key"><i class="icon-chevron-right"></i> Pinjaman Pihak III</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->ml_pinjaman_pihak_iii, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Vol. Usaha s/d Des 2012</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->volume_des_2012, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Vol. Usaha Th. Berjalan</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->volume_tahun_berjalan, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> SHU s/d Des 2012</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->shu_des_2012, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> SHU Th. Berjalan</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						echo "Rp ".number_format($model->shu_tahun_berjalan, 0, ",", ".").",-";
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-minus"></i> Sarana Usaha</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$sektor = Sektor::model()->findByPk($model->sektor_id);
						$saranas = Sarana::model()->findAll("sektor_id = '".$s->sektor_id."'");
						foreach($saranas as $sarana){
							$jml = SaranaUsaha::model()->find("data_id = '".$model->data_id."' and sarana_id = '".$sarana->sarana_id."'");
							echo "<div>" . $sarana->nama . " : ".$jml->jumlah_unit."</div>";
						}
						?>
					</span>
				</span>
			</li>
		</ul>
	</div>
</div>