<style>
	#dataperkab .flot-x-axis .tickLabel{
		-o-transform:rotate(-90deg);
		-moz-transform: rotate(-90deg);
		-webkit-transform:rotate(-90deg);
		 filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
		 width: 200px !important;
		 max-width: 200px !important;
		 text-align: right !important;
		 top: 292px !important;
	}
</style>

<div class="mws-panel grid_3">
	<div class="mws-panel-header">
		<span><i class="icon-book"></i> Pilih Kabupaten untuk Melihat Detail</span>
	</div>
	<div class="mws-panel-body no-padding">
		<form class="mws-form">
			<div class="mws-form-row">
				<label class="mws-form-label">Daftar Kabupaten</label>
				<div class="mws-form-item">
					<select id="kabupaten">
						<?php
						$kabupatens = Kabupaten::model()->findAll();
						foreach($kabupatens as $kab){
							echo "<option value='".$kab->kabupaten_id."'>".$kab->nama."</option>";
						}
						?>
					</select>
				</div>
			</div>
			<div class="mws-form-row">
				<label class="mws-form-label"></label>
				<div class="mws-form-item">
					<button class="btn btn-primary" id="lihatData">
						<i class="icon-search"></i> Lihat Data
					</button>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="mws-panel grid_5">
	<div class="mws-panel-header">
		<span><i class="icon-book"></i> Ringkasan Data Koperasi</span>
	</div>
	<div class="mws-panel-body no-padding">
		<ul class="mws-summary clearfix">
			<li>
				<span class="key"><i class="icon-database"></i> Jumlah Record</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$data = DataUtama::model()->count();
						echo $data;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-folder-closed"></i> &Sigma; Data Kabupaten</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$data = DataUtama::model()->count("status_binaan_id = 1 OR status_binaan_id = 3");
						echo $data;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-folder-closed"></i> &Sigma; Data Propinsi</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$data = DataUtama::model()->count("status_binaan_id = 2 OR status_binaan_id = 4");
						echo $data;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-users"></i> Jumlah Pengguna</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						$data = User::model()->count();
						echo $data;
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-key"></i> Logout Terakhir</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
							$user = User::model()->findByPk(Yii::app()->session["userid"]);
							$time = strtotime($user->last_logout);
							echo date("d M Y, H:i:s", $time);
						?>
					</span>
				</span>
			</li>
			<li>
				<span class="key"><i class="icon-windows"></i> Sistem Operasi</span>
				<span class="val">
					<span class="text-nowrap">
						<?php
						function getBrowser() 
						{ 
							$u_agent = $_SERVER['HTTP_USER_AGENT']; 
							$bname = 'Unknown';
							$platform = 'Unknown';
							$version= "";

							//First get the platform?
							if (preg_match('/linux/i', $u_agent)) {
								$platform = 'Linux';
							}
							elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
								$platform = 'Mac OS';
							}
							elseif (preg_match('/windows|win32/i', $u_agent)) {
								$platform = 'Windows';
							}

							// Next get the name of the useragent yes seperately and for good reason
							if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
							{ 
								$bname = 'Internet Explorer'; 
								$ub = "MSIE"; 
							} 
							elseif(preg_match('/Firefox/i',$u_agent)) 
							{ 
								$bname = 'Mozilla Firefox'; 
								$ub = "Firefox"; 
							} 
							elseif(preg_match('/Chrome/i',$u_agent)) 
							{ 
								$bname = 'Google Chrome'; 
								$ub = "Chrome"; 
							} 
							elseif(preg_match('/Safari/i',$u_agent)) 
							{ 
								$bname = 'Apple Safari'; 
								$ub = "Safari"; 
							} 
							elseif(preg_match('/Opera/i',$u_agent)) 
							{ 
								$bname = 'Opera'; 
								$ub = "Opera"; 
							} 
							elseif(preg_match('/Netscape/i',$u_agent)) 
							{ 
								$bname = 'Netscape'; 
								$ub = "Netscape"; 
							} 

							// finally get the correct version number
							$known = array('Version', $ub, 'other');
							$pattern = '#(?<browser>' . join('|', $known) .
							')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
							if (!preg_match_all($pattern, $u_agent, $matches)) {
								// we have no matching number just continue
							}

							// see how many we have
							$i = count($matches['browser']);
							if ($i != 1) {
								//we will have two since we are not using 'other' argument yet
								//see if version is before or after the name
								if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
									$version= $matches['version'][0];
								}
								else {
									$version= $matches['version'][1];
								}
							}
							else {
								$version= $matches['version'][0];
							}

							// check if we have a number
							if ($version==null || $version=="") {$version="?";}

							return array(
								'userAgent' => $u_agent,
								'name'      => $bname,
								'version'   => $version,
								'platform'  => $platform,
								'pattern'    => $pattern
							);
						} 

						// now try it
						$ua=getBrowser();
						$yourbrowser = $ua['platform'];
						echo $yourbrowser;
						?>
					</span>
				</span>
			</li>
		</ul>
	</div>
</div>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-graph"></i> Chart Sektor Riil Propinsi Jawa Timur</span>
	</div>
	<div class="mws-panel-body">
		<div id="mws-dashboard-chart" style="height: 222px;"></div>
	</div>
</div>
<!--
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-graph"></i> Chart Sektor Riil Per Kabupaten di Jawa Timur</span>
	</div>
	<div class="mws-panel-body">
		<div id="dataperkab" style="height: 222px;"></div>
	</div>
</div>
-->

<script>
	<?php
	$output = array();
	$sid = array();
	$arrID = array();
	$sektors = Sektor::model()->findAll("jenis = 'sektor'");
	foreach ($sektors as $s){
		$output[$s->nama] = 0;
		$sid[$s->sektor_id] = $s->nama;
		$arrID[] = $s->sektor_id;
	}
	
	$dataUtama = DataUtama::model()->findAll();
	foreach($dataUtama as $du){
		$output[$sid[$du->sektor_id]] += 1;
	}
	
	$output2 = array();
	foreach($output as $key => $val){
		$output2[] = '["'.$key.'", '.$val.']';
	}
	
	//Untuk chart kedua
	$list = Yii::app()->db->createCommand("
		select du.kabupaten_id, ka.nama, sum(su.jumlah_unit) as total
		from sarana_usaha su, data_utama du, sarana sa, sektor se, kabupaten ka
		where su.data_id = du.data_id AND su.sarana_id = sa.sarana_id AND sa.sektor_id = se.sektor_id AND se.jenis = 'sektor'
		AND ka.kabupaten_id = du.kabupaten_id
		group by du.kabupaten_id
	")->queryAll();
	
	$kabOutput = array();
	$kabs = Kabupaten::model()->findAll();
	foreach($kabs as $kab){
		$kabOutput[$kab->nama] = "0";
	}
	
	foreach($list as $item){
		$kabOutput[$item['nama']] = $item["total"]*1;
	}
	
	$outputPerKab = array();
	foreach($kabOutput as $key => $val){
		$outputPerKab[] = '["'.$key.'", '.$val.']';
	}
	?>
		
	$(document).ready(function() {
		$("#lihatData").click(function(){
			window.location = "<?php echo Yii::app()->request->baseUrl; ?>/site/perkabupaten?kabupaten_id="+$("#kabupaten").val();
			return false;
		});
		
		var idArray = [<?php echo implode(",",$arrID); ?>];
		
        if( $.plot ) {
			var data = [ 
				<?php
				echo implode(", ", $output2);
				?> 
			];

			$.plot("#mws-dashboard-chart", [ data ], {
				series: {
					bars: {
						show: true,
						barWidth: 0.6,
						align: "center"
					}
					,color: "#FF384C"
				},
				
				xaxis: {
					mode: "categories",
					tickLength: 0
				},
				tooltip: true,
				tooltipOpts: {
					content: "Jumlah: %y unit"
				},
                grid: {
                    hoverable: true,
                    borderWidth: 0,
					clickable: true
                }
			});
			
			$("#mws-dashboard-chart").bind("plotclick", function (event, pos, item) {
				if (item) {
					var index = item.dataIndex;
					var hasil = idArray[index];
					window.location = "<?php echo Yii::app()->request->baseUrl; ?>/site/persektorallkab?sektor_id="+hasil;
				}
			});
			
			/*
			var dataPerKab = [ 
				<?php
				echo implode(", ", $outputPerKab);
				?> 
			];

			$.plot("#dataperkab", [ dataPerKab ], {
				series: {
					bars: {
						show: true,
						barWidth: 0.6,
						align: "center"
					}
					,color: "#C20288"
				},
				
				xaxis: {
					mode: "categories",
					tickLength: 0,
					labelWidth: 30
				},
				tooltip: true,
				tooltipOpts: {
					content: "Jumlah: %y unit"
				},
                grid: {
                    hoverable: true,
                    borderWidth: 0
                }
			});*/
			
			/*
            var PageViews = [],
                Sales = [];
            for (var i = 0; i <= 31; i++) {
                PageViews.push([i, 100 + Math.floor((Math.random() < 0.5 ? -1 : 1) * Math.random() * 25)]);
                Sales.push([i, 60 + Math.floor((Math.random() < 0.5 ? -1 : 1) * Math.random() * 40)]);
            }

            var data = [{
                data: PageViews,
                label: "Page Views",
                color: "#c75d7b"
            }, {
                data: Sales,
                label: "Sales",
                color: "#c5d52b"
            }];

            var plot = $.plot($("#mws-dashboard-chart"), data, {
                series: {
					bars: { show: true },
                    points: {
                        show: false
                    }
                },
                tooltip: true,
                grid: {
                    hoverable: true,
                    borderWidth: 0
                }
            });*/
        }
    });
</script>