<?php
$kab = Kabupaten::model()->findByPk($kabupaten_id);
?>
<div class="grid_8" style="margin-bottom: 20px">
	<a href="<?php echo Yii::app()->request->baseUrl; ?>/site/" class="btn btn-warning">
		<i class="icon-chevron-left"></i> Kembali
	</a>
</div>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-graph"></i> Chart Sektor Riil di <?php echo $kab->nama; ?></span>
	</div>
	<div class="mws-panel-body">
		<div id="mws-dashboard-chart" style="height: 222px;"></div>
	</div>
</div>

<script>
	<?php
	$sektorArray = array();
	$sektorTempArr = Sektor::model()->findAll("jenis = 'sektor'");
	$data = array();
	$arrID = array();
	foreach($sektorTempArr as $sek){
		$sektorArray[$sek->sektor_id] = $sek->nama;
		$data[$sek->nama] = 0;
		$arrID[] = $sek->sektor_id;
	}

	$dataUtama = DataUtama::model()->findAllByAttributes(array("kabupaten_id"=>$kabupaten_id));
	foreach($dataUtama as $du){
		$data[$sektorArray[$du->sektor_id]] += 1;
	}
	?>
	var idArray = [<?php echo implode(",",$arrID); ?>];
	
	$(document).ready(function() {
		$("#lihatData").click(function(){
			window.location = "<?php echo Yii::app()->request->baseUrl; ?>/site/perkabupaten?kabupaten_id="+$("#kabupaten").val();
			return false;
		});
		
        if( $.plot ) {
			var data = [
				<?php
				$output = array();
				foreach ($data as $k => $v) {
					$output[] = '["'.$k.'", '. $v.']';
				}
				echo implode(",\n", $output);
				?>
			];

			$.plot("#mws-dashboard-chart", [ data ], {
				series: {
					bars: {
						show: true,
						barWidth: 0.6,
						align: "center",
						clickable: true
					}
					,color: "#C20288"
				},
				
				xaxis: {
					mode: "categories",
					tickLength: 0
				},
				tooltip: true,
				tooltipOpts: {
					content: "Jumlah: %y unit"
				},
                grid: {
                    hoverable: true,
                    borderWidth: 0,
					clickable: true
                }
			});
			
			$("#mws-dashboard-chart").bind("plotclick", function (event, pos, item) {
				if (item) {
					var index = item.dataIndex;
					var hasil = idArray[index];
					window.location = "<?php echo Yii::app()->request->baseUrl; ?>/site/persektor?kabupaten_id=<?php echo $kabupaten_id ?>&sektor_id="+hasil;
				}
			});
        }
    });
</script>