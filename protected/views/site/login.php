<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array("class" => "form-signin fadeHover", "method" => "post", "action" => Yii::app()->request->baseUrl . "/site/login"),
        ));
?>
<img src="<?php echo Yii::app()->request->baseUrl; ?>/static/login/img/tagline.png" width="100%"/> 
<hr />
<?php
if (isset($message)) {
    ?>
    <div class="mws-form-message error">
        <?php echo $message; ?>
    </div>
    <?php
}
?>
<div class="input-group">
    <span class="input-group-addon"><i class="icon-user"></i></span>
    <?php echo $form->textField($model, 'username', array("class" => "form-control required", "placeholder" => "Username")); ?>
</div>
<?php echo $form->error($model, 'username'); ?>
<br>
<div class="input-group">
    <span class="input-group-addon"><i class="icon-lock"></i></span>
    <?php echo $form->passwordField($model, 'password', array("class" => "form-control required", "placeholder" => "Password")); ?>
</div>
<?php echo $form->error($model, 'password'); ?>
<br>
<input type="submit" value="Masuk" class="btn btn-success">

<?php $this->endWidget(); ?>

