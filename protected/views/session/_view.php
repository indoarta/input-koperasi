<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('session_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->session_id), array('view', 'id' => $data->session_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('time')); ?>:
	<?php echo GxHtml::encode($data->time); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ip_address')); ?>:
	<?php echo GxHtml::encode($data->ip_address); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('user_agent')); ?>:
	<?php echo GxHtml::encode($data->user_agent); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('username')); ?>:
	<?php echo GxHtml::encode($data->username); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('activity')); ?>:
	<?php echo GxHtml::encode($data->activity); ?>
	<br />

</div>