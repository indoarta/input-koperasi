<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-table"></i> Session</span>
	</div>
	<div class="mws-panel-body no-padding">
		<table id="tabelUser" class="mws-datatable-fn mws-table">
			<thead>
				<tr>
					<th>Aktivitas</th>
					<th>Waktu</th>
					<th>#</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$User = Session::model()->findAll();
				foreach($User as $m){
					echo "<tr>
						<td>".$m->activity."</td>
						<td style='text-align:center'>".$m->time."</td>
						<td style='text-align:center'><a href='".Yii::app()->request->baseUrl."/session/update/".$m->session_id."' class='btn btn-primary'>Edit</a></td>
					</tr>";
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<script>
	$(document).ready(function(){
		$("#tabelUser").dataTable({
			sPaginationType: "full_numbers"
		});
	});
</script>