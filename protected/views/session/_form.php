
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'session-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array('class'=>'mws-form')
));
?>	<div class="mws-form-inline">
		<div class="mws-form-row">
			<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
			<?php echo $form->errorSummary($model); ?>
		</div>

		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'time'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'time'); ?>
				<?php echo $form->error($model,'time'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'ip_address'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'ip_address',array('maxlength'=>'15')); ?>
				<?php echo $form->error($model,'ip_address'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'user_agent'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textArea($model, 'user_agent'); ?>
				<?php echo $form->error($model,'user_agent'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'username'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'username',array('maxlength'=>'20')); ?>
				<?php echo $form->error($model,'username'); ?>
			</div>
		</div>
		<div class="mws-form-row">
			<div class="mws-form-label"><?php echo $form->labelEx($model,'activity'); ?></div>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'activity',array('maxlength'=>'50')); ?>
				<?php echo $form->error($model,'activity'); ?>
			</div>
		</div>


		<div class="mws-form-row">
			<input type="submit" value="Save" class="btn btn-primary">
		</div>
	</div>
<?php
$this->endWidget();
?>
