<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'session_id'); ?>
		<?php echo $form->textField($model, 'session_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'time'); ?>
		<?php echo $form->textField($model, 'time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ip_address'); ?>
		<?php echo $form->textField($model, 'ip_address',array('maxlength'=>'15')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'user_agent'); ?>
		<?php echo $form->textArea($model, 'user_agent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'username'); ?>
		<?php echo $form->textField($model, 'username',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'activity'); ?>
		<?php echo $form->textField($model, 'activity',array('maxlength'=>'50')); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
