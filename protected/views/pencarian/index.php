<?php
/* @var $this PencarianController */

$this->breadcrumbs=array(
	'Pencarian',
);
?>
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span><i class="icon-search"></i> Pencarian Data Koperasi</span>
    </div>
    <div class="mws-panel-body no-padding">
		<form class="mws-form">
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<div class="mws-form-label">Kabupaten</div>
					<div class="mws-form-item">
						<select id="kabupaten">
							<option value="">( Semua )</option>
							<?php
							$kabs = Kabupaten::model()->findAll();
							foreach($kabs as $kab){
								echo "<option value='".$kab->kabupaten_id."'>".$kab->nama."</option>";
							}
							?>
						</select>
					</div>
				</div>
				<div class="mws-form-row">
					<div class="mws-form-label">Sektor</div>
					<div class="mws-form-item">
						<select id="sektor">
							<option value="">( Semua )</option>
							<?php
							$sekts = Sektor::model()->findAll("jenis = 'sektor'");
							foreach($sekts as $sektor){
								echo "<option value='".$sektor->sektor_id."'>".$sektor->nama."</option>";
							}
							?>
						</select>
					</div>
				</div>
				<div class="mws-form-row">
					<div class="mws-form-label">Kondisi</div>
					<div class="mws-form-item">
						<select id="kondisi">
							<option value="">( Semua )</option>
							<option value="aktif">Aktif</option>
							<option value="pasif">Pasif</option>
						</select>
					</div>
				</div>
				<div class="mws-form-row">
					<div class="mws-form-label">Nama Koperasi</div>
					<div class="mws-form-item">
						<input type="text" id="nama" >
					</div>
				</div>
				<div class="mws-form-row">
					<div class="mws-form-label"></div>
					<div class="mws-form-item">
						<button id="cari" class="btn btn-primary">
							<i class="icon-search"></i> Cari
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>    	
</div>

<div id="outputz"></div>

<script>
	$("#cari").click(function(){
		var kabupaten = $("#kabupaten").val();
		var sektor = $("#sektor").val();
		var kondisi = $("#kondisi").val();
		var nama = $("#nama").val();
		
		$.ajax({
			url : "<?php echo Yii::app()->request->baseUrl ?>/ajax/search",
			data : {
				kabupaten : kabupaten,
				sektor : sektor,
				kondisi : kondisi,
				nama : nama
			},
			type : "post",
			dataType : "html",
			success : function(msg){
				$("#outputz").html(msg);
			}
		});
		
		return false;
	});
	
	$(document).on("click", ".delete", function(){
		var data_id = $(this).attr("data_id");
		var nama = $(this).attr("nama");
		if(confirm("Apakah Anda yakin ingin menghapus Koperasi '"+nama+"'")){
			$.ajax({
				url : "<?php echo Yii::app()->request->baseUrl ?>/ajax/hapusKoperasi?data_id="+data_id,
				success : function(msg){
					$("#cari").click();
					alert("Koperasi '"+nama+"' telah dihapus.")
				}
			});
		}
	})
</script>