<div class="mws-panel grid_8">
	<div class="mws-panel-header">
    	<span>Hasil Pencarian</span>
    </div>
    <div class="mws-panel-body no-padding">
		<table class="mws-table">
			<thead>
				<tr>
					<th>No.</th>
					<th>Nama Koperasi</th>
					<th>Kondisi</th>
					<th>Sektor</th>
					<th>Kabupaten</th>
					<th>#</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$no = 1;
				foreach($hasil as $data){
				?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><a style="text-decoration: underline; color: #669900" href="<?php echo Yii::app()->request->baseUrl ?>/site/perkoperasi/?data_id=<?php echo $data->data_id; ?>">
						<?php echo $data->nama_koperasi; ?></a></td>
					<td><?php echo $data->kondisi_koperasi == 1?"Aktif":"Pasif"; ?></td>
					<td><?php echo Sektor::model()->findByPk($data->sektor_id)->nama; ?></td>
					<td><?php echo Kabupaten::model()->findByPk($data->kabupaten_id)->nama; ?></td>
					<td style="text-align: center">
						<nobr>
							<a class="btn btn-info" href="<?php echo Yii::app()->request->baseUrl . "/inputData/update/" . $data->data_id ?>">
								<i class="icon-edit"></i> Update
							</a>
							<button class="btn btn-danger delete" nama="<?php echo $data->nama_koperasi; ?>" data_id="<?php echo $data->data_id ?>">
								<i class="icon-trash"></i> Hapus
							</button>
						</nobr>
					</td>
				</tr>
				<?php
				$no++;
				}
				?>
			</tbody>
		</table>
	</div>    	
</div