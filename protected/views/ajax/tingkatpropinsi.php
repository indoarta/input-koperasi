<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-graph"></i> Chart Tingkat Propinsi</span>
	</div>
	<div class="mws-panel-body">
		<div id="mws-dashboard-chart" style="height: 222px;"></div>
	</div>
</div>

<script>
	<?php
	
	//kabupaten
	$sektorArray = array();
	$sektorTempArr = Sektor::model()->findAll("jenis = 'sektor'");
	$data = array();
	foreach($sektorTempArr as $sek){
		$sektorArray[$sek->sektor_id] = $sek->nama;
		$data[$sek->nama] = 0;
	}

	$dataUtama = DataUtama::model()->findAll("status_binaan_id = '2' OR status_binaan_id = '4'");
	foreach($dataUtama as $du){
		$data[$sektorArray[$du->sektor_id]] += 1;
	}
	?>
		
	$(document).ready(function() {
		if( $.plot ) {
			var data = [ 
				<?php
				$output = array();
				foreach ($data as $k => $v) {
					$output[] = '["'.$k.'", '. $v.']';
				}
				echo implode(",\n", $output);
				?>
			];

			$.plot("#mws-dashboard-chart", [ data ], {
				series: {
					bars: {
						show: true,
						barWidth: 0.6,
						align: "center"
					}
					,color: "#C20288"
				},
				
				xaxis: {
					mode: "categories",
					tickLength: 0
				},
				tooltip: true,
				tooltipOpts: {
					content: "Jumlah: %y unit"
				},
                grid: {
                    hoverable: true,
                    borderWidth: 0
                }
			});
        }
    });
</script>