<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('data_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->data_id), array('view', 'id' => $data->data_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nama_koperasi')); ?>:
	<?php echo GxHtml::encode($data->nama_koperasi); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('no_badan_hukum')); ?>:
	<?php echo GxHtml::encode($data->no_badan_hukum); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tanggal_badan_hukum')); ?>:
	<?php echo GxHtml::encode($data->tanggal_badan_hukum); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('no_perubahan_anggaran_dasar')); ?>:
	<?php echo GxHtml::encode($data->no_perubahan_anggaran_dasar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tanggal_perubahan_anggaran_dasar')); ?>:
	<?php echo GxHtml::encode($data->tanggal_perubahan_anggaran_dasar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('akte_pendirian')); ?>:
	<?php echo GxHtml::encode($data->akte_pendirian); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('anggaran_dasar')); ?>:
	<?php echo GxHtml::encode($data->anggaran_dasar); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('izin_usaha_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->izinUsaha)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('izin_usaha_lainnya')); ?>:
	<?php echo GxHtml::encode($data->izin_usaha_lainnya); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tahun_berdiri')); ?>:
	<?php echo GxHtml::encode($data->tahun_berdiri); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('jalan')); ?>:
	<?php echo GxHtml::encode($data->jalan); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('desa')); ?>:
	<?php echo GxHtml::encode($data->desa); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('kecamatan')); ?>:
	<?php echo GxHtml::encode($data->kecamatan); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('kabupaten_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->kabupaten)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('email')); ?>:
	<?php echo GxHtml::encode($data->email); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('telepon')); ?>:
	<?php echo GxHtml::encode($data->telepon); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fax')); ?>:
	<?php echo GxHtml::encode($data->fax); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('usaha_kemitraan_antar_koperasi')); ?>:
	<?php echo GxHtml::encode($data->usaha_kemitraan_antar_koperasi); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('jumlah_anggota')); ?>:
	<?php echo GxHtml::encode($data->jumlah_anggota); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('jumlah_calon_anggota')); ?>:
	<?php echo GxHtml::encode($data->jumlah_calon_anggota); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('jumlah_masyarakat_terlayani')); ?>:
	<?php echo GxHtml::encode($data->jumlah_masyarakat_terlayani); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('usaha_kemitraan_bums')); ?>:
	<?php echo GxHtml::encode($data->usaha_kemitraan_bums); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('usaha_kemitraan_bumd')); ?>:
	<?php echo GxHtml::encode($data->usaha_kemitraan_bumd); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status_kantor_lainnya')); ?>:
	<?php echo GxHtml::encode($data->status_kantor_lainnya); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status_kantor_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->statusKantor)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('kondisi_koperasi')); ?>:
	<?php echo GxHtml::encode($data->kondisi_koperasi); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status_binaan_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->statusBinaan)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('bidang_usaha_inti')); ?>:
	<?php echo GxHtml::encode($data->bidang_usaha_inti); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('usaha_kemitraan_bumn')); ?>:
	<?php echo GxHtml::encode($data->usaha_kemitraan_bumn); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('periode_1')); ?>:
	<?php echo GxHtml::encode($data->periode_1); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('periode_2')); ?>:
	<?php echo GxHtml::encode($data->periode_2); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ketua')); ?>:
	<?php echo GxHtml::encode($data->ketua); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ketua_cp')); ?>:
	<?php echo GxHtml::encode($data->ketua_cp); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sekretaris')); ?>:
	<?php echo GxHtml::encode($data->sekretaris); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sekretaris_cp')); ?>:
	<?php echo GxHtml::encode($data->sekretaris_cp); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('bendahara')); ?>:
	<?php echo GxHtml::encode($data->bendahara); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('bendahara_cp')); ?>:
	<?php echo GxHtml::encode($data->bendahara_cp); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('pembantu_umum')); ?>:
	<?php echo GxHtml::encode($data->pembantu_umum); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('pembantu_umum_cp')); ?>:
	<?php echo GxHtml::encode($data->pembantu_umum_cp); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('koordinator')); ?>:
	<?php echo GxHtml::encode($data->koordinator); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('koordinator_cp')); ?>:
	<?php echo GxHtml::encode($data->koordinator_cp); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('anggota')); ?>:
	<?php echo GxHtml::encode($data->anggota); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('anggota_cp')); ?>:
	<?php echo GxHtml::encode($data->anggota_cp); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ms_simpanan_pokok')); ?>:
	<?php echo GxHtml::encode($data->ms_simpanan_pokok); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ms_simpanan_wajib')); ?>:
	<?php echo GxHtml::encode($data->ms_simpanan_wajib); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ms_bantuan_hibah')); ?>:
	<?php echo GxHtml::encode($data->ms_bantuan_hibah); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ms_bantuan_sosial')); ?>:
	<?php echo GxHtml::encode($data->ms_bantuan_sosial); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ms_cadangan')); ?>:
	<?php echo GxHtml::encode($data->ms_cadangan); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ml_pinjaman_bank')); ?>:
	<?php echo GxHtml::encode($data->ml_pinjaman_bank); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ml_lemb_non_bank')); ?>:
	<?php echo GxHtml::encode($data->ml_lemb_non_bank); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ml_pinjaman_pihak_iii')); ?>:
	<?php echo GxHtml::encode($data->ml_pinjaman_pihak_iii); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('volume_des_2012')); ?>:
	<?php echo GxHtml::encode($data->volume_des_2012); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('volume_tahun_berjalan')); ?>:
	<?php echo GxHtml::encode($data->volume_tahun_berjalan); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('shu_des_2012')); ?>:
	<?php echo GxHtml::encode($data->shu_des_2012); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('shu_tahun_berjalan')); ?>:
	<?php echo GxHtml::encode($data->shu_tahun_berjalan); ?>
	<br />
	*/ ?>

</div>