<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Create'),
);
?>
<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-plus-sign"></i> Input Data Baru</span>
	</div>
	<div class="mws-panel-body no-padding">
		<?php
		$this->renderPartial('_form', array(
				'model' => $model,
				'buttons' => 'create'));
		?>
	</div>
</div>