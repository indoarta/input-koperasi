<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'data_id'); ?>
		<?php echo $form->textField($model, 'data_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'nama_koperasi'); ?>
		<?php echo $form->textField($model, 'nama_koperasi',array('maxlength'=>'100')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'no_badan_hukum'); ?>
		<?php echo $form->textField($model, 'no_badan_hukum',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'tanggal_badan_hukum'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'tanggal_badan_hukum',
			'value' => $model->tanggal_badan_hukum,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'no_perubahan_anggaran_dasar'); ?>
		<?php echo $form->textField($model, 'no_perubahan_anggaran_dasar',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'tanggal_perubahan_anggaran_dasar'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'tanggal_perubahan_anggaran_dasar',
			'value' => $model->tanggal_perubahan_anggaran_dasar,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'akte_pendirian'); ?>
		<?php echo $form->textField($model, 'akte_pendirian'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'anggaran_dasar'); ?>
		<?php echo $form->textField($model, 'anggaran_dasar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'izin_usaha_id'); ?>
		<?php echo $form->dropDownList($model, 'izin_usaha_id', GxHtml::listDataEx(IzinUsaha::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'izin_usaha_lainnya'); ?>
		<?php echo $form->textField($model, 'izin_usaha_lainnya',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'tahun_berdiri'); ?>
		<?php echo $form->textField($model, 'tahun_berdiri'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'jalan'); ?>
		<?php echo $form->textField($model, 'jalan',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'desa'); ?>
		<?php echo $form->textField($model, 'desa',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'kecamatan'); ?>
		<?php echo $form->textField($model, 'kecamatan',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'kabupaten_id'); ?>
		<?php echo $form->dropDownList($model, 'kabupaten_id', GxHtml::listDataEx(Kabupaten::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'email'); ?>
		<?php echo $form->textField($model, 'email',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'telepon'); ?>
		<?php echo $form->textField($model, 'telepon',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'fax'); ?>
		<?php echo $form->textField($model, 'fax',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'usaha_kemitraan_antar_koperasi'); ?>
		<?php echo $form->textField($model, 'usaha_kemitraan_antar_koperasi',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'jumlah_anggota'); ?>
		<?php echo $form->textField($model, 'jumlah_anggota'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'jumlah_calon_anggota'); ?>
		<?php echo $form->textField($model, 'jumlah_calon_anggota'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'jumlah_masyarakat_terlayani'); ?>
		<?php echo $form->textField($model, 'jumlah_masyarakat_terlayani'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'usaha_kemitraan_bums'); ?>
		<?php echo $form->textField($model, 'usaha_kemitraan_bums',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'usaha_kemitraan_bumd'); ?>
		<?php echo $form->textField($model, 'usaha_kemitraan_bumd',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'status_kantor_lainnya'); ?>
		<?php echo $form->textField($model, 'status_kantor_lainnya',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'status_kantor_id'); ?>
		<?php echo $form->dropDownList($model, 'status_kantor_id', GxHtml::listDataEx(StatusKantor::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'kondisi_koperasi'); ?>
		<?php echo $form->textField($model, 'kondisi_koperasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'status_binaan_id'); ?>
		<?php echo $form->dropDownList($model, 'status_binaan_id', GxHtml::listDataEx(StatusBinaan::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'bidang_usaha_inti'); ?>
		<?php echo $form->textField($model, 'bidang_usaha_inti',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'usaha_kemitraan_bumn'); ?>
		<?php echo $form->textField($model, 'usaha_kemitraan_bumn',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'periode_1'); ?>
		<?php echo $form->textField($model, 'periode_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'periode_2'); ?>
		<?php echo $form->textField($model, 'periode_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ketua'); ?>
		<?php echo $form->textField($model, 'ketua',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ketua_cp'); ?>
		<?php echo $form->textField($model, 'ketua_cp',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sekretaris'); ?>
		<?php echo $form->textField($model, 'sekretaris',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'sekretaris_cp'); ?>
		<?php echo $form->textField($model, 'sekretaris_cp',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'bendahara'); ?>
		<?php echo $form->textField($model, 'bendahara',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'bendahara_cp'); ?>
		<?php echo $form->textField($model, 'bendahara_cp',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'pembantu_umum'); ?>
		<?php echo $form->textField($model, 'pembantu_umum',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'pembantu_umum_cp'); ?>
		<?php echo $form->textField($model, 'pembantu_umum_cp',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'koordinator'); ?>
		<?php echo $form->textField($model, 'koordinator',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'koordinator_cp'); ?>
		<?php echo $form->textField($model, 'koordinator_cp',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'anggota'); ?>
		<?php echo $form->textField($model, 'anggota',array('maxlength'=>'50')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'anggota_cp'); ?>
		<?php echo $form->textField($model, 'anggota_cp',array('maxlength'=>'20')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ms_simpanan_pokok'); ?>
		<?php echo $form->textField($model, 'ms_simpanan_pokok',array('maxlength'=>'15')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ms_simpanan_wajib'); ?>
		<?php echo $form->textField($model, 'ms_simpanan_wajib',array('maxlength'=>'15')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ms_bantuan_hibah'); ?>
		<?php echo $form->textField($model, 'ms_bantuan_hibah',array('maxlength'=>'15')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ms_bantuan_sosial'); ?>
		<?php echo $form->textField($model, 'ms_bantuan_sosial',array('maxlength'=>'15')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ms_cadangan'); ?>
		<?php echo $form->textField($model, 'ms_cadangan',array('maxlength'=>'15')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ml_pinjaman_bank'); ?>
		<?php echo $form->textField($model, 'ml_pinjaman_bank',array('maxlength'=>'15')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ml_lemb_non_bank'); ?>
		<?php echo $form->textField($model, 'ml_lemb_non_bank',array('maxlength'=>'15')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ml_pinjaman_pihak_iii'); ?>
		<?php echo $form->textField($model, 'ml_pinjaman_pihak_iii',array('maxlength'=>'15')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'volume_des_2012'); ?>
		<?php echo $form->textField($model, 'volume_des_2012',array('maxlength'=>'15')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'volume_tahun_berjalan'); ?>
		<?php echo $form->textField($model, 'volume_tahun_berjalan',array('maxlength'=>'15')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'shu_des_2012'); ?>
		<?php echo $form->textField($model, 'shu_des_2012',array('maxlength'=>'15')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'shu_tahun_berjalan'); ?>
		<?php echo $form->textField($model, 'shu_tahun_berjalan',array('maxlength'=>'15')); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
