<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('data-utama-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'data-utama-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'data_id',
		'nama_koperasi',
		'no_badan_hukum',
		'tanggal_badan_hukum',
		'no_perubahan_anggaran_dasar',
		'tanggal_perubahan_anggaran_dasar',
		/*
		'akte_pendirian',
		'anggaran_dasar',
		array(
				'name'=>'izin_usaha_id',
				'value'=>'GxHtml::valueEx($data->izinUsaha)',
				'filter'=>GxHtml::listDataEx(IzinUsaha::model()->findAllAttributes(null, true)),
				),
		'izin_usaha_lainnya',
		'tahun_berdiri',
		'jalan',
		'desa',
		'kecamatan',
		array(
				'name'=>'kabupaten_id',
				'value'=>'GxHtml::valueEx($data->kabupaten)',
				'filter'=>GxHtml::listDataEx(Kabupaten::model()->findAllAttributes(null, true)),
				),
		'email',
		'telepon',
		'fax',
		'usaha_kemitraan_antar_koperasi',
		'jumlah_anggota',
		'jumlah_calon_anggota',
		'jumlah_masyarakat_terlayani',
		'usaha_kemitraan_bums',
		'usaha_kemitraan_bumd',
		'status_kantor_lainnya',
		array(
				'name'=>'status_kantor_id',
				'value'=>'GxHtml::valueEx($data->statusKantor)',
				'filter'=>GxHtml::listDataEx(StatusKantor::model()->findAllAttributes(null, true)),
				),
		'kondisi_koperasi',
		array(
				'name'=>'status_binaan_id',
				'value'=>'GxHtml::valueEx($data->statusBinaan)',
				'filter'=>GxHtml::listDataEx(StatusBinaan::model()->findAllAttributes(null, true)),
				),
		'bidang_usaha_inti',
		'usaha_kemitraan_bumn',
		'periode_1',
		'periode_2',
		'ketua',
		'ketua_cp',
		'sekretaris',
		'sekretaris_cp',
		'bendahara',
		'bendahara_cp',
		'pembantu_umum',
		'pembantu_umum_cp',
		'koordinator',
		'koordinator_cp',
		'anggota',
		'anggota_cp',
		'ms_simpanan_pokok',
		'ms_simpanan_wajib',
		'ms_bantuan_hibah',
		'ms_bantuan_sosial',
		'ms_cadangan',
		'ml_pinjaman_bank',
		'ml_lemb_non_bank',
		'ml_pinjaman_pihak_iii',
		'volume_des_2012',
		'volume_tahun_berjalan',
		'shu_des_2012',
		'shu_tahun_berjalan',
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>