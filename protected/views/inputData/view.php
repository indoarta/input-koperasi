<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->data_id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->data_id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'data_id',
'nama_koperasi',
'no_badan_hukum',
'tanggal_badan_hukum',
'no_perubahan_anggaran_dasar',
'tanggal_perubahan_anggaran_dasar',
'akte_pendirian',
'anggaran_dasar',
array(
			'name' => 'izinUsaha',
			'type' => 'raw',
			'value' => $model->izinUsaha !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->izinUsaha)), array('izinUsaha/view', 'id' => GxActiveRecord::extractPkValue($model->izinUsaha, true))) : null,
			),
'izin_usaha_lainnya',
'tahun_berdiri',
'jalan',
'desa',
'kecamatan',
array(
			'name' => 'kabupaten',
			'type' => 'raw',
			'value' => $model->kabupaten !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->kabupaten)), array('kabupaten/view', 'id' => GxActiveRecord::extractPkValue($model->kabupaten, true))) : null,
			),
'email',
'telepon',
'fax',
'usaha_kemitraan_antar_koperasi',
'jumlah_anggota',
'jumlah_calon_anggota',
'jumlah_masyarakat_terlayani',
'usaha_kemitraan_bums',
'usaha_kemitraan_bumd',
'status_kantor_lainnya',
array(
			'name' => 'statusKantor',
			'type' => 'raw',
			'value' => $model->statusKantor !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->statusKantor)), array('statusKantor/view', 'id' => GxActiveRecord::extractPkValue($model->statusKantor, true))) : null,
			),
'kondisi_koperasi',
array(
			'name' => 'statusBinaan',
			'type' => 'raw',
			'value' => $model->statusBinaan !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->statusBinaan)), array('statusBinaan/view', 'id' => GxActiveRecord::extractPkValue($model->statusBinaan, true))) : null,
			),
'bidang_usaha_inti',
'usaha_kemitraan_bumn',
'periode_1',
'periode_2',
'ketua',
'ketua_cp',
'sekretaris',
'sekretaris_cp',
'bendahara',
'bendahara_cp',
'pembantu_umum',
'pembantu_umum_cp',
'koordinator',
'koordinator_cp',
'anggota',
'anggota_cp',
'ms_simpanan_pokok',
'ms_simpanan_wajib',
'ms_bantuan_hibah',
'ms_bantuan_sosial',
'ms_cadangan',
'ml_pinjaman_bank',
'ml_lemb_non_bank',
'ml_pinjaman_pihak_iii',
'volume_des_2012',
'volume_tahun_berjalan',
'shu_des_2012',
'shu_tahun_berjalan',
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('bidangUsahas')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->bidangUsahas as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('bidangUsaha/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('pengelolas')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->pengelolas as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('pengelola/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>