<style>
	span.question {
  cursor: pointer;
  display: inline-block;
  width: 16px;
  height: 16px;
  background-color: #89A4CC;
  line-height: 16px;
  color: White;
  font-size: 13px;
  font-weight: bold;
  border-radius: 8px;
  text-align: center;
  position: relative;
}
span.question:hover { background-color: #3D6199; }
div.tooltip {
  background-color: #3D6199;
  color: White;
  position: absolute;
  left: 25px;
  top: -25px;
  z-index: 1000000;
  width: 250px;
  border-radius: 5px;
  opacity: 1;
}
div.tooltip:before {
  border-color: transparent #3D6199 transparent transparent;
  border-right: 6px solid #3D6199;
  border-style: solid;
  border-width: 6px 6px 6px 0px;
  content: "";
  display: block;
  height: 0;
  width: 0;
  line-height: 0;
  position: absolute;
  top: 40%;
  left: -6px;
}
div.tooltip p {
  margin: 10px;
  color: White;
}
</style>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'data-utama-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array("class" => "mws-form"),
));
?>
	<div class="mws-form-inline">
		<div class="mws-form-row">
			<div class="mws-form-item"> 
				<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
				<?php echo $form->errorSummary($model); ?>
			</div>
		</div>

		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'nama_koperasi',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'nama_koperasi',array('class'=>'form-control', 'style'=>'width:50%','maxlength'=>'100')); ?>
				<?php echo $form->error($model,'nama_koperasi'); ?>
				<span class="question" judul="Nama Koperasi">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<label class="mws-form-label">Sektor :</label>
			<div class="mws-form-item">
				<?php
				$sektor_id = $model->sektor_id;
				$sekts = Sektor::model()->findAll();
				foreach($sekts as $se){
					echo "<div><label><input type='radio' ".($sektor_id == $se->sektor_id?"checked":"")." name='sektor_id' class='sektor_check' value='".$se->sektor_id."'> ".$se->nama."</label></div>";
				}
				?>
			</div>
		</div>
		<script>
			$(document).ready(function(){
				$(".sektor_check").click(function(){
					var val = $(this).val();
					$(".sektor_tr").hide();
					$(".sektor_"+val).show();
				});
				$(".sektor_check:checked").click();
			})
		</script>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="col-sm-12">No. &amp; Tgl. Badan Hukum</h4>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'no_badan_hukum',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'no_badan_hukum',array('class'=>'form-control','maxlength'=>'50')); ?>
				<?php echo $form->error($model,'no_badan_hukum'); ?>
				<span class="question" judul="Nomor yang tertera dalam surat Badan Hukum Koperasi">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'tanggal_badan_hukum',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'tanggal_badan_hukum',array('class'=>'hasdatepicker form-control','maxlength'=>'50')); ?>
				<?php echo $form->error($model,'tanggal_badan_hukum'); ?>
				<span class="question" judul="Tanggal yang tertera dalam surat Badan Hukum Koperasi">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="col-sm-12">Perubahan Anggaran Dasar</h4>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'no_perubahan_anggaran_dasar',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'no_perubahan_anggaran_dasar',array('class'=>'form-control','maxlength'=>'50')); ?>
				<?php echo $form->error($model,'no_perubahan_anggaran_dasar'); ?>
				<span class="question" judul="Nomor yang tertera dalam surat Perubahan Anggaran Dasar">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'tanggal_perubahan_anggaran_dasar',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'tanggal_perubahan_anggaran_dasar',array('class'=>'hasdatepicker form-control','maxlength'=>'50')); ?>
				<?php echo $form->error($model,'tanggal_perubahan_anggaran_dasar'); ?>
				<span class="question" judul="Tanggal yang tertera dalam surat Perubahan Anggaran Dasar">?</span>
			</div>
		</div>
		<script>
			$(document).ready(function(){
				$(".hasdatepicker").datepicker({
					dateFormat : "yy-mm-dd"
				});
			});
		</script>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'akte_pendirian',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->dropDownList($model, 'akte_pendirian', array("0"=>"Tidak Ada", "1"=>"Ada"), array("style"=>"width:20%")); ?>
				<?php echo $form->error($model,'akte_pendirian'); ?>
				<span class="question" judul="Akte Pendirian">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'anggaran_dasar',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->dropDownList($model, 'anggaran_dasar', array("0"=>"Tidak Ada", "1"=>"Ada"), array("style"=>"width:20%")); ?>
				<?php echo $form->error($model,'anggaran_dasar'); ?>
				<span class="question" judul="Anggaran Dasar">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<label class="mws-form-label">Izin Usaha :</label>
			<div class="mws-form-item">
				<div style="display: inline-block;width: 70px">TDP : </div><?php echo $form->dropDownList($model, 'izin_usaha_tdp', array("0"=>"Tidak Ada", "1"=>"Ada"), array("style"=>"width:20%")); ?><br><br>
				<div style="display: inline-block;width: 70px">SIUP : </div><?php echo $form->dropDownList($model, 'izin_usaha_siup', array("0"=>"Tidak Ada", "1"=>"Ada"), array("style"=>"width:20%")); ?><br><br>
				<div style="display: inline-block;width: 70px">HO : </div><?php echo $form->dropDownList($model, 'izin_usaha_ho', array("0"=>"Tidak Ada", "1"=>"Ada"), array("style"=>"width:20%")); ?><br><br>
				<div style="display: inline-block;width: 70px">Lainnya : </div><?php echo $form->dropDownList($model, 'izin_usaha_lainnya', array("0"=>"Tidak Ada", "1"=>"Ada"), array("style"=>"width:20%")); ?><br>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'tahun_berdiri',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'tahun_berdiri',array('class'=>'form-control','maxlength'=>'4')); ?>
				<?php echo $form->error($model,'tahun_berdiri'); ?>
				
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="col-sm-12">Alamat Koperasi</h4>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'jalan',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'jalan',array('class'=>'form-control','maxlength'=>'50')); ?>
				<?php echo $form->error($model,'jalan'); ?>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'desa',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'desa',array('class'=>'form-control','maxlength'=>'50')); ?>
				<?php echo $form->error($model,'desa'); ?>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'kecamatan',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'kecamatan',array('class'=>'form-control','maxlength'=>'50')); ?>
				<?php echo $form->error($model,'kecamatan'); ?>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'kabupaten_id',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->dropDownList($model, 'kabupaten_id', GxHtml::listDataEx(Kabupaten::model()->findAllAttributes(null, true)), array("style"=>"width:40%")); ?>
				<?php echo $form->error($model,'kabupaten_id'); ?>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'email',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->emailField($model, 'email',array('class'=>'form-control','maxlength'=>'50')); ?>
				<?php echo $form->error($model,'email'); ?>
				<span class="question" judul="Alamat Email Koperasi">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'telepon',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->telField($model, 'telepon',array('class'=>'form-control','maxlength'=>'20')); ?>
				<?php echo $form->error($model,'telepon'); ?>
				<span class="question" judul="No Telp Koperasi">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'fax',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->telField($model, 'fax',array('class'=>'form-control','maxlength'=>'20')); ?>
				<?php echo $form->error($model,'fax'); ?>
				<span class="question" judul="No Fax Koperasi (Jika Ada)">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="col-sm-12">Status Kantor yg Ditempati</h4>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'status_kantor_id',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->dropDownList($model, 'status_kantor_id', GxHtml::listDataEx(StatusKantor::model()->findAllAttributes(null, true)), array("style"=>"width:40%")); ?>
				<?php echo $form->textField($model, 'status_kantor_lainnya',array('class'=>'form-control','maxlength'=>'20','style'=>"width:50%;display:none")); ?>
				<?php echo $form->error($model,'status_kantor_id'); ?>
				<span class="question" judul="Status kantor yg ditempati">?</span>
			</div>
		</div>
		<script>
			$("#DataUtama_status_kantor_id").change(function(){
				if($(this).val() == 3){
					//show
					$("#DataUtama_status_kantor_lainnya").css("display","inline");
				}else{
					$("#DataUtama_status_kantor_lainnya").css("display","none");
				}
			})
		</script>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'kondisi_koperasi',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->dropDownList($model, 'kondisi_koperasi', array("0"=>"Pasif", "1"=>"Aktif"), array("style"=>"width:40%")); ?>
				<?php echo $form->error($model,'kondisi_koperasi'); ?>
				<span class="question" judul="Kondisi Koperasi">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'status_binaan_id',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->dropDownList($model, 'status_binaan_id', GxHtml::listDataEx(StatusBinaan::model()->findAllAttributes(null, true)), array("style"=>"width:40%")); ?>
				<?php echo $form->error($model,'status_binaan_id'); ?>
				<span class="question" judul="Status Binaan">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="">Unit Usaha <span class="question" judul="Unit Usaha Koperasi (Minimal 4 Buah)">?</span></h4>
		</div>
		<div id="bidangUnit">
			<?php
			if($model->isNewRecord){
				for($i=1;$i<=4;$i++){
			?>
			<div class="mws-form-row">
				<label class="mws-form-label"><?php echo $i; ?></label>
				<div class="mws-form-item">
					<input type="text" name="bidangusaha[]" class="form-control" maxlength="30">
				</div>
			</div>
			<?php
				}
			}else{
				$bidangs = BidangUsaha::model()->findAllByAttributes(array("data_id"=>$model->primaryKey));
				$jml = 1;
				foreach($bidangs as $bidang){
					?>
			<div class="mws-form-row">
				<label class="mws-form-label"><?php echo $jml; ?></label>
				<div class="mws-form-item">
					<input value="<?php echo $bidang->nama; ?>" type="text" name="bidangusaha[]" class="bidangusaha form-control" maxlength="30">
				</div>
			</div>
					<?php
					$jml++;
				}
				for($i=$jml;$i<=4;$i++){
					?>
			<div class="mws-form-row">
				<label class="mws-form-label"><?php echo $i; ?></label>
				<div class="mws-form-item">
					<input value="" type="text" name="bidangusaha[]" class="bidangusaha form-control" maxlength="30">
				</div>
			</div>
					<?php
				}
			}
			?>
		</div>
		<div class="mws-form-row">
			<label class="mws-form-label"></label>
			<div class="mws-form-item">
				<button id="tambahBidang" class="btn btn-info">Tambah</button>
				<button id="kurangiBidang" class="btn btn-danger">Kurangi</button>
			</div>
		</div>
		<script>
			var isi = '<div class="mws-form-row">'+
			'	<label class="mws-form-label">';
			var isi2 = '</label>'+
			'	<div class="mws-form-item">'+
			'		<input type="text" class="form-control" name="bidangusaha[]" maxlength="30">'+
			'	</div>'+
			'</div>';
			$("#tambahBidang").click(function(){
				$("#bidangUnit").append(isi + ($("#bidangUnit .mws-form-row").length+1) + isi2);
				
				return false;
			});
			$("#kurangiBidang").click(function(){
				$("#bidangUnit .mws-form-row:last").remove();
				return false;
			});
		</script>
		<div class="mws-form-row">
			<h4 class="">Unit Usaha Inti <span class="question" judul="Unit Usaha Inti Koperasi">?</span></h4>
		</div>
		<div id="bidangUnitInti">
			<?php
			if($model->isNewRecord){
			?>
			<div class="mws-form-row">
				<label class="mws-form-label">1</label>
				<div class="mws-form-item">
					<input type="text" name="bidangusahainti[]" class="form-control" maxlength="30">
				</div>
			</div>
			<?php
			}else{
				$bidangs = BidangUsahaInti::model()->findAllByAttributes(array("data_id"=>$model->primaryKey));
				$jml = 1;
				foreach($bidangs as $bidang){
					?>
			<div class="mws-form-row">
				<label class="mws-form-label"><?php echo $jml; ?></label>
				<div class="mws-form-item">
					<input value="<?php echo $bidang->nama; ?>" type="text" name="bidangusahainti[]" class="bidangusahainti form-control" maxlength="30">
				</div>
			</div>
					<?php
					break;
				}
			}
			?>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="">Usaha Kementrian Koperasi</h4>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'usaha_kemitraan_bums',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'usaha_kemitraan_bums',array('class'=>'form-control','maxlength'=>'20')); ?>
				<?php echo $form->error($model,'usaha_kemitraan_bums'); ?>
				<span class="question" judul="Usaha BUMS yang bekerja sama dengan koperasi.">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'usaha_kemitraan_bumd',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'usaha_kemitraan_bumd',array('class'=>'form-control','maxlength'=>'20')); ?>
				<?php echo $form->error($model,'usaha_kemitraan_bumd'); ?>
				<span class="question" judul="Usaha BUMD yang bekerja sama dengan koperasi.">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'usaha_kemitraan_bumn',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'usaha_kemitraan_bumn',array('class'=>'form-control','maxlength'=>'20')); ?>
				<?php echo $form->error($model,'usaha_kemitraan_bumn'); ?>
				<span class="question" judul="Usaha BUMN yang bekerja sama dengan koperasi.">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'usaha_kemitraan_antar_koperasi',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'usaha_kemitraan_antar_koperasi',array('class'=>'form-control','maxlength'=>'20')); ?>
				<?php echo $form->error($model,'usaha_kemitraan_antar_koperasi'); ?>
				<span class="question" judul="Usaha kerja sama antar koperasi.">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'jumlah_anggota',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'jumlah_anggota',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'jumlah_anggota'); ?>
				<span class="question" judul="Jumlah Anggota Koperasi Keseluruhan.">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'jumlah_calon_anggota',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'jumlah_calon_anggota',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'jumlah_calon_anggota'); ?>
				<span class="question" judul="Jumlah Calon Anggota Koperasi.">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'jumlah_masyarakat_terlayani',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'jumlah_masyarakat_terlayani',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'jumlah_masyarakat_terlayani'); ?>
				<span class="question" judul="Jumlah Masyarakat yang dijangkau oleh koperasi.">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="">Pengurus Koperasi</h4>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'periode_1',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'periode_1',array('class'=>'form-control', 'style'=>"width:70px;display:inline", "maxlength"=>4)); ?> - 
				<?php echo $form->textField($model, 'periode_2',array('class'=>'form-control', 'style'=>"width:70px;display:inline", "maxlength"=>4)); ?>
				<?php echo $form->error($model,'periode_1'); ?>
				<?php echo $form->error($model,'periode_2'); ?>
				<span class="question" judul="Periode Kepemimpinan Koperasi Saat Ini.">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'ketua',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'ketua',array('class'=>'form-control','maxlength'=>'50','placeholder'=>'Nama', 'style'=>"width:50%;display:inline")); ?>
				<?php echo $form->textField($model, 'ketua_cp',array('class'=>'form-control','maxlength'=>'20','placeholder'=>'No Telp', 'style'=>"width:30%;display:inline")); ?>
				<?php echo $form->error($model,'ketua'); ?>
				<?php echo $form->error($model,'ketua_cp'); ?>
				<span class="question" judul="Ketua Koperasi Aktif pada Periode tertulis.">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'sekretaris',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'sekretaris',array('class'=>'form-control','maxlength'=>'50','placeholder'=>'Nama', 'style'=>"width:50%;display:inline")); ?>
				<?php echo $form->textField($model, 'sekretaris_cp',array('class'=>'form-control','maxlength'=>'20','placeholder'=>'No Telp', 'style'=>"width:30%;display:inline")); ?>
				<?php echo $form->error($model,'sekretaris'); ?>
				<?php echo $form->error($model,'sekretaris_cp'); ?>
				<span class="question" judul="Sekretaris Aktif pada Periode tertulis.">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'bendahara',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'bendahara',array('class'=>'form-control','maxlength'=>'50','placeholder'=>'Nama', 'style'=>"width:50%;display:inline")); ?>
				<?php echo $form->textField($model, 'bendahara_cp',array('class'=>'form-control','maxlength'=>'20','placeholder'=>'No Telp', 'style'=>"width:30%;display:inline")); ?>
				<?php echo $form->error($model,'bendahara'); ?>
				<?php echo $form->error($model,'bendahara_cp'); ?>
				<span class="question" judul="Bendahara Koperasi Aktif pada Periode tertulis.">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'pembantu_umum',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'pembantu_umum',array('class'=>'form-control','maxlength'=>'50','placeholder'=>'Nama', 'style'=>"width:50%;display:inline")); ?>
				<?php echo $form->textField($model, 'pembantu_umum_cp',array('class'=>'form-control','maxlength'=>'20','placeholder'=>'No Telp', 'style'=>"width:30%;display:inline")); ?>
				<?php echo $form->error($model,'pembantu_umum'); ?>
				<?php echo $form->error($model,'pembantu_umum_cp'); ?>
				<span class="question" judul="Pembantu Umum Aktif pada Periode tertulis.">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="">Pengawas Koperasi</h4>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'koordinator',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'koordinator',array('class'=>'form-control','maxlength'=>'50','placeholder'=>'Nama', 'style'=>"width:50%;display:inline")); ?>
				<?php echo $form->textField($model, 'koordinator_cp',array('class'=>'form-control','maxlength'=>'20','placeholder'=>'No Telp', 'style'=>"width:30%;display:inline")); ?>
				<?php echo $form->error($model,'koordinator'); ?>
				<?php echo $form->error($model,'koordinator_cp'); ?>
				<span class="question" judul="Koordinator Aktif pada Periode tertulis.">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'anggota',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'anggota',array('class'=>'form-control','maxlength'=>'50','placeholder'=>'Nama', 'style'=>"width:50%;display:inline")); ?>
				<?php echo $form->textField($model, 'anggota_cp',array('class'=>'form-control','maxlength'=>'20','placeholder'=>'No Telp', 'style'=>"width:30%;display:inline")); ?>
				<?php echo $form->error($model,'anggota'); ?>
				<?php echo $form->error($model,'anggota_cp'); ?>
				<span class="question" judul="Anggota Aktif pada Periode tertulis.">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="">Pengelola <span class="question" judul="Pengelola Koperasi.">?</span></h4>
		</div>
		<textarea id="pengelolahidden" style="display: none">
			<tr>
				<td class="nomor"></td>
				<td class="jenis_pengelola">
					<?php echo CHtml::dropDownList("jenis_pengelola[]", NULL, 
					GxHtml::listDataEx(JenisPengelola::model()->findAllAttributes(null, true)),array("class"=>"form-control m-b")) ?>
				</td>
				<td class="nama_bagian">
					<input type="text" name="nama_bagian[]" class="form-control" placeholder="Bagian" maxlength="30">
				</td>
				<td class="nama">
					<input type="text" name="nama[]" class="form-control" placeholder="Nama" maxlength="30">
				</td>
				<td class="kontak_person">
					<input type="text" name="kontak_person[]" class="form-control" placeholder="Telepon" maxlength="20">
				</td>
			</tr>
		</textarea>
		<div class="mws-form-row">
			<label class="mws-form-label"></label>
			<div class="mws-form-item">
				<table>
					<tbody id="tbody">
				<?php
				if($model->isNewRecord){
				?>
						<tr>
							<td class="nomor"></td>
							<td class="jenis_pengelola">
								<?php echo CHtml::dropDownList("jenis_pengelola[]", NULL, 
								GxHtml::listDataEx(JenisPengelola::model()->findAllAttributes(null, true)),array("class"=>"form-control m-b")) ?>
							</td>
							<td class="nama_bagian">
								<input type="text" name="nama_bagian[]" class="form-control" placeholder="Bagian" maxlength="30">
							</td>
							<td class="nama">
								<input type="text" name="nama[]" class="form-control" placeholder="Nama" maxlength="30">
							</td>
							<td class="kontak_person">
								<input type="text" name="kontak_person[]" class="form-control" placeholder="Telepon" maxlength="20">
							</td>
						</tr>
				<?php
				}else{
					$pengelolas = Pengelola::model()->findAllByAttributes(array("data_id"=>$model->primaryKey));
					$jml = 1;
					foreach($pengelolas as $pengelola){
						?>
						<tr>
							<td class="nomor"></td>
							<td class="jenis_pengelola">
								<?php echo CHtml::dropDownList("jenis_pengelola[]", $pengelola->jenis_pengelola_id, 
								GxHtml::listDataEx(JenisPengelola::model()->findAllAttributes(null, true)),array("class"=>"form-control m-b")) ?>
							</td>
							<td class="nama_bagian">
								<input value="<?php echo $pengelola->nama_bagian ?>" type="text" name="nama_bagian[]" class="form-control" placeholder="Bagian" maxlength="30">
							</td>
							<td class="nama">
								<input value="<?php echo $pengelola->nama ?>" type="text" name="nama[]" class="form-control" placeholder="Nama" maxlength="30">
							</td>
							<td class="kontak_person">
								<input value="<?php echo $pengelola->kontak_person ?>" type="text" name="kontak_person[]" class="form-control" placeholder="Telepon" maxlength="20">
							</td>
						</tr>
						<?php
						$jml++;
					}
				}
				?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="mws-form-row">
			<label class="mws-form-label"></label>
			<div class="mws-form-item">
				<button id="tambahPengelola" class="btn btn-info">Tambah</button>
				<button id="kurangiPengelola" class="btn btn-danger">Kurangi</button>
			</div>
		</div>
		<script>
			var isip = $("#pengelolahidden").val();
			
			$("#tambahPengelola").click(function(){
				$("#tbody").append(isip);
				
				return false;
			});
			$("#kurangiPengelola").click(function(){
				$("#tbody tr:last").remove();
				return false;
			});
		</script>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="">Modal Sendiri</h4>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'ms_simpanan_pokok',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'ms_simpanan_pokok',array('class'=>'form-control angka','maxlength'=>'15')); ?>
				<?php echo $form->error($model,'ms_simpanan_pokok'); ?>
				<span class="question" judul="Simpanan Pokok koperasi saat ini.">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'ms_simpanan_wajib',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'ms_simpanan_wajib',array('class'=>'form-control angka','maxlength'=>'15')); ?>
				<?php echo $form->error($model,'ms_simpanan_wajib'); ?>
				<span class="question" judul="Jumlah Simpanan Wajib koperasi saat ini. (Dalam Rupiah)">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'ms_bantuan_hibah',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'ms_bantuan_hibah',array('class'=>'form-control angka','maxlength'=>'15')); ?>
				<?php echo $form->error($model,'ms_bantuan_hibah'); ?>
				<span class="question" judul="Jumlah Bantuan Hibah koperasi saat ini. (Dalam Rupiah)">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'ms_bantuan_sosial',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'ms_bantuan_sosial',array('class'=>'form-control angka','maxlength'=>'15')); ?>
				<?php echo $form->error($model,'ms_bantuan_sosial'); ?>
				<span class="question" judul="Jumlah Bantuan Sosial koperasi saat ini. (Dalam Rupiah)">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'ms_cadangan',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'ms_cadangan',array('class'=>'form-control angka','maxlength'=>'15')); ?>
				<?php echo $form->error($model,'ms_cadangan'); ?>
				<span class="question" judul="Jumlah Cadangan koperasi saat ini (Dalam Rupiah).">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="">Modal Luar</h4>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'ml_pinjaman_bank',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'ml_pinjaman_bank',array('class'=>'form-control angka','maxlength'=>'15')); ?>
				<?php echo $form->error($model,'ml_pinjaman_bank'); ?>
				<span class="question" judul="Jumlah Pinjaman Ke Bank oleh koperasi saat ini. (Dalam Rupiah)">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'ml_lemb_non_bank',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'ml_lemb_non_bank',array('class'=>'form-control angka','maxlength'=>'15')); ?>
				<?php echo $form->error($model,'ml_lemb_non_bank'); ?>
				<span class="question" judul="Jumlah Pinjaman Ke Lembaga Non Bank oleh koperasi saat ini. (Dalam Rupiah)">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'ml_pinjaman_pihak_iii',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'ml_pinjaman_pihak_iii',array('class'=>'form-control angka','maxlength'=>'15')); ?>
				<?php echo $form->error($model,'ml_pinjaman_pihak_iii'); ?>
				<span class="question" judul="Jumlah Pinjaman Ke Pihak Ke-3 oleh koperasi saat ini. (Dalam Rupiah)">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="">Volume Usaha</h4>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'volume_des_2012',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'volume_des_2012',array('class'=>'form-control angka','maxlength'=>'15')); ?>
				<?php echo $form->error($model,'volume_des_2012'); ?>
				<span class="question" judul="Jumlah Volume Usaha Koperasi sampai Desember 2012. (Dalam Rupiah)">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'volume_tahun_berjalan',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'volume_tahun_berjalan',array('class'=>'form-control angka','maxlength'=>'15')); ?>
				<?php echo $form->error($model,'volume_tahun_berjalan'); ?>
				<span class="question" judul="Jumlah Volume Usaha Koperasi Tahun Berjalan. (Dalam Rupiah)">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="mws-form-row">
			<h4 class="">Sisa Hasil Usaha (SHU)</h4>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'shu_des_2012',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'shu_des_2012',array('class'=>'form-control angka','maxlength'=>'15')); ?>
				<?php echo $form->error($model,'shu_des_2012'); ?>
				<span class="question" judul="Jumlah Sisa Hasil Usaha Koperasi sampai Desember 2012. (Dalam Rupiah)">?</span>
			</div>
		</div>
		<div class="mws-form-row">
			<?php echo $form->labelEx($model,'shu_tahun_berjalan',array('class'=>'mws-form-label')); ?>
			<div class="mws-form-item">
				<?php echo $form->textField($model, 'shu_tahun_berjalan',array('class'=>'form-control angka','maxlength'=>'15')); ?>
				<?php echo $form->error($model,'shu_tahun_berjalan'); ?>
				<span class="question" judul="Jumlah Sisa Hasil Usaha Koperasi Tahun Berjalan. (Dalam Rupiah)">?</span>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		
		<div class="mws-form-row">
			<h4>Sarana Kantor</h4>
		</div>
		
		<table style="width: 100%" class="mws-table">
			<thead>
			<tr>
				<th style="width: 5%">No.</th>
				<th style="">Sarana Kantor</th>
				<th style="width: 20%">Kapasitas</th>
				<th style="width: 15%">Jml Unit</th>
			</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Kantor Koperasi</td>
					<td>
						<?php echo $form->textField($model, 'sarana_kantor_kapasitas',array('class'=>'form-control','maxlength'=>'10','style'=>'width:100%')); ?>
					</td>
					<td>
						<?php echo $form->textField($model, 'sarana_kantor',array('class'=>'form-control','maxlength'=>'10','style'=>'width:100%')); ?>
					</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Kendaraan Roda 4</td>
					<td>
						<?php echo $form->textField($model, 'sarana_mobil_kapasitas',array('class'=>'form-control','maxlength'=>'10','style'=>'width:100%')); ?>
					</td>
					<td>
						<?php echo $form->textField($model, 'sarana_mobil',array('class'=>'form-control','maxlength'=>'10','style'=>'width:100%')); ?>
					</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Sepeda Motor</td>
					<td>
						<?php echo $form->textField($model, 'sarana_sepeda_motor_kapasitas',array('class'=>'form-control','maxlength'=>'10','style'=>'width:100%')); ?>
					</td>
					<td>
						<?php echo $form->textField($model, 'sarana_sepeda_motor',array('class'=>'form-control','maxlength'=>'10','style'=>'width:100%')); ?>
					</td>
				</tr>
				<tr>
					<td>4</td>
					<td>Sepeda Angin</td>
					<td>
						<?php echo $form->textField($model, 'sarana_sepeda_angin_kapasitas',array('class'=>'form-control','maxlength'=>'10','style'=>'width:100%')); ?>
					</td>
					<td>
						<?php echo $form->textField($model, 'sarana_sepeda_angin',array('class'=>'form-control','maxlength'=>'10','style'=>'width:100%')); ?>
					</td>
				</tr>
				<tr>
					<td>5</td>
					<td>Lainnya</td>
					<td>
						<?php echo $form->textField($model, 'sarana_lainnya_kapasitas',array('class'=>'form-control','maxlength'=>'10','style'=>'width:100%')); ?>
					</td>
					<td>
						<?php echo $form->textField($model, 'sarana_lainnya',array('class'=>'form-control','maxlength'=>'10','style'=>'width:100%')); ?>
					</td>
				</tr>
			</tbody>
		</table>
		
		<div class="mws-form-row">
			<h4>Sektor</h4>
		</div>
		
		<table style="width: 100%" class="mws-table">
			<thead>
			<tr>
				<th style="width: 5%">No.</th>
				<th style="">Sektor</th>
				<th style="">Jenis Sarana</th>
				<th style="width: 20%">Kapasitas</th>
				<th style="width: 15%">Jml Unit</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$sektors = Sektor::model()->findAll();
			$no = 1;
			foreach($sektors as $sektor){
				$saranas = Sarana::model()->findAll("sektor_id = '{$sektor->sektor_id}' ");
				$i = 0;
				foreach($saranas as $sarana){
					$str = "";
					$nomer = "";
					if($i == 0 ){
						$nomer = $no;
						$str = $sektor->nama;
					}
					echo "
					<tr class='sektor_tr sektor_".$sektor->sektor_id." sarana_".$sarana->sarana_id."' style='display:none'>
						<td>".$nomer."</td>
						<td>".$str."</td>
						<td>".$sarana->nama."</td>
						<td>
						<input type='hidden' class='sarana_id' name='sarana_id[]' value='".$sarana->sarana_id."'>
						<input type='text' class='kapasitas' name='kapasitas[]' style='width:100%' class='form-control' maxlength='18'></td>
						<td><input type='text' name='jumlah_unit[]' style='width:100%' class='jumlah_unit form-control' maxlength='6'></td>
					</tr>";
					$i++;
				}
				$no ++;
			}
			?>
			</tbody>
		</table>
		
		<script>
			//script untuk mengupdate sektor data.
			<?php
				if(!$model->isNewRecord){
					$saranas = Sarana::model()->findAll("sektor_id = '".$model->sektor_id."'");
					foreach($saranas as $s){
						//echo $s->sarana_id."\n";
						$sar = SaranaUsaha::model()->find("data_id = '".$model->primaryKey."' AND sarana_id = '".$s->sarana_id."'");
						$kap = "";
						$jml = 0;
						if($sar != NULL){
							$kap = $sar->kapasitas;
							$jml = $sar->jumlah_unit;
						}
						echo "\$(\".sarana_".$s->sarana_id." .kapasitas\").val(\"".$kap."\");\n			";
						echo "\$(\".sarana_".$s->sarana_id." .jumlah_unit\").val(\"".$jml."\");\n			";
					}
				}
			?>
				
		</script>
		
		<div class="line line-dashed line-lg pull-in"></div>

		<div class="mws-form-row">
			<div class="mws-form-item"> 
				<button type="submit" class="btn btn-success">Simpan</button> 
				<button type="reset" class="btn btn-warning">Reset</button> 
				<a href="<?php  echo Yii::app()->request->baseUrl; ?>/<?php echo Yii::app()->controller->id; ?>" class="btn btn-danger">Kembali</a> 
			</div>
		</div>

		<script>
			/*$("select").each(function(){
				if(!$(this).hasClass("m-b")){
					$(this).select2();
				}
			});*/
		</script>
	</div>
<?php
$this->endWidget();
?>

<script>
	$("form").submit(function(){
		var string = $("form").serialize();
		$(".mws-form-row").removeClass("has-error");
		$.ajax({
			url : "<?php 
			if($model->isNewRecord){
				echo Yii::app()->request->baseUrl . "/inputData/ajaxCreate";
			}else{
				echo Yii::app()->request->baseUrl . "/inputData/ajaxUpdate/?id=".$model->primaryKey;
			}
			
			?>",
			data : string,
			type : "post",
			dataType : "json",
			success : function(msg){
				if(msg.success == "ok"){
					alert("Data Berhasil <?php echo $model->isNewRecord?"Ditambahkan":"Disimpan"; ?>");
					//window.location.reload();
					return;
				}
				var jml = 0;
				var arr = new Array();
				$.each(msg, function(index, value) {
					$("#DataUtama_"+index).closest(".mws-form-row").addClass("has-error")
					console.log(index + ", " + value);
					arr.push([index, value]);
					jml ++;
				});
				if(jml > 0){
					alert("Pada input Anda terdapat beberapa yang belum terisi.\nMohon isi kotak tersebut.");
					$("#DataUtama_"+arr[0][0]).focus();
				}
				console.log("Sukses");
			}
		});
		console.log(string);
		return false;
	});
	
	$(document).ready(function () {
		$("span.question").hover(
		function () {
			console.log("Haloooo" + $(this).attr("title"))
			$(this).append('<div class="tooltip"><p>'+$(this).attr("judul")+'</p></div>');
		}, function () {
			$("div.tooltip").remove();
		});
		
		$(".angka").priceFormat({
			prefix: '',
			thousandsSeparator: '.',
			centsLimit : 0
		});
	});
</script>