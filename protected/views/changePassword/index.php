<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-table"></i> Ganti Password</span>
	</div>
	<div class="mws-panel-body no-padding">
		<form class="mws-form" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/changepassword/save">
			<?php
			if(isset($oke)){
			?>
			<div class="mws-form-message success">
				Data berhasil diperbaharui
			</div>
			<?php
			}
			?>
			<?php
			if(isset($err)){
			?>
			<div class="mws-form-message error">
				Password lama salah.
			</div>
			<?php
			}
			?>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<div class="mws-form-label">
						Password Lama :
					</div>
					<div class="mws-form-item">
						<input type="password" id="password_lama" name="password_lama" >
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<div class="mws-form-label">
						Password Baru :
					</div>
					<div class="mws-form-item">
						<input type="password" id="password_baru" name="password_baru" >
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<div class="mws-form-label">
						Password Baru (Ulangi) :
					</div>
					<div class="mws-form-item">
						<input type="password" id="password_baru_2" name="password_baru_2" >
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<div class="mws-form-label">
						
					</div>
					<div class="mws-form-item">
						<button id="simpanBtn" class="btn btn-primary">Simpan</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	$("#simpanBtn").click(function(){
		if($("#password_baru").val() == $("#password_baru_2").val()){
			return true;
		}else{
			alert("Password baru harus sama.")
			return false;
		}
	});
</script>