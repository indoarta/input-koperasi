<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('kelas_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->kelas_id), array('view', 'id' => $data->kelas_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('kode')); ?>:
	<?php echo GxHtml::encode($data->kode); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('nama')); ?>:
	<?php echo GxHtml::encode($data->nama); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('jenis')); ?>:
	<?php echo GxHtml::encode($data->jenis); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('angkatan')); ?>:
	<?php echo GxHtml::encode($data->angkatan); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('parent_id')); ?>:
	<?php echo GxHtml::encode($data->parent_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ada_elektif')); ?>:
	<?php echo GxHtml::encode($data->ada_elektif); ?>
	<br />

</div>