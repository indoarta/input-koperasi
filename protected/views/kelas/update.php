<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
	Yii::t('app', 'Update'),
);
?>
<section class="panel"> 
	<header class="panel-heading">
		<span class="h4">Edit <?php echo GxHtml::encode($model->label()); ?></span>
	</header> 
	<div class="panel-body"> 
		<?php
		$this->renderPartial('_form', array(
				'model' => $model));
		?>	</div> 
</section>