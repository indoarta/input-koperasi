<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Create'),
);
?>
<section class="panel"> 
	<header class="panel-heading">
		<span class="h4">New <?php echo GxHtml::encode($model->label()); ?></span>
	</header> 
	<div class="panel-body"> 
		<?php
		$this->renderPartial('_form', array(
				'model' => $model,
				'buttons' => 'create'));
		?>	</div> 
</section>