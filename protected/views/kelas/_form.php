
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'kelas-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array("class" => "form-horizontal"),
));
?>
		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-2"> 
				<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
				<?php echo $form->errorSummary($model); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'kode',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model, 'kode',array('class'=>'form-control','maxlength'=>'10')); ?>
				<?php echo $form->error($model,'kode'); ?>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'nama',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model, 'nama',array('class'=>'form-control','maxlength'=>'100')); ?>
				<?php echo $form->error($model,'nama'); ?>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'jenis',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model, 'jenis',array('class'=>'form-control','maxlength'=>'10')); ?>
				<?php echo $form->error($model,'jenis'); ?>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'angkatan',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model, 'angkatan',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'angkatan'); ?>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'parent_id',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model, 'parent_id',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'parent_id'); ?>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'ada_elektif',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->textField($model, 'ada_elektif',array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'ada_elektif'); ?>
			</div>
		</div>
		<div class="line line-dashed line-lg pull-in"></div>


		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-2"> 
				<button type="submit" class="btn btn-success">Simpan</button> 
				<button type="reset" class="btn btn-warning">Reset</button> 
				<a href="<?php  echo Yii::app()->request->baseUrl; ?>/<?php echo Yii::app()->controller->id; ?>" class="btn btn-danger">Kembali</a> 
			</div>
		</div>

<?php
$this->endWidget();
?>
