<?php

$this->breadcrumbs = array(
	Kelas::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Kelas::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Kelas::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Kelas::label(2)); ?></h1>

<div class="wrapper bg-light pull-in b-b font-bold" style="margin-bottom: 18px"> 
	<a href="<?php echo Yii::app()->request->baseUrl; ?>/kelas/create" class="btn btn-primary"><i class="icon-plus"></i> Tambah Data</a> 
</div>

<?php $this->widget('ext.giix-core.widgets.DataTablesWidget', array(
	'dataProvider'=>$dataProvider,
)); 