<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'kelas_id'); ?>
		<?php echo $form->textField($model, 'kelas_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'kode'); ?>
		<?php echo $form->textField($model, 'kode',array('maxlength'=>'10')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'nama'); ?>
		<?php echo $form->textField($model, 'nama',array('maxlength'=>'100')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'jenis'); ?>
		<?php echo $form->textField($model, 'jenis',array('maxlength'=>'10')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'angkatan'); ?>
		<?php echo $form->textField($model, 'angkatan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'parent_id'); ?>
		<?php echo $form->textField($model, 'parent_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ada_elektif'); ?>
		<?php echo $form->textField($model, 'ada_elektif'); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
