<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-pencil"></i> Import Excel untuk Master Dosen</span>
	</div>
	<div class="mws-panel-body no-padding">
		<div class="mws-form-message info">
			Excel yang didukung dalam fasilitas import ini sementara adalah dengan format <b><i>.xls</i></b> (Excel 2003).<br>
			Fasilitas Import ini akan menambahkan data ke dalam sistem, bukan meng-edit data yang sudah ada.
		</div>
		<form name="formupload" method="post" class="mws-form"
			  enctype="multipart/form-data" action="<?php echo Yii::app()->request->baseUrl ?>/mahasiswa/upload">  
			<div class="mws-form-inline">
				<div class="mws-form-row">
					<label class="mws-form-label">Contoh File Excel</label>
					<div class="mws-form-item">
						<a href="<?php echo Yii::app()->request->baseUrl ?>/upload/contoh_mahasiswa.xls" class="btn btn-danger">Download</a>
					</div>
				</div>
				<div class="mws-form-row">
					<label class="mws-form-label">File Excel :</label>
					<div class="mws-form-item">
						<input name="picture" class="large" type="file" />  
					</div>
				</div>
				<div class="mws-form-row">
					<div class="mws-form-item">
						<input type="submit" name="upload" class="btn btn-primary" value="Upload" />  
					</div>
				</div>
			</div>
			
		</form>  
	</div>
</div>