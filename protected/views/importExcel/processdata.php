<script>
/*
* jQuery.ajaxQueue - A queue for ajax requests
* 
* (c) 2011 Corey Frang
* Dual licensed under the MIT and GPL licenses.
*
* Requires jQuery 1.5+
*/ 
(function($) {

// jQuery on an empty object, we are going to use this as our Queue
var ajaxQueue = $({});

$.ajaxQueue = function( ajaxOpts ) {
    var jqXHR,
        dfd = $.Deferred(),
        promise = dfd.promise();

    // queue our ajax request
    ajaxQueue.queue( doRequest );

    // add the abort method
    promise.abort = function( statusText ) {

        // proxy abort to the jqXHR if it is active
        if ( jqXHR ) {
            return jqXHR.abort( statusText );
        }

        // if there wasn't already a jqXHR we need to remove from queue
        var queue = ajaxQueue.queue(),
            index = $.inArray( doRequest, queue );

        if ( index > -1 ) {
            queue.splice( index, 1 );
        }

        // and then reject the deferred
        dfd.rejectWith( ajaxOpts.context || ajaxOpts, [ promise, statusText, "" ] );
        return promise;
    };

    // run the actual query
    function doRequest( next ) {
        jqXHR = $.ajax( ajaxOpts )
            .done( dfd.resolve )
            .fail( dfd.reject )
            .then( next, next );
    }

    return promise;
};

})(jQuery);
</script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/static/js/libs/jgrowl/jquery.jgrowl.js"></script>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/static/js/libs/jgrowl/jquery.jgrowl.css" rel="stylesheet" />

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-pencil"></i> Pengaturan</span>
	</div>
	<div class="mws-panel-body no-padding">
		<div class="mws-form-message info">
			Catatan : <br>
			- Untuk format tanggal menggunakan : YYYY-mm-dd, contoh : 2013-01-31
		</div>
		<form class="mws-form" action="form_layouts.html">
			
			<div class="mws-form-inline">
				<div class="mws-form-row bordered">
					<label class="mws-form-label">Mulai Import dari Nomor :</label>
						<div class="mws-form-item">
							<input id="awal" type="text" class="mws-spinner" value="3">
						</div>
				</div>
				<div class="mws-form-row bordered">
					<label class="mws-form-label">Progress Upload :</label>
						<div class="mws-form-item">
							<div class="mws-progressbar"></div>
						</div>
				</div>
			</div>
			<div class="mws-button-row">
				<input type="button" value="Proses" id="submit" class="btn btn-danger">
				<input type="reset" value="Reset" class="btn ">
			</div>
		</form>
	</div>
</div>

<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-pencil"></i> Import Excel - Proses Excel</span>
	</div>
	<div class="mws-panel-body no-padding">
		<?php
		
		function getColumn(){
			$array = array(
				"" => "-",
				'nama_koperasi' => Yii::t('app', 'Nama Koperasi'),
				'sektor_id' => Yii::t('app', 'Sektor Koperasi'),
				'no_badan_hukum' => Yii::t('app', 'No Badan Hukum'),
				'tanggal_badan_hukum' => Yii::t('app', 'Tanggal Badan Hukum'),
				'no_perubahan_anggaran_dasar' => Yii::t('app', 'No Perubahan Anggaran Dasar'),
				'tanggal_perubahan_anggaran_dasar' => Yii::t('app', 'Tanggal Perubahan Anggaran Dasar'),
				'akte_pendirian' => Yii::t('app', 'Akte Pendirian'),
				'anggaran_dasar' => Yii::t('app', 'Anggaran Dasar'),
				'izin_usaha_tdp' => Yii::t('app', 'Izin Usaha TDP'),
				'izin_usaha_siup' => Yii::t('app', 'Izin Usaha SIUP'),
				'izin_usaha_ho' => Yii::t('app', 'Izin Usaha HO'),
				'izin_usaha_lainnya' => Yii::t('app', 'Izin Usaha Lainnya'),
				'tahun_berdiri' => Yii::t('app', 'Tahun Berdiri'),
				'jalan' => Yii::t('app', 'Jalan'),
				'desa' => Yii::t('app', 'Desa'),
				'kecamatan' => Yii::t('app', 'Kecamatan'),
				'kabupaten_id' => Yii::t('app', 'Kabupaten'), //KHUSUS
				
				'telepon' => Yii::t('app', 'Telepon'),
				'fax' => Yii::t('app', 'Fax'),
				'email' => Yii::t('app', 'Email'),
				
				'status_kantor_id' => Yii::t('app', 'Status Kantor'),
				'status_kantor_lainnya' => Yii::t('app', 'Status Kantor Lainnya'),
				'kondisi_koperasi' => Yii::t('app', 'Kondisi Koperasi'),
				'status_binaan_id' => Yii::t('app', 'Status Binaan'),
				
				'unit_usaha' => Yii::t('app', 'Unit Usaha'),
				'unit_usaha_inti' => Yii::t('app', 'Unit Usaha Inti'),
				
				'usaha_kemitraan_bumn' => Yii::t('app', 'Usaha Kemitraan BUMN'),
				'usaha_kemitraan_bumd' => Yii::t('app', 'Usaha Kemitraan BUMD'),
				'usaha_kemitraan_bums' => Yii::t('app', 'Usaha Kemitraan BUMS'),
				'usaha_kemitraan_antar_koperasi' => Yii::t('app', 'Usaha Kemitraan Antar Koperasi'),
				
				'jumlah_anggota' => Yii::t('app', 'Jumlah Anggota'),
				'jumlah_calon_anggota' => Yii::t('app', 'Jumlah Calon Anggota'),
				'jumlah_masyarakat_terlayani' => Yii::t('app', 'Jumlah Masyarakat Terlayani'),
				
				'periode' => Yii::t('app', 'Periode'), //KHUSUS
				'jumlah_pengurus' => Yii::t('app', 'Jumlah Pengurus'),
				'jumlah_manager' => Yii::t('app', 'Jumlah Manager'),
				'jumlah_pengawas' => Yii::t('app', 'Jumlah Pengawas'),
				'jumlah_karyawan' => Yii::t('app', 'Jumlah Karyawan'),
				
				'ketua' => Yii::t('app', 'Ketua'),
				'ketua_cp' => Yii::t('app', 'No. Telp Ketua'),
				'sekretaris' => Yii::t('app', 'Sekretaris'),
				'sekretaris_cp' => Yii::t('app', 'No. Telp Sekretaris'),
				'bendahara' => Yii::t('app', 'Bendahara'),
				'bendahara_cp' => Yii::t('app', 'No. Telp Bendahara'),
				'pembantu_umum' => Yii::t('app', 'Pembantu Umum'),
				'pembantu_umum_cp' => Yii::t('app', 'No. Telp Pembantu Umum'),
				'koordinator' => Yii::t('app', 'Koordinator'),
				'koordinator_cp' => Yii::t('app', 'No. Telp Koordinator'),
				'anggota' => Yii::t('app', 'Anggota'),
				'anggota_cp' => Yii::t('app', 'No. Telp Anggota'),
				
				'ms_simpanan_pokok' => Yii::t('app', 'Simpanan Pokok'),
				'ms_simpanan_wajib' => Yii::t('app', 'Simpanan Wajib'),
				'ms_bantuan_hibah' => Yii::t('app', 'Bantuan Hibah'),
				'ms_bantuan_sosial' => Yii::t('app', 'Bantuan Sosial'),
				'ms_cadangan' => Yii::t('app', 'Cadangan'),
				'ml_pinjaman_bank' => Yii::t('app', 'Pinjaman Bank'),
				'ml_lemb_non_bank' => Yii::t('app', 'Lemb Non Bank'),
				'ml_pinjaman_pihak_iii' => Yii::t('app', 'Pinjaman Pihak III'),
				'volume_des_2012' => Yii::t('app', 'Volume Des 2012'),
				'volume_tahun_berjalan' => Yii::t('app', 'Volume Tahun Berjalan'),
				'shu_des_2012' => Yii::t('app', 'SHU Des 2012'),
				'shu_tahun_berjalan' => Yii::t('app', 'SHU Tahun Berjalan'),
				
				'sarana_kantor' => Yii::t('app', 'Kantor Koperasi'),
				'sarana_mobil' => Yii::t('app', 'Kendaraan Roda 4'),
				'sarana_sepeda_motor' => Yii::t('app', 'Sepeda Motor'),
				'sarana_sepeda_angin' => Yii::t('app', 'Sepeda Angin'),
				'sarana_lainnya' => Yii::t('app', 'Lainnya'),
			);
			
			
			
			//$mdl = new DataUtama();
			//$array = $array + $mdl->attributeLabels();
			
			$str = "<select class='column' style='width:100%'>";
			foreach($array as $key => $value){
				$str .= "<option value='".$key."'>".$value."</option>";
			}
			$arrr = array();
			$sektors = Sektor::model()->findAll();
			foreach ($sektors as $sektor) {
				$str .= "<optgroup label='".$sektor->nama."'>";
				$saranas = Sarana::model()->findAllByAttributes(array("sektor_id"=>$sektor->sektor_id, "tipe"=>"p"));
				foreach ($saranas as $sarana) {
					//$arrr[$sarana->sarana_id] = $sarana->nama;
					$str .= "<option value='sarana_".$sarana->sarana_id."'>".$sarana->nama."</option>";
				}
				$str .= "</optgroup>";
			}
			$str .= "</select>";
			return $str;
		}
		
		if(isset(Yii::app()->session["excel"])){
			$filename = Yii::app()->session["excel"];
			
			$dir = Yii::getPathOfAlias('ext.phpexcelreader');
			
			$filepath = $dir . DIRECTORY_SEPARATOR . 'excelreader.php';

			include($filepath);
			
			$data = new Spreadsheet_Excel_Reader($filename);
			
			echo $data->dump(true,true);
			
			?>
		<script>
			$(".excel").addClass("mws-table");
			var jml = -1;
			$("thead th").each(function(){
				$(this).html("<?php echo getColumn(); ?>");
				var select = $(this).find("select");
				if(jml >= 0){
					select.val(select.find('option').eq(jml).val());
				}
				jml++;
			});
		</script>
			<?php
		}
		
		?>
		<!--
		<table class="mws-table">
			<thead>
				<tr>
					<th>Rendering engine</th>
					<th>Browser</th>
					<th>Platform(s)</th>
					<th>Engine version</th>
					<th>CSS grade</th>
				</tr>
			</thead>
			
		</table>
		-->
	</div>
</div>

<script>
	$(document).ready(function(){
		$(".mws-progressbar").progressbar({ value: 0, showValue: true });
		$("#submit").click(function(){
			var i=0;
			var indeks = 1;
			var awal = parseInt($("#awal").val());
			var maks = $("tbody tr").length - awal;
			$("tbody tr").each(function(){
				if(i>=awal-1){
					var persen = Math.round(indeks/maks*100);
					processTR($(this), persen);
					
					indeks++;
				}
				i++;
			})
			
			return false;
		});
	});
	
	var currentError = "";
	
	function processTR(tr, persen){
		var num = 0;
		var arrayColumn = new Array();
		$("thead select").each(function(){
			arrayColumn[num] = $(this).val();
			num++;
		});
		var data = new Object();
		
		data.sektor_id = 'Pertambangan';
		data.nama_koperasi = 'Harap Isi Nama Koperasi';
		data.no_badan_hukum = '';
		data.tanggal_badan_hukum = '';
		data.no_perubahan_anggaran_dasar = '';
		data.tanggal_perubahan_anggaran_dasar = '';
		data.akte_pendirian = 'Tidak Ada';
		data.anggaran_dasar = 'Tidak Ada';
		data.izin_usaha_tdp = 'Tidak Ada';
		data.izin_usaha_siup = 'Tidak Ada';
		data.izin_usaha_ho = 'Tidak Ada';
		data.izin_usaha_lainnya = 'Tidak Ada';
		data.tahun_berdiri = '';
		data.jalan = '';
		data.desa = '';
		data.kecamatan = '';
		data.kabupaten_id = 'Surabaya';
		data.email = '-';
		data.telepon = '-';
		data.fax = '-';
		data.jumlah_anggota = '0';
		data.jumlah_calon_anggota = '0';
		data.jumlah_masyarakat_terlayani = '0';
		data.usaha_kemitraan_bumn = '';
		data.usaha_kemitraan_bums = '';
		data.usaha_kemitraan_bumd = '';
		data.usaha_kemitraan_antar_koperasi = '';
		data.status_kantor_id = '1';
		data.status_kantor_lainnya = '';
		
		data.kondisi_koperasi = '1';
		data.status_binaan_id = '1';
		
		data.periode = '0-0';
		data.jumlah_pengurus = '0';
		data.jumlah_pengawas = '0';
		data.jumlah_manager = '0';
		data.jumlah_karyawan = '0';
		data.ketua = '';
		data.ketua_cp = '';
		data.sekretaris = '';
		data.sekretaris_cp = '';
		data.bendahara = '';
		data.bendahara_cp = '';
		data.pembantu_umum = '';
		data.pembantu_umum_cp = '';
		data.koordinator = '';
		data.koordinator_cp = '';
		data.anggota = '';
		data.anggota_cp = '';
		data.ms_simpanan_pokok = '0';
		data.ms_simpanan_wajib = '0';
		data.ms_bantuan_hibah = '0';
		data.ms_bantuan_sosial = '0';
		data.ms_cadangan = '0';
		data.ml_pinjaman_bank = '0';
		data.ml_lemb_non_bank = '0';
		data.ml_pinjaman_pihak_iii = '0';
		data.volume_des_2012 = '0';
		data.volume_tahun_berjalan = '0';
		data.shu_des_2012 = '0';
		data.shu_tahun_berjalan = '0';
		data.is_skala_propinsi = '1';
		
		<?php
		$sektors = Sektor::model()->findAll();
		foreach ($sektors as $sektor) {
			$saranas = Sarana::model()->findAllByAttributes(array("sektor_id"=>$sektor->sektor_id, "tipe"=>"p"));
			foreach ($saranas as $sarana) {
				echo "data.sarana_".$sarana->sarana_id." = '0';\n		";
			}
		}
		?>
		
		for(var i=0;i<arrayColumn.length;i++){
			if(arrayColumn[i] != ""){
				var nilai = $.trim(tr.find("td:eq("+(i-1)+")").text());
				if(nilai != ""){
					data[arrayColumn[i]] = nilai;
				}
			}
		}
		$.ajaxQueue({
			url : "<?php echo Yii::app()->request->baseUrl; ?>/importExcel/ajaxcreate/",
			dataType : "json",
			type : "post",
			data : data,
			success : function(e){
				if(e.status == "OK"){
					console.log("Sukses Create Baru.");
				}else{
					console.log("Error Create Baru.");
					var obj = e.error;
					var str = "";
					for (var key in obj) {
						str += obj[key]+ "<br>";
					}
					
					var errorstr = "Error : <br>"+str;
					
					if(currentError != errorstr){
						$.jGrowl(errorstr, {
							position: "bottom-right"
						});
						currentError = errorstr;
					}
				}
				
				$(".mws-progressbar").progressbar({ value: persen, showValue: true });
			}
		});
	}
</script>