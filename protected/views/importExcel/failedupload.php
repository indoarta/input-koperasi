<div class="mws-panel grid_8">
	<div class="mws-panel-header">
		<span><i class="icon-pencil"></i> Import Excel - Informasi</span>
	</div>
	<div class="mws-panel-body no-padding">
		<div class="mws-form-inline">
		<div class="mws-form-row">
		Maaf, Import Excel gagal, silakan ulangi lagi.<br>
		<?php echo $error; ?>
		<br>
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/admin/other/import/" class="btn">Kembali</a>
		</div>
		</div>
	</div>
</div>