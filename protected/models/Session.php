<?php

Yii::import('application.models._base.BaseSession');

class Session extends BaseSession
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public static function addSession($activity){
		if(Yii::app()->session["userid"] != ""){
			$user = User::model()->findByPk(Yii::app()->session["userid"]);
			$sess = new Session();
			$sess->activity = $activity;
			$sess->time = date("Y-m-d H:i:s");
			$sess->username = $user->username;
			$sess->user_agent = $_SERVER['HTTP_USER_AGENT'];
			$sess->ip_address = $_SERVER['REMOTE_ADDR'];
			if(!$sess->save()){
				var_dump($sess->errors);
			}
		}
	}
}