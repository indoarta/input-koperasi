<?php

/**
 * This is the model base class for the table "sektor".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Sektor".
 *
 * Columns in table "sektor" available as properties of the model,
 * followed by relations of table "sektor" available as properties of the model.
 *
 * @property integer $sektor_id
 * @property string $nama
 * @property string $jenis
 *
 * @property Sarana[] $saranas
 */
abstract class BaseSektor extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'sektor';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Sektor|Sektors', $n);
	}

	public static function representingColumn() {
		return 'nama';
	}

	public function rules() {
		return array(
			array('nama', 'required'),
			array('nama', 'length', 'max'=>30),
			array('jenis', 'length', 'max'=>10),
			array('jenis', 'default', 'setOnEmpty' => true, 'value' => null),
			array('sektor_id, nama, jenis', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'saranas' => array(self::HAS_MANY, 'Sarana', 'sektor_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'sektor_id' => Yii::t('app', 'Sektor'),
			'nama' => Yii::t('app', 'Nama'),
			'jenis' => Yii::t('app', 'Jenis'),
			'saranas' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('sektor_id', $this->sektor_id);
		$criteria->compare('nama', $this->nama, true);
		$criteria->compare('jenis', $this->jenis, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}