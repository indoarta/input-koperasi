<?php

/**
 * This is the model base class for the table "status_kantor".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "StatusKantor".
 *
 * Columns in table "status_kantor" available as properties of the model,
 * followed by relations of table "status_kantor" available as properties of the model.
 *
 * @property integer $status_kantor_id
 * @property string $nama
 *
 * @property DataUtama[] $dataUtamas
 */
abstract class BaseStatusKantor extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'status_kantor';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'StatusKantor|StatusKantors', $n);
	}

	public static function representingColumn() {
		return 'nama';
	}

	public function rules() {
		return array(
			array('nama', 'required'),
			array('nama', 'length', 'max'=>30),
			array('status_kantor_id, nama', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'dataUtamas' => array(self::HAS_MANY, 'DataUtama', 'status_kantor_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'status_kantor_id' => Yii::t('app', 'Status Kantor'),
			'nama' => Yii::t('app', 'Nama'),
			'dataUtamas' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('status_kantor_id', $this->status_kantor_id);
		$criteria->compare('nama', $this->nama, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}