<?php

/**
 * This is the model base class for the table "pengelola".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Pengelola".
 *
 * Columns in table "pengelola" available as properties of the model,
 * followed by relations of table "pengelola" available as properties of the model.
 *
 * @property integer $pengelola_id
 * @property integer $data_id
 * @property integer $jenis_pengelola_id
 * @property string $nama_bagian
 * @property string $nama
 * @property string $kontak_person
 *
 * @property JenisPengelola $jenisPengelola
 * @property DataUtama $data
 */
abstract class BasePengelola extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'pengelola';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Pengelola|Pengelolas', $n);
	}

	public static function representingColumn() {
		return 'nama_bagian';
	}

	public function rules() {
		return array(
			array('data_id, jenis_pengelola_id, nama_bagian, nama, kontak_person', 'required'),
			array('data_id, jenis_pengelola_id', 'numerical', 'integerOnly'=>true),
			array('nama_bagian', 'length', 'max'=>30),
			array('nama', 'length', 'max'=>50),
			array('kontak_person', 'length', 'max'=>20),
			array('pengelola_id, data_id, jenis_pengelola_id, nama_bagian, nama, kontak_person', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'jenisPengelola' => array(self::BELONGS_TO, 'JenisPengelola', 'jenis_pengelola_id'),
			'data' => array(self::BELONGS_TO, 'DataUtama', 'data_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'pengelola_id' => Yii::t('app', 'Pengelola'),
			'data_id' => null,
			'jenis_pengelola_id' => null,
			'nama_bagian' => Yii::t('app', 'Nama Bagian'),
			'nama' => Yii::t('app', 'Nama'),
			'kontak_person' => Yii::t('app', 'Kontak Person'),
			'jenisPengelola' => null,
			'data' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('pengelola_id', $this->pengelola_id);
		$criteria->compare('data_id', $this->data_id);
		$criteria->compare('jenis_pengelola_id', $this->jenis_pengelola_id);
		$criteria->compare('nama_bagian', $this->nama_bagian, true);
		$criteria->compare('nama', $this->nama, true);
		$criteria->compare('kontak_person', $this->kontak_person, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}