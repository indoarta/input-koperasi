<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user = User::model()->findByAttributes(array("username"=>$this->username, "password"=>md5($this->password)));
		if($user != NULL){
			Yii::app()->session["userid"] = $user->user_id;
			Session::addSession("Login");
			User::login();
			$this->errorCode=self::ERROR_NONE;
		}else{
			$this->errorCode=self::ERROR_UNKNOWN_IDENTITY;
		}
		return !$this->errorCode;
	}
}