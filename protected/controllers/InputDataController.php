<?php

class InputDataController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'DataUtama'),
		));
	}
	
	public function simpan($model){
		$model->ms_simpanan_pokok = str_replace(".", "", $model->ms_simpanan_pokok);
		$model->ms_simpanan_wajib = str_replace(".", "", $model->ms_simpanan_wajib);
		$model->ms_bantuan_hibah = str_replace(".", "", $model->ms_bantuan_hibah);
		$model->ms_bantuan_sosial = str_replace(".", "", $model->ms_bantuan_sosial);
		$model->ms_cadangan = str_replace(".", "", $model->ms_cadangan);
		$model->ml_pinjaman_bank = str_replace(".", "", $model->ml_pinjaman_bank);
		$model->ml_lemb_non_bank = str_replace(".", "", $model->ml_lemb_non_bank);
		$model->ml_pinjaman_pihak_iii = str_replace(".", "", $model->ml_pinjaman_pihak_iii);
		$model->volume_des_2012 = str_replace(".", "", $model->volume_des_2012);
		$model->volume_tahun_berjalan = str_replace(".", "", $model->volume_tahun_berjalan);
		$model->shu_des_2012 = str_replace(".", "", $model->shu_des_2012);
		$model->shu_tahun_berjalan = str_replace(".", "", $model->shu_tahun_berjalan);

		$model->sektor_id = $_POST['sektor_id'];

		if ($model->save()) {
			//hapus semua data awal
			BidangUsaha::model()->deleteAll("data_id = '".$model->primaryKey."'");
			BidangUsahaInti::model()->deleteAll("data_id = '".$model->primaryKey."'");
			Pengelola::model()->deleteAll("data_id = '".$model->primaryKey."'");
			SaranaUsaha::model()->deleteAll("data_id = '".$model->primaryKey."'");
			
			echo json_encode(array("success"=>"ok"));
			$primary = $model->primaryKey;
			//echo "Primary : ".$primary."=";

			//get bidangusaha
			$bidangusaha = $_POST['bidangusaha'];

			foreach($bidangusaha as $b){
				$bu = new BidangUsaha();
				$bu->data_id = $primary;
				$bu->nama = $b;
				if(!$bu->save()){
					//echo json_encode($bu->errors);
				}
			}
			//get bidangusahainti
			$bidangusahainti = $_POST['bidangusahainti'];

			foreach($bidangusahainti as $b){
				$bu = new BidangUsahaInti();
				$bu->data_id = $primary;
				$bu->nama = $b;
				if(!$bu->save()){
					//echo json_encode($bu->errors);
				}
			}
			//get jenis_pengelola
			$jenispengelola = $_POST['jenis_pengelola'];
			$namabagian = $_POST['nama_bagian'];
			$nama = $_POST['nama'];
			$kontak_person = $_POST['kontak_person'];

			$jml_pengurus = 0;
			$jml_manager = 0;
			$jml_karyawan = 0;
			$jml_pengawas = 0;

			if($model->ketua != NULL){
				$jml_pengurus ++;
			}
			if($model->sekretaris != NULL){
				$jml_pengurus ++;
			}
			if($model->bendahara != NULL){
				$jml_pengurus ++;
			}
			if($model->pembantu_umum != NULL){
				$jml_pengurus ++;
			}

			if($model->koordinator != NULL){
				$jml_pengawas ++;
			}
			if($model->anggota != NULL){
				$jml_pengawas ++;
			}

			//var_dump($jenispengelola);
			for($i=0;$i<count($jenispengelola);$i++){
				$jenis = $jenispengelola[$i];
				$namabag = $namabagian[$i];
				$n = $nama[$i];
				$kontak = $kontak_person[$i];

				//echo $jenis."->".$namabag."->".$n."->".$kontak."||";

				if($n == ""){
					continue;
				}

				if($jenis == 1 || $jenis == 2){
					$jml_manager++;
				}else if($jenis == 3){
					$jml_karyawan++;
				}

				$pengelola = new Pengelola();
				$pengelola->data_id = $primary;
				$pengelola->jenis_pengelola_id = $jenis;
				$pengelola->nama_bagian = $namabag;
				$pengelola->nama = $n;
				$pengelola->kontak_person = $kontak;
				if(!$pengelola->save()){
					//echo json_encode($pengelola->errors);
				}
			}

			$newModel = DataUtama::model()->findByPk($primary);
			$newModel->jumlah_pengurus = $jml_pengurus;
			$newModel->jumlah_manager = $jml_manager;
			$newModel->jumlah_karyawan = $jml_karyawan;
			$newModel->jumlah_pengawas = $jml_pengawas;
			$newModel->save();

			//sarana
			$sarana_id = $_POST['sarana_id'];
			$kapasitas = $_POST['kapasitas'];
			$jumlah_unit = $_POST['jumlah_unit'];
			for($i=0;$i<count($sarana_id);$i++){
				$s = $sarana_id[$i];
				$k = $kapasitas[$i];
				$j = $jumlah_unit[$i];

				//echo $s."->".$k."->".$j."||";

				if($j != ""){
					//echo $s."SIPP||";
					$sh = new SaranaUsaha();
					$sh->data_id = $primary;
					$sh->sarana_id = $s;
					$sh->kapasitas = $k;
					$sh->jumlah_unit = $j;
					if(!$sh->save()){
						//echo json_encode($sh->errors);
					}
				}
			}
		}else{
			$err = $model->errors;
			$err['success'] = "no";
			echo json_encode($err);
		}
	}
	
	public function actionAjaxCreate() {
		$model = new DataUtama;

		if (isset($_POST['DataUtama'])) {
			$model->setAttributes($_POST['DataUtama']);
			
			$this->simpan($model);
			
			Session::addSession("Insert Data Koperasi");
		}
	}
	
	public function actionAjaxUpdate() {
		$id = $_GET['id'];
		$model = DataUtama::model()->findByPk($id);

		if (isset($_POST['DataUtama'])) {
			$model->setAttributes($_POST['DataUtama']);
			
			$this->simpan($model);
			
			Session::addSession("Update Data Koperasi, ID:".$id);
		}
	}

	public function actionCreate() {
		$model = new DataUtama;

		//$model->is_skala_propinsi = 0;

		if (isset($_POST['DataUtama'])) {
			$model->setAttributes($_POST['DataUtama']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->data_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
	
	public function actionCreate2() {
		$model = new DataUtama;

		//$model->is_skala_propinsi = 1;
		
		if (isset($_POST['DataUtama'])) {
			$model->setAttributes($_POST['DataUtama']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->data_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'DataUtama');


		if (isset($_POST['DataUtama'])) {
			$model->setAttributes($_POST['DataUtama']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->data_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'DataUtama')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = DataUtama::model()->findAll();
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new DataUtama('search');
		$model->unsetAttributes();

		if (isset($_GET['DataUtama']))
			$model->setAttributes($_GET['DataUtama']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}