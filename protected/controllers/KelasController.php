<?php

class KelasController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Kelas'),
		));
	}

	public function actionCreate() {
		$model = new Kelas;


		if (isset($_POST['Kelas'])) {
			$model->setAttributes($_POST['Kelas']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->kelas_id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Kelas');


		if (isset($_POST['Kelas'])) {
			$model->setAttributes($_POST['Kelas']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->kelas_id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Kelas')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = Kelas::model()->findAll();
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Kelas('search');
		$model->unsetAttributes();

		if (isset($_GET['Kelas']))
			$model->setAttributes($_GET['Kelas']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}