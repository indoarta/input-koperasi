<?php

class ModuleController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Module'),
		));
	}

	public function actionCreate() {
		$model = new Module;


		if (isset($_POST['Module'])) {
			$model->setAttributes($_POST['Module']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('index'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Module');


		if (isset($_POST['Module'])) {
			$model->setAttributes($_POST['Module']);

			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Module')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$this->render('index');
	}

	public function actionAdmin() {
		$model = new Module('search');
		$model->unsetAttributes();

		if (isset($_GET['Module']))
			$model->setAttributes($_GET['Module']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}