<?php

class LaporanController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	private function loadPHPExcelLib($reportName){
		spl_autoload_unregister(array('YiiBase', 'autoload'));
		$phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes.PHPExcel');
		//include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
		include($phpExcelPath . DIRECTORY_SEPARATOR . 'IOFactory.php');
		spl_autoload_register(array('YiiBase', 'autoload'));

		$reportPath = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . "report" . DIRECTORY_SEPARATOR;

		return PHPExcel_IOFactory::load($reportPath . $reportName . ".xlsx");
	}
	
	private function generateReport($objPHPExcel, $report, $type, $isRedirect = TRUE){
		$outputPath = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . "output" . DIRECTORY_SEPARATOR;
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save($outputPath . $report . ".xls");

		if($isRedirect){
			header("Location: " . Yii::app()->request->baseUrl . "/output/" . $report . ".xls");
		}
		return "/output/" . $report . ".xls";
	}
	
	private function getHeaderStyle(){
		$headerBorder = array(
			/*'fill' 	=> array(
				'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
				'color'		=> array('argb' => 'FF000000')
			),*/
			'borders' => array(
				//'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
				'allborders' => array('style' => PHPExcel_Style_Border::BORDER_NONE)
				//'bottom'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
				//'right'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
				//'left'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
				//'top'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
			),
		 );
		
		return $headerBorder;
	}
	
	private function getNormalStyle(){
		$normalBorder = array(
			'fill' 	=> array(
				'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
				'color'		=> array('argb' => 'FFFFFFFF')
			),
			'borders' => array(
				'bottom'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'right'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'left'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'top'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
			),
			'font' => array(
				'bold' => false,
				'size' => 12,
				
			)
		 );
		return $normalBorder;
	}
	
	public function actionPreview() {
		$type = $_GET["type"];
		if($type == 1){
			$report = $this->actionLaporan1(FALSE);
		}else if($type == 2){
			$report = $this->actionLaporan2(FALSE);
		}else if($type == 3){
			$report = $this->actionLaporan3(FALSE);
		}else if($type == 4){
			$report = $this->actionLaporan4(FALSE, $_GET['data_id']);
		}else if($type == 11){
			$report = $this->actionLaporan11(FALSE);
		}else if($type == 20){
			$report = $this->actionLaporan20(FALSE);
		}else if($type == 21){
			$report = $this->actionLaporan21(FALSE);
		}
		
		$this->renderPartial("preview", array("filename"=>Yii::getPathOfAlias('webroot').$report));
	}
	
	public function actionLaporan1($is_redirect = TRUE) {
		$reportName = "laporan_umum";
		
		Session::addSession("Generate Report.");
		
		$objPHPExcel = $this->loadPHPExcelLib($reportName);
		$objPHPExcel->setActiveSheetIndex(0);
		$activeSheet = $objPHPExcel->getActiveSheet();
		
		//cetak sarana
		$kolom = 56;
		$sektors = Sektor::model()->findAll();
		$arraySarana = array();
		foreach($sektors as $sektor){
			$saranas = Sarana::model()->findAllByAttributes(array("sektor_id"=>$sektor->sektor_id));
			$arraySarana[] = $saranas;
			$activeSheet->getCellByColumnAndRow($kolom, 4)->setValue($sektor->nama);
			$style = $activeSheet->getStyleByColumnAndRow($kolom, 4);
			$style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$style->applyFromArray($this->getHeaderStyle());
			$kolomStart = $kolom;
			foreach($saranas as $sarana){
				$activeSheet->getCellByColumnAndRow($kolom, 5)->setValue($sarana->nama);
				$style = $activeSheet->getStyleByColumnAndRow($kolom, 5);
				$style->getAlignment()->setWrapText(true); 
				$style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$style->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$style->applyFromArray($this->getHeaderStyle());
				$kolom++;
			}
			$activeSheet->mergeCellsByColumnAndRow($kolomStart, 4, $kolom-1, 4);
		}
		
		$baris = 6;
		$dataUtamaArray = DataUtama::model()->findAll("status_binaan_id = 1 OR status_binaan_id = 3");
		foreach($dataUtamaArray as $d){
			//$d = new DataUtama();
			$activeSheet->getCellByColumnAndRow(0, $baris)->setValue($baris-2);
			$activeSheet->getCellByColumnAndRow(1, $baris)->setValue($d->nama_koperasi);
			$activeSheet->getCellByColumnAndRow(2, $baris)->setValue(Sektor::model()->findByPk($d->sektor_id)->nama);
			$activeSheet->getCellByColumnAndRow(3, $baris)->setValue($d->no_badan_hukum);
			$activeSheet->getCellByColumnAndRow(4, $baris)->setValue($d->tanggal_badan_hukum);
			$activeSheet->getCellByColumnAndRow(5, $baris)->setValue($d->no_perubahan_anggaran_dasar);
			$activeSheet->getCellByColumnAndRow(6, $baris)->setValue($d->tanggal_perubahan_anggaran_dasar);
			$activeSheet->getCellByColumnAndRow(7, $baris)->setValue($d->akte_pendirian==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(8, $baris)->setValue($d->anggaran_dasar==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(9, $baris)->setValue($d->izin_usaha_tdp==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(10, $baris)->setValue($d->izin_usaha_siup==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(11, $baris)->setValue($d->izin_usaha_ho==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(12, $baris)->setValue($d->izin_usaha_lainnya==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(13, $baris)->setValue($d->tahun_berdiri);
			$activeSheet->getCellByColumnAndRow(14, $baris)->setValue($d->jalan);
			$activeSheet->getCellByColumnAndRow(15, $baris)->setValue($d->desa);
			$activeSheet->getCellByColumnAndRow(16, $baris)->setValue($d->kecamatan);
			$activeSheet->getCellByColumnAndRow(17, $baris)->setValue(Kabupaten::model()->findByPk($d->kabupaten_id)->nama);
			$activeSheet->getCellByColumnAndRow(18, $baris)->setValue($d->telepon);
			$activeSheet->getCellByColumnAndRow(19, $baris)->setValue($d->fax);
			$activeSheet->getCellByColumnAndRow(20, $baris)->setValue($d->email);
			$activeSheet->getCellByColumnAndRow(21, $baris)->setValue($d->status_kantor_id);
			$activeSheet->getCellByColumnAndRow(22, $baris)->setValue($d->status_kantor_lainnya);
			$activeSheet->getCellByColumnAndRow(23, $baris)->setValue($d->kondisi_koperasi==1?"A":"P");
			$activeSheet->getCellByColumnAndRow(24, $baris)->setValue($d->status_binaan_id);
			$unitUsaha = BidangUsaha::model()->findAll("data_id = '".$d->data_id."'");
			$unitArr = array();
			foreach($unitUsaha as $unit){
				$unitArr[] = $unit->nama;
			}
			$unitUsahaInti = BidangUsahaInti::model()->findAll("data_id = '".$d->data_id."'");
			$unitIntiArr = array();
			foreach($unitUsahaInti as $unit){
				$unitIntiArr[] = $unit->nama;
			}
			$activeSheet->getCellByColumnAndRow(25, $baris)->setValue(implode(";", $unitArr));
			$activeSheet->getCellByColumnAndRow(26, $baris)->setValue(implode(";", $unitIntiArr));
			$activeSheet->getCellByColumnAndRow(27, $baris)->setValue($d->usaha_kemitraan_bumn);
			$activeSheet->getCellByColumnAndRow(28, $baris)->setValue($d->usaha_kemitraan_bumd);
			$activeSheet->getCellByColumnAndRow(29, $baris)->setValue($d->usaha_kemitraan_bums);
			$activeSheet->getCellByColumnAndRow(30, $baris)->setValue($d->usaha_kemitraan_antar_koperasi);
			$activeSheet->getCellByColumnAndRow(31, $baris)->setValue($d->jumlah_anggota);
			$activeSheet->getCellByColumnAndRow(32, $baris)->setValue($d->jumlah_calon_anggota);
			$activeSheet->getCellByColumnAndRow(33, $baris)->setValue($d->jumlah_masyarakat_terlayani);
			$activeSheet->getCellByColumnAndRow(34, $baris)->setValue($d->periode_1."-".$d->periode_2);
			$activeSheet->getCellByColumnAndRow(35, $baris)->setValue($d->jumlah_pengurus);
			$activeSheet->getCellByColumnAndRow(36, $baris)->setValue($d->jumlah_manager);
			$activeSheet->getCellByColumnAndRow(37, $baris)->setValue($d->jumlah_pengawas);
			$activeSheet->getCellByColumnAndRow(38, $baris)->setValue($d->jumlah_karyawan);
			
			$activeSheet->getCellByColumnAndRow(39, $baris)->setValue($d->ms_simpanan_pokok);
			$activeSheet->getCellByColumnAndRow(40, $baris)->setValue($d->ms_simpanan_wajib);
			$activeSheet->getCellByColumnAndRow(41, $baris)->setValue($d->ms_bantuan_hibah);
			$activeSheet->getCellByColumnAndRow(42, $baris)->setValue($d->ms_bantuan_sosial);
			$activeSheet->getCellByColumnAndRow(43, $baris)->setValue($d->ms_cadangan);
			$activeSheet->getCellByColumnAndRow(44, $baris)->setValue($d->ml_pinjaman_bank);
			$activeSheet->getCellByColumnAndRow(45, $baris)->setValue($d->ml_lemb_non_bank);
			$activeSheet->getCellByColumnAndRow(46, $baris)->setValue($d->ml_pinjaman_pihak_iii);
			
			$activeSheet->getCellByColumnAndRow(47, $baris)->setValue($d->volume_des_2012);
			$activeSheet->getCellByColumnAndRow(48, $baris)->setValue($d->volume_tahun_berjalan);
		
			$activeSheet->getCellByColumnAndRow(49, $baris)->setValue($d->shu_des_2012);
			$activeSheet->getCellByColumnAndRow(50, $baris)->setValue($d->shu_tahun_berjalan);
			
			$activeSheet->getCellByColumnAndRow(51, $baris)->setValue($d->sarana_kantor);
			$activeSheet->getCellByColumnAndRow(52, $baris)->setValue($d->sarana_mobil);
			$activeSheet->getCellByColumnAndRow(53, $baris)->setValue($d->sarana_sepeda_motor);
			$activeSheet->getCellByColumnAndRow(54, $baris)->setValue($d->sarana_sepeda_angin);
			$activeSheet->getCellByColumnAndRow(55, $baris)->setValue($d->sarana_lainnya);
			
			$i = 0;
			$k = 56;
			foreach($sektors as $sektor){
				foreach($arraySarana[$i] as $sarana){
					$jumlah = 0;
					$su = SaranaUsaha::model()->findByAttributes(array("data_id"=>$d->primaryKey, "sarana_id"=>$sarana->sarana_id));
					if($su != NULL){
						$jumlah = $su->jumlah_unit;
					}
					$activeSheet->getCellByColumnAndRow($k, $baris)->setValue($jumlah);
					$k++;
				}
				$i++;
			}
			
			//$activeSheet->insertNewColumnBeforeByIndex($baris);
			$baris++;
		}
		
		$activeSheet->getStyle(
			'A4:' . 
			$activeSheet->getHighestColumn() . 
			$activeSheet->getHighestRow()
		)->applyFromArray($this->getHeaderStyle());
		
		$report = $reportName . date("_Y_m_d_H_i_s");
		//generate the report
		return $this->generateReport($objPHPExcel, $report, "xls", $is_redirect);
	}
	
	public function actionLaporan2($is_redirect = TRUE) {
		$reportName = "laporan_umum";
		
		Session::addSession("Generate Report.");
		
		$objPHPExcel = $this->loadPHPExcelLib($reportName);
		$objPHPExcel->setActiveSheetIndex(0);
		$activeSheet = $objPHPExcel->getActiveSheet();
		
		//cetak sarana
		$kolom = 56;
		$sektors = Sektor::model()->findAll();
		$arraySarana = array();
		foreach($sektors as $sektor){
			$saranas = Sarana::model()->findAllByAttributes(array("sektor_id"=>$sektor->sektor_id));
			$arraySarana[] = $saranas;
			$activeSheet->getCellByColumnAndRow($kolom, 4)->setValue($sektor->nama);
			$style = $activeSheet->getStyleByColumnAndRow($kolom, 4);
			$style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$style->applyFromArray($this->getHeaderStyle());
			$kolomStart = $kolom;
			foreach($saranas as $sarana){
				$activeSheet->getCellByColumnAndRow($kolom, 5)->setValue($sarana->nama);
				$style = $activeSheet->getStyleByColumnAndRow($kolom, 5);
				$style->getAlignment()->setWrapText(true); 
				$style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$style->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$style->applyFromArray($this->getHeaderStyle());
				$kolom++;
			}
			$activeSheet->mergeCellsByColumnAndRow($kolomStart, 4, $kolom-1, 4);
		}
		
		$baris = 6;
		$dataUtamaArray = DataUtama::model()->findAll("status_binaan_id = 2 OR status_binaan_id = 4");
		foreach($dataUtamaArray as $d){
			//$d = new DataUtama();
			$activeSheet->getCellByColumnAndRow(0, $baris)->setValue($baris-2);
			$activeSheet->getCellByColumnAndRow(1, $baris)->setValue($d->nama_koperasi);
			$activeSheet->getCellByColumnAndRow(2, $baris)->setValue(Sektor::model()->findByPk($d->sektor_id)->nama);
			$activeSheet->getCellByColumnAndRow(3, $baris)->setValue($d->no_badan_hukum);
			$activeSheet->getCellByColumnAndRow(4, $baris)->setValue($d->tanggal_badan_hukum);
			$activeSheet->getCellByColumnAndRow(5, $baris)->setValue($d->no_perubahan_anggaran_dasar);
			$activeSheet->getCellByColumnAndRow(6, $baris)->setValue($d->tanggal_perubahan_anggaran_dasar);
			$activeSheet->getCellByColumnAndRow(7, $baris)->setValue($d->akte_pendirian==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(8, $baris)->setValue($d->anggaran_dasar==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(9, $baris)->setValue($d->izin_usaha_tdp==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(10, $baris)->setValue($d->izin_usaha_siup==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(11, $baris)->setValue($d->izin_usaha_ho==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(12, $baris)->setValue($d->izin_usaha_lainnya==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(13, $baris)->setValue($d->tahun_berdiri);
			$activeSheet->getCellByColumnAndRow(14, $baris)->setValue($d->jalan);
			$activeSheet->getCellByColumnAndRow(15, $baris)->setValue($d->desa);
			$activeSheet->getCellByColumnAndRow(16, $baris)->setValue($d->kecamatan);
			$activeSheet->getCellByColumnAndRow(17, $baris)->setValue(Kabupaten::model()->findByPk($d->kabupaten_id)->nama);
			$activeSheet->getCellByColumnAndRow(18, $baris)->setValue($d->telepon);
			$activeSheet->getCellByColumnAndRow(19, $baris)->setValue($d->fax);
			$activeSheet->getCellByColumnAndRow(20, $baris)->setValue($d->email);
			$activeSheet->getCellByColumnAndRow(21, $baris)->setValue($d->status_kantor_id);
			$activeSheet->getCellByColumnAndRow(22, $baris)->setValue($d->status_kantor_lainnya);
			$activeSheet->getCellByColumnAndRow(23, $baris)->setValue($d->kondisi_koperasi==1?"A":"P");
			$activeSheet->getCellByColumnAndRow(24, $baris)->setValue($d->status_binaan_id);
			$unitUsaha = BidangUsaha::model()->findAll("data_id = '".$d->data_id."'");
			$unitArr = array();
			foreach($unitUsaha as $unit){
				$unitArr[] = $unit->nama;
			}
			$unitUsahaInti = BidangUsahaInti::model()->findAll("data_id = '".$d->data_id."'");
			$unitIntiArr = array();
			foreach($unitUsahaInti as $unit){
				$unitIntiArr[] = $unit->nama;
			}
			$activeSheet->getCellByColumnAndRow(25, $baris)->setValue(implode(";", $unitArr));
			$activeSheet->getCellByColumnAndRow(26, $baris)->setValue(implode(";", $unitIntiArr));
			$activeSheet->getCellByColumnAndRow(27, $baris)->setValue($d->usaha_kemitraan_bumn);
			$activeSheet->getCellByColumnAndRow(28, $baris)->setValue($d->usaha_kemitraan_bumd);
			$activeSheet->getCellByColumnAndRow(29, $baris)->setValue($d->usaha_kemitraan_bums);
			$activeSheet->getCellByColumnAndRow(30, $baris)->setValue($d->usaha_kemitraan_antar_koperasi);
			$activeSheet->getCellByColumnAndRow(31, $baris)->setValue($d->jumlah_anggota);
			$activeSheet->getCellByColumnAndRow(32, $baris)->setValue($d->jumlah_calon_anggota);
			$activeSheet->getCellByColumnAndRow(33, $baris)->setValue($d->jumlah_masyarakat_terlayani);
			$activeSheet->getCellByColumnAndRow(34, $baris)->setValue($d->periode_1."-".$d->periode_2);
			$activeSheet->getCellByColumnAndRow(35, $baris)->setValue($d->jumlah_pengurus);
			$activeSheet->getCellByColumnAndRow(36, $baris)->setValue($d->jumlah_manager);
			$activeSheet->getCellByColumnAndRow(37, $baris)->setValue($d->jumlah_pengawas);
			$activeSheet->getCellByColumnAndRow(38, $baris)->setValue($d->jumlah_karyawan);
			
			$activeSheet->getCellByColumnAndRow(39, $baris)->setValue($d->ms_simpanan_pokok);
			$activeSheet->getCellByColumnAndRow(40, $baris)->setValue($d->ms_simpanan_wajib);
			$activeSheet->getCellByColumnAndRow(41, $baris)->setValue($d->ms_bantuan_hibah);
			$activeSheet->getCellByColumnAndRow(42, $baris)->setValue($d->ms_bantuan_sosial);
			$activeSheet->getCellByColumnAndRow(43, $baris)->setValue($d->ms_cadangan);
			$activeSheet->getCellByColumnAndRow(44, $baris)->setValue($d->ml_pinjaman_bank);
			$activeSheet->getCellByColumnAndRow(45, $baris)->setValue($d->ml_lemb_non_bank);
			$activeSheet->getCellByColumnAndRow(46, $baris)->setValue($d->ml_pinjaman_pihak_iii);
			
			$activeSheet->getCellByColumnAndRow(47, $baris)->setValue($d->volume_des_2012);
			$activeSheet->getCellByColumnAndRow(48, $baris)->setValue($d->volume_tahun_berjalan);
		
			$activeSheet->getCellByColumnAndRow(49, $baris)->setValue($d->shu_des_2012);
			$activeSheet->getCellByColumnAndRow(50, $baris)->setValue($d->shu_tahun_berjalan);
			
			$activeSheet->getCellByColumnAndRow(51, $baris)->setValue($d->sarana_kantor);
			$activeSheet->getCellByColumnAndRow(52, $baris)->setValue($d->sarana_mobil);
			$activeSheet->getCellByColumnAndRow(53, $baris)->setValue($d->sarana_sepeda_motor);
			$activeSheet->getCellByColumnAndRow(54, $baris)->setValue($d->sarana_sepeda_angin);
			$activeSheet->getCellByColumnAndRow(55, $baris)->setValue($d->sarana_lainnya);
			
			$i = 0;
			$k = 56;
			foreach($sektors as $sektor){
				foreach($arraySarana[$i] as $sarana){
					$jumlah = 0;
					$su = SaranaUsaha::model()->findByAttributes(array("data_id"=>$d->primaryKey, "sarana_id"=>$sarana->sarana_id));
					if($su != NULL){
						$jumlah = $su->jumlah_unit;
					}
					$activeSheet->getCellByColumnAndRow($k, $baris)->setValue($jumlah);
					$k++;
				}
				$i++;
			}
			
			//$activeSheet->insertNewColumnBeforeByIndex($baris);
			$baris++;
		}
		
		$activeSheet->getStyle(
			'A4:' . 
			$activeSheet->getHighestColumn() . 
			$activeSheet->getHighestRow()
		)->applyFromArray($this->getHeaderStyle());
		
		$report = $reportName . date("_Y_m_d_H_i_s");
		//generate the report
		return $this->generateReport($objPHPExcel, $report, "xls", $is_redirect);
	}
	
	public function actionLaporan3($is_redirect = TRUE) {
		$reportName = "laporan_per_kabupaten";
		
		Session::addSession("Generate Report.");
		
		$objPHPExcel = $this->loadPHPExcelLib($reportName);
		$objPHPExcel->setActiveSheetIndex(0);
		$activeSheet = $objPHPExcel->getActiveSheet();
		
		$kabs = Kabupaten::model()->findAll();
		$baris = 4;
		foreach($kabs as $kab){
			$activeSheet->insertNewRowBefore($baris);
			
			$activeSheet->getCellByColumnAndRow(0, $baris)->setValue($baris-3);
			$activeSheet->getCellByColumnAndRow(1, $baris)->setValue($kab->nama);
			
			$jmlKoperasi = 0;
			$jmlAktif = 0;
			$jmlPasif = 0;
			$jmlAnggota = 0;
			$jmlManager = 0;
			$jmlKaryawan = 0;
			$jmlModalSendiri = 0;
			$jmlModalLuar = 0;
			$jmlAsset = 0;
			$jmlVolUsahaDesember = 0;
			$jmlVolUsahaTahunBerjalan = 0;
			$jmlSHUUsahaDesember = 0;
			$jmlSHUUsahaTahunBerjalan = 0;
			$jmlSarana = 0;
			
			$data = DataUtama::model()->findAllByAttributes(array("kabupaten_id"=>$kab->kabupaten_id));
			foreach($data as $d){
				//$d = new DataUtama;
				if($d->kondisi_koperasi == 1){
					$jmlAktif ++;
				}else{
					$jmlPasif ++;
				}
				
				$jmlAnggota += $d->jumlah_anggota;
				$jmlManager += $d->jumlah_manager;
				$jmlKaryawan += $d->jumlah_karyawan;
				
				$modalSendiri = $d->ms_simpanan_pokok + $d->ms_simpanan_wajib
					 + $d->ms_bantuan_hibah + $d->ms_bantuan_sosial
					 + $d->ms_cadangan;
			
				$modalLuar = $d->ml_pinjaman_bank + $d->ml_lemb_non_bank
						 + $d->ml_pinjaman_pihak_iii;

				$modalAsset = $modalSendiri + $modalLuar;
				
				$jmlModalSendiri += $modalSendiri;
				$jmlModalLuar += $modalLuar;
				$jmlAsset += $modalAsset;
				
				$jmlVolUsahaDesember += $d->volume_des_2012;
				$jmlVolUsahaTahunBerjalan += $d->volume_tahun_berjalan;
				$jmlSHUUsahaDesember += $d->shu_des_2012;
				$jmlSHUUsahaTahunBerjalan += $d->shu_tahun_berjalan;
				
				$jmlSarana += $this->getJumlahSarana($d);
			}
			$jmlKoperasi = count($data);
			
			$activeSheet->getCellByColumnAndRow(2, $baris)->setValue($jmlKoperasi);
			$activeSheet->getCellByColumnAndRow(3, $baris)->setValue($jmlAktif);
			$activeSheet->getCellByColumnAndRow(4, $baris)->setValue($jmlPasif);
			$activeSheet->getCellByColumnAndRow(5, $baris)->setValue($jmlAktif + $jmlPasif);
			$activeSheet->getCellByColumnAndRow(6, $baris)->setValue($jmlAnggota);
			$activeSheet->getCellByColumnAndRow(7, $baris)->setValue($jmlManager);
			$activeSheet->getCellByColumnAndRow(8, $baris)->setValue($jmlKaryawan);
			$activeSheet->getCellByColumnAndRow(9, $baris)->setValue($jmlModalSendiri);
			$activeSheet->getCellByColumnAndRow(10, $baris)->setValue($jmlModalLuar);
			$activeSheet->getCellByColumnAndRow(11, $baris)->setValue($jmlAsset);
			$activeSheet->getCellByColumnAndRow(12, $baris)->setValue($jmlVolUsahaDesember);
			$activeSheet->getCellByColumnAndRow(13, $baris)->setValue($jmlVolUsahaTahunBerjalan);
			$activeSheet->getCellByColumnAndRow(14, $baris)->setValue($jmlSHUUsahaDesember);
			$activeSheet->getCellByColumnAndRow(15, $baris)->setValue($jmlSHUUsahaTahunBerjalan);
			$activeSheet->getCellByColumnAndRow(16, $baris)->setValue($jmlSarana);
			
			$baris++;
		}
		
		$activeSheet->getCellByColumnAndRow(2, $baris)->setValue("=SUM(C4:C".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(3, $baris)->setValue("=SUM(D4:D".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(4, $baris)->setValue("=SUM(E4:E".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(5, $baris)->setValue("=SUM(F4:F".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(6, $baris)->setValue("=SUM(G4:G".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(7, $baris)->setValue("=SUM(H4:H".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(8, $baris)->setValue("=SUM(I4:I".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(9, $baris)->setValue("=SUM(J4:J".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(10, $baris)->setValue("=SUM(K4:K".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(11, $baris)->setValue("=SUM(L4:L".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(12, $baris)->setValue("=SUM(M4:M".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(13, $baris)->setValue("=SUM(N4:N".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(14, $baris)->setValue("=SUM(O4:O".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(15, $baris)->setValue("=SUM(P4:P".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(16, $baris)->setValue("=SUM(Q4:Q".($baris-1).")");
		
		//untuk baris terakhir
		
		$jmlKoperasi = 0;
		$jmlAktif = 0;
		$jmlPasif = 0;
		$jmlAnggota = 0;
		$jmlManager = 0;
		$jmlKaryawan = 0;
		$jmlModalSendiri = 0;
		$jmlModalLuar = 0;
		$jmlAsset = 0;
		$jmlVolUsahaDesember = 0;
		$jmlVolUsahaTahunBerjalan = 0;
		$jmlSHUUsahaDesember = 0;
		$jmlSHUUsahaTahunBerjalan = 0;
		$jmlSarana = 0;

		$data = DataUtama::model()->findAll();
		foreach($data as $d){
			//$d = new DataUtama;
			if($d->kondisi_koperasi == 1){
				$jmlAktif ++;
			}else{
				$jmlPasif ++;
			}

			$jmlAnggota += $d->jumlah_anggota;
			$jmlManager += $d->jumlah_manager;
			$jmlKaryawan += $d->jumlah_karyawan;

			$modalSendiri = $d->ms_simpanan_pokok + $d->ms_simpanan_wajib
				 + $d->ms_bantuan_hibah + $d->ms_bantuan_sosial
				 + $d->ms_cadangan;

			$modalLuar = $d->ml_pinjaman_bank + $d->ml_lemb_non_bank
					 + $d->ml_pinjaman_pihak_iii;

			$modalAsset = $modalSendiri + $modalLuar;

			$jmlModalSendiri += $modalSendiri;
			$jmlModalLuar += $modalLuar;
			$jmlAsset += $modalAsset;

			$jmlVolUsahaDesember += $d->volume_des_2012;
			$jmlVolUsahaTahunBerjalan += $d->volume_tahun_berjalan;
			$jmlSHUUsahaDesember += $d->shu_des_2012;
			$jmlSHUUsahaTahunBerjalan += $d->shu_tahun_berjalan;

			$jmlSarana += $this->getJumlahSarana($d);
		}
		$jmlKoperasi = count($data);
		
		$arr = array($jmlKoperasi, $jmlAktif, $jmlPasif, ($jmlAktif+$jmlPasif), $jmlAnggota, $jmlManager, 
			$jmlKaryawan, $jmlModalSendiri, $jmlModalLuar, $jmlAsset, $jmlVolUsahaDesember, 
			$jmlVolUsahaTahunBerjalan, $jmlSHUUsahaDesember, $jmlSHUUsahaTahunBerjalan, $jmlSarana);
		
		$c = 0;
		for($x=2;$x<17;$x++){
			$activeSheet->getCellByColumnAndRow($x, $baris+1)->setValue($arr[$c]);
			$c++;
		}
		
		$activeSheet->getStyle(
			'A4:' . 
			$activeSheet->getHighestColumn() . 
			$activeSheet->getHighestRow()
		)->applyFromArray($this->getHeaderStyle());
		
		$report = $reportName . date("_Y_m_d_H_i_s");
		//generate the report
		return $this->generateReport($objPHPExcel, $report, "xls", $is_redirect);
	}
	
	public function actionLaporan4($is_redirect = TRUE) {
		$reportName = "laporan_umum";
		
		Session::addSession("Generate Report.");
		
		if(!isset($data_id)){
			$data_id = $_GET['data_id'];
		}
		
		if(!isset($data_id)){
			echo "Error !!! : Kode Koperasi tidak ada.";
			return;
		}
		
		$objPHPExcel = $this->loadPHPExcelLib($reportName);
		$objPHPExcel->setActiveSheetIndex(0);
		$activeSheet = $objPHPExcel->getActiveSheet();
		
		//cetak sarana
		$kolom = 56;
		$sektors = Sektor::model()->findAll();
		$arraySarana = array();
		foreach($sektors as $sektor){
			$saranas = Sarana::model()->findAllByAttributes(array("sektor_id"=>$sektor->sektor_id));
			$arraySarana[] = $saranas;
			$activeSheet->getCellByColumnAndRow($kolom, 4)->setValue($sektor->nama);
			$style = $activeSheet->getStyleByColumnAndRow($kolom, 4);
			$style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			//$style->applyFromArray($this->getHeaderStyle());
			$kolomStart = $kolom;
			foreach($saranas as $sarana){
				$activeSheet->getCellByColumnAndRow($kolom, 5)->setValue($sarana->nama);
				$style = $activeSheet->getStyleByColumnAndRow($kolom, 5);
				$style->getAlignment()->setWrapText(true); 
				$style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$style->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$style->applyFromArray($this->getHeaderStyle());
				$kolom++;
			}
			$activeSheet->mergeCellsByColumnAndRow($kolomStart, 4, $kolom-1, 4);
		}
		
		$baris = 6;
		$dataUtamaArray = DataUtama::model()->findAll("data_id = '".$data_id."'");
		foreach($dataUtamaArray as $d){
			//$d = new DataUtama();
			$activeSheet->getCellByColumnAndRow(0, $baris)->setValue($baris-2);
			$activeSheet->getCellByColumnAndRow(1, $baris)->setValue($d->nama_koperasi);
			$activeSheet->getCellByColumnAndRow(2, $baris)->setValue(Sektor::model()->findByPk($d->sektor_id)->nama);
			$activeSheet->getCellByColumnAndRow(3, $baris)->setValue($d->no_badan_hukum);
			$activeSheet->getCellByColumnAndRow(4, $baris)->setValue($d->tanggal_badan_hukum);
			$activeSheet->getCellByColumnAndRow(5, $baris)->setValue($d->no_perubahan_anggaran_dasar);
			$activeSheet->getCellByColumnAndRow(6, $baris)->setValue($d->tanggal_perubahan_anggaran_dasar);
			$activeSheet->getCellByColumnAndRow(7, $baris)->setValue($d->akte_pendirian==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(8, $baris)->setValue($d->anggaran_dasar==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(9, $baris)->setValue($d->izin_usaha_tdp==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(10, $baris)->setValue($d->izin_usaha_siup==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(11, $baris)->setValue($d->izin_usaha_ho==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(12, $baris)->setValue($d->izin_usaha_lainnya==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(13, $baris)->setValue($d->tahun_berdiri);
			$activeSheet->getCellByColumnAndRow(14, $baris)->setValue($d->jalan);
			$activeSheet->getCellByColumnAndRow(15, $baris)->setValue($d->desa);
			$activeSheet->getCellByColumnAndRow(16, $baris)->setValue($d->kecamatan);
			$activeSheet->getCellByColumnAndRow(17, $baris)->setValue(Kabupaten::model()->findByPk($d->kabupaten_id)->nama);
			$activeSheet->getCellByColumnAndRow(18, $baris)->setValue($d->telepon);
			$activeSheet->getCellByColumnAndRow(19, $baris)->setValue($d->fax);
			$activeSheet->getCellByColumnAndRow(20, $baris)->setValue($d->email);
			$activeSheet->getCellByColumnAndRow(21, $baris)->setValue($d->status_kantor_id);
			$activeSheet->getCellByColumnAndRow(22, $baris)->setValue($d->status_kantor_lainnya);
			$activeSheet->getCellByColumnAndRow(23, $baris)->setValue($d->kondisi_koperasi==1?"A":"P");
			$activeSheet->getCellByColumnAndRow(24, $baris)->setValue($d->status_binaan_id);
			$unitUsaha = BidangUsaha::model()->findAll("data_id = '".$d->data_id."'");
			$unitArr = array();
			foreach($unitUsaha as $unit){
				$unitArr[] = $unit->nama;
			}
			$unitUsahaInti = BidangUsahaInti::model()->findAll("data_id = '".$d->data_id."'");
			$unitIntiArr = array();
			foreach($unitUsahaInti as $unit){
				$unitIntiArr[] = $unit->nama;
			}
			$activeSheet->getCellByColumnAndRow(25, $baris)->setValue(implode(";", $unitArr));
			$activeSheet->getCellByColumnAndRow(26, $baris)->setValue(implode(";", $unitIntiArr));
			$activeSheet->getCellByColumnAndRow(27, $baris)->setValue($d->usaha_kemitraan_bumn);
			$activeSheet->getCellByColumnAndRow(28, $baris)->setValue($d->usaha_kemitraan_bumd);
			$activeSheet->getCellByColumnAndRow(29, $baris)->setValue($d->usaha_kemitraan_bums);
			$activeSheet->getCellByColumnAndRow(30, $baris)->setValue($d->usaha_kemitraan_antar_koperasi);
			$activeSheet->getCellByColumnAndRow(31, $baris)->setValue($d->jumlah_anggota);
			$activeSheet->getCellByColumnAndRow(32, $baris)->setValue($d->jumlah_calon_anggota);
			$activeSheet->getCellByColumnAndRow(33, $baris)->setValue($d->jumlah_masyarakat_terlayani);
			$activeSheet->getCellByColumnAndRow(34, $baris)->setValue($d->periode_1."-".$d->periode_2);
			$activeSheet->getCellByColumnAndRow(35, $baris)->setValue($d->jumlah_pengurus);
			$activeSheet->getCellByColumnAndRow(36, $baris)->setValue($d->jumlah_manager);
			$activeSheet->getCellByColumnAndRow(37, $baris)->setValue($d->jumlah_pengawas);
			$activeSheet->getCellByColumnAndRow(38, $baris)->setValue($d->jumlah_karyawan);
			
			$activeSheet->getCellByColumnAndRow(39, $baris)->setValue($d->ms_simpanan_pokok);
			$activeSheet->getCellByColumnAndRow(40, $baris)->setValue($d->ms_simpanan_wajib);
			$activeSheet->getCellByColumnAndRow(41, $baris)->setValue($d->ms_bantuan_hibah);
			$activeSheet->getCellByColumnAndRow(42, $baris)->setValue($d->ms_bantuan_sosial);
			$activeSheet->getCellByColumnAndRow(43, $baris)->setValue($d->ms_cadangan);
			$activeSheet->getCellByColumnAndRow(44, $baris)->setValue($d->ml_pinjaman_bank);
			$activeSheet->getCellByColumnAndRow(45, $baris)->setValue($d->ml_lemb_non_bank);
			$activeSheet->getCellByColumnAndRow(46, $baris)->setValue($d->ml_pinjaman_pihak_iii);
			
			$activeSheet->getCellByColumnAndRow(47, $baris)->setValue($d->volume_des_2012);
			$activeSheet->getCellByColumnAndRow(48, $baris)->setValue($d->volume_tahun_berjalan);
		
			$activeSheet->getCellByColumnAndRow(49, $baris)->setValue($d->shu_des_2012);
			$activeSheet->getCellByColumnAndRow(50, $baris)->setValue($d->shu_tahun_berjalan);
			
			$activeSheet->getCellByColumnAndRow(51, $baris)->setValue($d->sarana_kantor);
			$activeSheet->getCellByColumnAndRow(52, $baris)->setValue($d->sarana_mobil);
			$activeSheet->getCellByColumnAndRow(53, $baris)->setValue($d->sarana_sepeda_motor);
			$activeSheet->getCellByColumnAndRow(54, $baris)->setValue($d->sarana_sepeda_angin);
			$activeSheet->getCellByColumnAndRow(55, $baris)->setValue($d->sarana_lainnya);
			
			$i = 0;
			$k = 56;
			foreach($sektors as $sektor){
				foreach($arraySarana[$i] as $sarana){
					$jumlah = 0;
					$su = SaranaUsaha::model()->findByAttributes(array("data_id"=>$d->primaryKey, "sarana_id"=>$sarana->sarana_id));
					if($su != NULL){
						$jumlah = $su->jumlah_unit;
					}
					$activeSheet->getCellByColumnAndRow($k, $baris)->setValue($jumlah);
					$k++;
				}
				$i++;
			}
			
			//$activeSheet->insertNewColumnBeforeByIndex($baris);
			$baris++;
		}
		
		$activeSheet->getStyle(
			'A4:' . 
			$activeSheet->getHighestColumn() . 
			$activeSheet->getHighestRow()
		)->applyFromArray($this->getHeaderStyle());
		
		$report = $reportName . date("_Y_m_d_H_i_s");
		//generate the report
		return $this->generateReport($objPHPExcel, $report, "xls", $is_redirect);
	}
	
	public function actionLaporan11($is_redirect = TRUE) {
		$reportName = "laporan_umum";
		
		$kabupaten = $_GET['kabupaten'];
		$sektor = $_GET['sektor'];
		$kondisi = $_GET['kondisi'];
		$nama = $_GET['nama'];
		
		Session::addSession("Generate Report.");
		
		$objPHPExcel = $this->loadPHPExcelLib($reportName);
		$objPHPExcel->setActiveSheetIndex(0);
		$activeSheet = $objPHPExcel->getActiveSheet();
		
		//cetak sarana
		$kolom = 56;
		if($sektor != ""){
			$sektors = Sektor::model()->findAll("sektor_id = '".$sektor."'");
		}else{
			$sektors = Sektor::model()->findAll();
		}
		$arraySarana = array();
		foreach($sektors as $_sektor){
			$saranas = Sarana::model()->findAllByAttributes(array("sektor_id"=>$_sektor->sektor_id));
			$arraySarana[] = $saranas;
			$activeSheet->getCellByColumnAndRow($kolom, 4)->setValue($_sektor->nama);
			$style = $activeSheet->getStyleByColumnAndRow($kolom, 4);
			$style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$style->applyFromArray($this->getHeaderStyle());
			$kolomStart = $kolom;
			foreach($saranas as $sarana){
				$activeSheet->getCellByColumnAndRow($kolom, 5)->setValue($sarana->nama);
				$style = $activeSheet->getStyleByColumnAndRow($kolom, 5);
				$style->getAlignment()->setWrapText(true); 
				$style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$style->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$style->applyFromArray($this->getHeaderStyle());
				$kolom++;
			}
			$activeSheet->mergeCellsByColumnAndRow($kolomStart, 4, $kolom-1, 4);
		}
		
		$baris = 6;
		
		
		
		$arr = array();
		
		if($kabupaten != ""){
			$arr["kabupaten_id"] = $kabupaten;
		}
		if($sektor != ""){
			$arr["sektor_id"] = $sektor;
		}
		if($kondisi != ""){
			if($kondisi == "aktif"){
				$kondisi = 1;
			}else{
				$kondisi = 0;
			}
			$arr["kondisi_koperasi"] = $kondisi;
		}
		if($nama != ""){
			$arr["nama_koperasi"] = '%'.$nama.'%';
		}
		
		$temp = array();
		foreach($arr as $key => $val){
			if($key == "nama_koperasi"){
				$temp[] = $key." like '".$val."'";
			}else{
				$temp[] = $key." = '".$val."'";
			}
		}
		
		//echo "Hasil : ".implode(" AND ", $temp);
		
		$dataUtamaArray = DataUtama::model()->findAll(implode(" AND ", $temp));
		foreach($dataUtamaArray as $d){
			//$d = new DataUtama();
			$activeSheet->getCellByColumnAndRow(0, $baris)->setValue($baris-5);
			$activeSheet->getCellByColumnAndRow(1, $baris)->setValue($d->nama_koperasi);
			$activeSheet->getCellByColumnAndRow(2, $baris)->setValue(Sektor::model()->findByPk($d->sektor_id)->nama);
			$activeSheet->getCellByColumnAndRow(3, $baris)->setValue($d->no_badan_hukum);
			$activeSheet->getCellByColumnAndRow(4, $baris)->setValue($d->tanggal_badan_hukum);
			$activeSheet->getCellByColumnAndRow(5, $baris)->setValue($d->no_perubahan_anggaran_dasar);
			$activeSheet->getCellByColumnAndRow(6, $baris)->setValue($d->tanggal_perubahan_anggaran_dasar);
			$activeSheet->getCellByColumnAndRow(7, $baris)->setValue($d->akte_pendirian==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(8, $baris)->setValue($d->anggaran_dasar==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(9, $baris)->setValue($d->izin_usaha_tdp==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(10, $baris)->setValue($d->izin_usaha_siup==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(11, $baris)->setValue($d->izin_usaha_ho==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(12, $baris)->setValue($d->izin_usaha_lainnya==1?"Ada":"Tidak Ada");
			$activeSheet->getCellByColumnAndRow(13, $baris)->setValue($d->tahun_berdiri);
			$activeSheet->getCellByColumnAndRow(14, $baris)->setValue($d->jalan);
			$activeSheet->getCellByColumnAndRow(15, $baris)->setValue($d->desa);
			$activeSheet->getCellByColumnAndRow(16, $baris)->setValue($d->kecamatan);
			$activeSheet->getCellByColumnAndRow(17, $baris)->setValue(Kabupaten::model()->findByPk($d->kabupaten_id)->nama);
			$activeSheet->getCellByColumnAndRow(18, $baris)->setValue($d->telepon);
			$activeSheet->getCellByColumnAndRow(19, $baris)->setValue($d->fax);
			$activeSheet->getCellByColumnAndRow(20, $baris)->setValue($d->email);
			$activeSheet->getCellByColumnAndRow(21, $baris)->setValue($d->status_kantor_id);
			$activeSheet->getCellByColumnAndRow(22, $baris)->setValue($d->status_kantor_lainnya);
			$activeSheet->getCellByColumnAndRow(23, $baris)->setValue($d->kondisi_koperasi==1?"A":"P");
			$activeSheet->getCellByColumnAndRow(24, $baris)->setValue($d->status_binaan_id);
			$unitUsaha = BidangUsaha::model()->findAll("data_id = '".$d->data_id."'");
			$unitArr = array();
			foreach($unitUsaha as $unit){
				$unitArr[] = $unit->nama;
			}
			$unitUsahaInti = BidangUsahaInti::model()->findAll("data_id = '".$d->data_id."'");
			$unitIntiArr = array();
			foreach($unitUsahaInti as $unit){
				$unitIntiArr[] = $unit->nama;
			}
			$activeSheet->getCellByColumnAndRow(25, $baris)->setValue(implode(";", $unitArr));
			$activeSheet->getCellByColumnAndRow(26, $baris)->setValue(implode(";", $unitIntiArr));
			$activeSheet->getCellByColumnAndRow(27, $baris)->setValue($d->usaha_kemitraan_bumn);
			$activeSheet->getCellByColumnAndRow(28, $baris)->setValue($d->usaha_kemitraan_bumd);
			$activeSheet->getCellByColumnAndRow(29, $baris)->setValue($d->usaha_kemitraan_bums);
			$activeSheet->getCellByColumnAndRow(30, $baris)->setValue($d->usaha_kemitraan_antar_koperasi);
			$activeSheet->getCellByColumnAndRow(31, $baris)->setValue($d->jumlah_anggota);
			$activeSheet->getCellByColumnAndRow(32, $baris)->setValue($d->jumlah_calon_anggota);
			$activeSheet->getCellByColumnAndRow(33, $baris)->setValue($d->jumlah_masyarakat_terlayani);
			$activeSheet->getCellByColumnAndRow(34, $baris)->setValue($d->periode_1."-".$d->periode_2);
			$activeSheet->getCellByColumnAndRow(35, $baris)->setValue($d->jumlah_pengurus);
			$activeSheet->getCellByColumnAndRow(36, $baris)->setValue($d->jumlah_manager);
			$activeSheet->getCellByColumnAndRow(37, $baris)->setValue($d->jumlah_pengawas);
			$activeSheet->getCellByColumnAndRow(38, $baris)->setValue($d->jumlah_karyawan);
			
			$activeSheet->getCellByColumnAndRow(39, $baris)->setValue($d->ms_simpanan_pokok);
			$activeSheet->getCellByColumnAndRow(40, $baris)->setValue($d->ms_simpanan_wajib);
			$activeSheet->getCellByColumnAndRow(41, $baris)->setValue($d->ms_bantuan_hibah);
			$activeSheet->getCellByColumnAndRow(42, $baris)->setValue($d->ms_bantuan_sosial);
			$activeSheet->getCellByColumnAndRow(43, $baris)->setValue($d->ms_cadangan);
			$activeSheet->getCellByColumnAndRow(44, $baris)->setValue($d->ml_pinjaman_bank);
			$activeSheet->getCellByColumnAndRow(45, $baris)->setValue($d->ml_lemb_non_bank);
			$activeSheet->getCellByColumnAndRow(46, $baris)->setValue($d->ml_pinjaman_pihak_iii);
			
			$activeSheet->getCellByColumnAndRow(47, $baris)->setValue($d->volume_des_2012);
			$activeSheet->getCellByColumnAndRow(48, $baris)->setValue($d->volume_tahun_berjalan);
		
			$activeSheet->getCellByColumnAndRow(49, $baris)->setValue($d->shu_des_2012);
			$activeSheet->getCellByColumnAndRow(50, $baris)->setValue($d->shu_tahun_berjalan);
			
			$activeSheet->getCellByColumnAndRow(51, $baris)->setValue($d->sarana_kantor);
			$activeSheet->getCellByColumnAndRow(52, $baris)->setValue($d->sarana_mobil);
			$activeSheet->getCellByColumnAndRow(53, $baris)->setValue($d->sarana_sepeda_motor);
			$activeSheet->getCellByColumnAndRow(54, $baris)->setValue($d->sarana_sepeda_angin);
			$activeSheet->getCellByColumnAndRow(55, $baris)->setValue($d->sarana_lainnya);
			
			$i = 0;
			$k = 56;
			foreach($sektors as $sektor){
				foreach($arraySarana[$i] as $sarana){
					$jumlah = 0;
					$su = SaranaUsaha::model()->findByAttributes(array("data_id"=>$d->primaryKey, "sarana_id"=>$sarana->sarana_id));
					if($su != NULL){
						$jumlah = $su->jumlah_unit;
					}
					$activeSheet->getCellByColumnAndRow($k, $baris)->setValue($jumlah);
					$k++;
				}
				$i++;
			}
			
			//$activeSheet->insertNewColumnBeforeByIndex($baris);
			$baris++;
		}
		
		$activeSheet->getStyle(
			'A4:' . 
			$activeSheet->getHighestColumn() . 
			$activeSheet->getHighestRow()
		)->applyFromArray($this->getHeaderStyle());
		
		$report = $reportName . date("_Y_m_d_H_i_s");
		//generate the report
		return $this->generateReport($objPHPExcel, $report, "xls", $is_redirect);
	}
	
	public function actionLaporan20($is_redirect = TRUE) {
		$reportName = "rekap_per_kab";
		
		$sektor = $_GET['sektor'];
		$skt = Sektor::model()->findByPk($sektor);
		$namaSektor = $skt->nama;
		
		Session::addSession("Generate Report.");
		
		$objPHPExcel = $this->loadPHPExcelLib($reportName);
		$objPHPExcel->setActiveSheetIndex(0);
		$activeSheet = $objPHPExcel->getActiveSheet();
		
		$activeSheet->getCell("A3")->setValue("Sektor ".$namaSektor);
		
		//cetak sarana
		$kolom = 22;
		$saranas = Sarana::model()->findAllByAttributes(array("sektor_id"=>$sektor));
		$arraySarana[] = $saranas;
		$activeSheet->getCellByColumnAndRow($kolom, 6)->setValue($namaSektor);
		$style = $activeSheet->getStyleByColumnAndRow($kolom, 6);
		$style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$style->applyFromArray($this->getHeaderStyle());
		$kolomStart = $kolom;
		$saranaIDArray = array();
		foreach($saranas as $sarana){
			$activeSheet->getCellByColumnAndRow($kolom, 7)->setValue($sarana->nama);
			$style = $activeSheet->getStyleByColumnAndRow($kolom, 7);
			$style->getAlignment()->setWrapText(true); 
			$style->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$style->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$style->applyFromArray($this->getHeaderStyle());
			$saranaIDArray[] = $sarana->sarana_id;
			$kolom++;
		}
		$activeSheet->mergeCellsByColumnAndRow($kolomStart, 6, $kolom-1, 6);
		
		
		
		$kabs = Kabupaten::model()->findAll();
		$baris = 8;
		foreach($kabs as $kab){
			if($kab->kabupaten_id == 39){
				continue;
			}
			$activeSheet->insertNewRowBefore($baris);
			
			$activeSheet->getCellByColumnAndRow(0, $baris)->setValue($baris-7);
			$activeSheet->getCellByColumnAndRow(1, $baris)->setValue($kab->nama);
			
			$jmlKoperasi = 0;
			$jmlAktif = 0;
			$jmlPasif = 0;
			$jmlAnggota = 0;
			$jmlManager = 0;
			$jmlKaryawan = 0;
			$jmlModalSendiri = 0;
			$jmlModalLuar = 0;
			$jmlAsset = 0;
			$jmlVolUsahaDesember = 0;
			$jmlVolUsahaTahunBerjalan = 0;
			$jmlSHUUsahaDesember = 0;
			$jmlSHUUsahaTahunBerjalan = 0;
			$jmlSarana = 0;
			
			$nilai = array(0,0,0,0,0);
			$saranaNilai = array();
			foreach($saranaIDArray as $sia){
				$saranaNilai[] = 0;
			}
			
			$data = DataUtama::model()->findAllByAttributes(array("kabupaten_id"=>$kab->kabupaten_id, "sektor_id"=>$sektor));
			foreach($data as $d){
				//$d = new DataUtama;
				if($d->kondisi_koperasi == 1){
					$jmlAktif ++;
				}else{
					$jmlPasif ++;
				}
				
				$jmlAnggota += $d->jumlah_anggota;
				$jmlManager += $d->jumlah_manager;
				$jmlKaryawan += $d->jumlah_karyawan;
				
				$modalSendiri = $d->ms_simpanan_pokok + $d->ms_simpanan_wajib
					 + $d->ms_bantuan_hibah + $d->ms_bantuan_sosial
					 + $d->ms_cadangan;
			
				$modalLuar = $d->ml_pinjaman_bank + $d->ml_lemb_non_bank
						 + $d->ml_pinjaman_pihak_iii;

				$modalAsset = 0;
				
				$jmlModalSendiri += $modalSendiri;
				$jmlModalLuar += $modalLuar;
				$jmlAsset += $modalAsset;
				
				$jmlVolUsahaDesember += $d->volume_des_2012;
				$jmlVolUsahaTahunBerjalan += $d->volume_tahun_berjalan;
				$jmlSHUUsahaDesember += $d->shu_des_2012;
				$jmlSHUUsahaTahunBerjalan += $d->shu_tahun_berjalan;
				
				$nilai[0] += $d->sarana_kantor;
				$nilai[1] += $d->sarana_mobil;
				$nilai[2] += $d->sarana_sepeda_motor;
				$nilai[3] += $d->sarana_sepeda_angin;
				$nilai[4] += $d->sarana_lainnya;
				
				$b = 0;
				foreach($saranaIDArray as $sia){
					$sarana = SaranaUsaha::model()->findByAttributes(array("data_id"=>$d->data_id, "sarana_id"=>$sia));
					$saranaNilai[$b] = $sarana->jumlah_unit;
					$b++;
				}
				
				$jmlSarana += $this->getJumlahSarana($d);
			}
			$jmlKoperasi = count($data);
			
			$activeSheet->getCellByColumnAndRow(2, $baris)->setValue($jmlKoperasi);
			$activeSheet->getCellByColumnAndRow(3, $baris)->setValue($jmlAktif);
			$activeSheet->getCellByColumnAndRow(4, $baris)->setValue($jmlPasif);
			$activeSheet->getCellByColumnAndRow(5, $baris)->setValue($jmlAktif + $jmlPasif);
			$activeSheet->getCellByColumnAndRow(6, $baris)->setValue($jmlAnggota);
			$activeSheet->getCellByColumnAndRow(7, $baris)->setValue($jmlManager);
			$activeSheet->getCellByColumnAndRow(8, $baris)->setValue($jmlKaryawan);
			$activeSheet->getCellByColumnAndRow(9, $baris)->setValue($jmlModalSendiri);
			$activeSheet->getCellByColumnAndRow(10, $baris)->setValue($jmlModalLuar);
			$activeSheet->getCellByColumnAndRow(11, $baris)->setValue($jmlAsset);
			$activeSheet->getCellByColumnAndRow(12, $baris)->setValue($jmlVolUsahaDesember);
			$activeSheet->getCellByColumnAndRow(13, $baris)->setValue($jmlVolUsahaTahunBerjalan);
			$activeSheet->getCellByColumnAndRow(14, $baris)->setValue($jmlSHUUsahaDesember);
			$activeSheet->getCellByColumnAndRow(15, $baris)->setValue($jmlSHUUsahaTahunBerjalan);
			$activeSheet->getCellByColumnAndRow(16, $baris)->setValue($jmlSarana);
			$activeSheet->getCellByColumnAndRow(17, $baris)->setValue($nilai[0]);
			$activeSheet->getCellByColumnAndRow(18, $baris)->setValue($nilai[1]);
			$activeSheet->getCellByColumnAndRow(19, $baris)->setValue($nilai[2]);
			$activeSheet->getCellByColumnAndRow(20, $baris)->setValue($nilai[3]);
			$activeSheet->getCellByColumnAndRow(21, $baris)->setValue($nilai[4]);
			
			$b = 22;
			foreach($saranaNilai as $sia){
				$activeSheet->getCellByColumnAndRow($b, $baris)->setValue($sia);
				$b++;
			}
			
			$baris++;
		}
		
		$activeSheet->getCellByColumnAndRow(2, $baris)->setValue("=SUM(C4:C".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(3, $baris)->setValue("=SUM(D4:D".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(4, $baris)->setValue("=SUM(E4:E".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(5, $baris)->setValue("=SUM(F4:F".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(6, $baris)->setValue("=SUM(G4:G".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(7, $baris)->setValue("=SUM(H4:H".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(8, $baris)->setValue("=SUM(I4:I".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(9, $baris)->setValue("=SUM(J4:J".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(10, $baris)->setValue("=SUM(K4:K".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(11, $baris)->setValue("=SUM(L4:L".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(12, $baris)->setValue("=SUM(M4:M".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(13, $baris)->setValue("=SUM(N4:N".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(14, $baris)->setValue("=SUM(O4:O".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(15, $baris)->setValue("=SUM(P4:P".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(16, $baris)->setValue("=SUM(Q4:Q".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(17, $baris)->setValue("=SUM(R4:R".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(18, $baris)->setValue("=SUM(S4:S".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(19, $baris)->setValue("=SUM(T4:T".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(20, $baris)->setValue("=SUM(U4:U".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(21, $baris)->setValue("=SUM(V4:V".($baris-1).")");
		
		$h = array("W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN");
		$b = 22;
		$n = 0;
		foreach($saranaNilai as $sia){
			$activeSheet->getCellByColumnAndRow($b, $baris)->setValue("=SUM(".$h[$n]."4:".$h[$n]."".($baris-1).")");
			$b++;
			$n++;
		}
		
		//untuk baris terakhir
		
		$jmlKoperasi = 0;
		$jmlAktif = 0;
		$jmlPasif = 0;
		$jmlAnggota = 0;
		$jmlManager = 0;
		$jmlKaryawan = 0;
		$jmlModalSendiri = 0;
		$jmlModalLuar = 0;
		$jmlAsset = 0;
		$jmlVolUsahaDesember = 0;
		$jmlVolUsahaTahunBerjalan = 0;
		$jmlSHUUsahaDesember = 0;
		$jmlSHUUsahaTahunBerjalan = 0;
		$jmlSarana = 0;
		
		$nilai = array(0,0,0,0,0);
		$saranaNilai = array();
		foreach($saranaIDArray as $sia){
			$saranaNilai[] = 0;
		}

		$data = DataUtama::model()->findAll("kabupaten_id = '39' AND sektor_id = '".$sektor."'");
		foreach($data as $d){
			//$d = new DataUtama;
			if($d->kondisi_koperasi == 1){
				$jmlAktif ++;
			}else{
				$jmlPasif ++;
			}

			$jmlAnggota += $d->jumlah_anggota;
			$jmlManager += $d->jumlah_manager;
			$jmlKaryawan += $d->jumlah_karyawan;

			$modalSendiri = $d->ms_simpanan_pokok + $d->ms_simpanan_wajib
				 + $d->ms_bantuan_hibah + $d->ms_bantuan_sosial
				 + $d->ms_cadangan;

			$modalLuar = $d->ml_pinjaman_bank + $d->ml_lemb_non_bank
					 + $d->ml_pinjaman_pihak_iii;

			$modalAsset = 0;

			$jmlModalSendiri += $modalSendiri;
			$jmlModalLuar += $modalLuar;
			$jmlAsset += $modalAsset;

			$jmlVolUsahaDesember += $d->volume_des_2012;
			$jmlVolUsahaTahunBerjalan += $d->volume_tahun_berjalan;
			$jmlSHUUsahaDesember += $d->shu_des_2012;
			$jmlSHUUsahaTahunBerjalan += $d->shu_tahun_berjalan;
			
			$b = 0;
			foreach($saranaIDArray as $sia){
				$sarana = SaranaUsaha::model()->findByAttributes(array("data_id"=>$d->data_id, "sarana_id"=>$sia));
				$saranaNilai[$b] = $sarana->jumlah_unit;
				$b++;
			}

			$jmlSarana += $this->getJumlahSarana($d);
		}
		$jmlKoperasi = count($data);
		
		$arr = array($jmlKoperasi, $jmlAktif, $jmlPasif, ($jmlAktif+$jmlPasif), $jmlAnggota, $jmlManager, 
			$jmlKaryawan, $jmlModalSendiri, $jmlModalLuar, $jmlAsset, $jmlVolUsahaDesember, 
			$jmlVolUsahaTahunBerjalan, $jmlSHUUsahaDesember, $jmlSHUUsahaTahunBerjalan, $jmlSarana);
		
		$c = 0;
		for($x=2;$x<17;$x++){
			$activeSheet->getCellByColumnAndRow($x, $baris+1)->setValue($arr[$c]);
			$c++;
		}
		
		$b = 22;
		foreach($saranaNilai as $sia){
			$activeSheet->getCellByColumnAndRow($b, $baris+1)->setValue($sia);
			$b++;
		}
		
		$activeSheet->getStyle(
			'A6:' . 
			$activeSheet->getHighestColumn() . 
			$activeSheet->getHighestRow()
		)->applyFromArray($this->getHeaderStyle());
		
		$report = $reportName . date("_Y_m_d_H_i_s");
		//generate the report
		return $this->generateReport($objPHPExcel, $report, "xls", $is_redirect);
	}
	
	public function actionLaporan21($is_redirect = TRUE) {
		$reportName = "rekap_per_sektor";
		
		$kabupaten = $_GET['kabupaten'];
		$kab = Kabupaten::model()->findByPk($kabupaten);
		$namaKabupaten = $kab->nama;
		
		Session::addSession("Generate Report.");
		
		$objPHPExcel = $this->loadPHPExcelLib($reportName);
		$objPHPExcel->setActiveSheetIndex(0);
		$activeSheet = $objPHPExcel->getActiveSheet();
		
		$activeSheet->getCell("A3")->setValue("Kabupaten ".$namaKabupaten);
		
		$sektors = Sektor::model()->findAll();
		$baris = 8;
		foreach($sektors as $sektor){
			$activeSheet->insertNewRowBefore($baris);
			
			$activeSheet->getCellByColumnAndRow(0, $baris)->setValue($baris-7);
			$activeSheet->getCellByColumnAndRow(1, $baris)->setValue($sektor->nama);
			
			$jmlKoperasi = 0;
			$jmlAktif = 0;
			$jmlPasif = 0;
			$jmlAnggota = 0;
			$jmlManager = 0;
			$jmlKaryawan = 0;
			$jmlModalSendiri = 0;
			$jmlModalLuar = 0;
			$jmlAsset = 0;
			$jmlVolUsahaDesember = 0;
			$jmlVolUsahaTahunBerjalan = 0;
			$jmlSHUUsahaDesember = 0;
			$jmlSHUUsahaTahunBerjalan = 0;
			$jmlSarana = 0;
			
			$nilai = array(0,0,0,0,0);
			
			$data = DataUtama::model()->findAllByAttributes(
					array("sektor_id"=>$sektor->sektor_id));
			foreach($data as $d){
				//$d = new DataUtama;
				if($d->kondisi_koperasi == 1){
					$jmlAktif ++;
				}else{
					$jmlPasif ++;
				}
				
				$jmlAnggota += $d->jumlah_anggota;
				$jmlManager += $d->jumlah_manager;
				$jmlKaryawan += $d->jumlah_karyawan;
				
				$modalSendiri = $d->ms_simpanan_pokok + $d->ms_simpanan_wajib
					 + $d->ms_bantuan_hibah + $d->ms_bantuan_sosial
					 + $d->ms_cadangan;
			
				$modalLuar = $d->ml_pinjaman_bank + $d->ml_lemb_non_bank
						 + $d->ml_pinjaman_pihak_iii;

				$modalAsset = 0;
				
				$jmlModalSendiri += $modalSendiri;
				$jmlModalLuar += $modalLuar;
				$jmlAsset += $modalAsset;
				
				$jmlVolUsahaDesember += $d->volume_des_2012;
				$jmlVolUsahaTahunBerjalan += $d->volume_tahun_berjalan;
				$jmlSHUUsahaDesember += $d->shu_des_2012;
				$jmlSHUUsahaTahunBerjalan += $d->shu_tahun_berjalan;
				
				$nilai[0] += $d->sarana_kantor;
				$nilai[1] += $d->sarana_mobil;
				$nilai[2] += $d->sarana_sepeda_motor;
				$nilai[3] += $d->sarana_sepeda_angin;
				$nilai[4] += $d->sarana_lainnya;
				
				$jmlSarana += $this->getJumlahSarana($d);
			}
			$jmlKoperasi = count($data);
			
			$activeSheet->getCellByColumnAndRow(2, $baris)->setValue($jmlKoperasi);
			$activeSheet->getCellByColumnAndRow(3, $baris)->setValue($jmlAktif);
			$activeSheet->getCellByColumnAndRow(4, $baris)->setValue($jmlPasif);
			$activeSheet->getCellByColumnAndRow(5, $baris)->setValue($jmlAktif + $jmlPasif);
			$activeSheet->getCellByColumnAndRow(6, $baris)->setValue($jmlAnggota);
			$activeSheet->getCellByColumnAndRow(7, $baris)->setValue($jmlManager);
			$activeSheet->getCellByColumnAndRow(8, $baris)->setValue($jmlKaryawan);
			$activeSheet->getCellByColumnAndRow(9, $baris)->setValue($jmlModalSendiri);
			$activeSheet->getCellByColumnAndRow(10, $baris)->setValue($jmlModalLuar);
			$activeSheet->getCellByColumnAndRow(11, $baris)->setValue($jmlAsset);
			$activeSheet->getCellByColumnAndRow(12, $baris)->setValue($jmlVolUsahaDesember);
			$activeSheet->getCellByColumnAndRow(13, $baris)->setValue($jmlVolUsahaTahunBerjalan);
			$activeSheet->getCellByColumnAndRow(14, $baris)->setValue($jmlSHUUsahaDesember);
			$activeSheet->getCellByColumnAndRow(15, $baris)->setValue($jmlSHUUsahaTahunBerjalan);
			$activeSheet->getCellByColumnAndRow(16, $baris)->setValue($jmlSarana);
			$activeSheet->getCellByColumnAndRow(17, $baris)->setValue($nilai[0]);
			$activeSheet->getCellByColumnAndRow(18, $baris)->setValue($nilai[1]);
			$activeSheet->getCellByColumnAndRow(19, $baris)->setValue($nilai[2]);
			$activeSheet->getCellByColumnAndRow(20, $baris)->setValue($nilai[3]);
			$activeSheet->getCellByColumnAndRow(21, $baris)->setValue($nilai[4]);
			
			$baris++;
		}
		
		$activeSheet->getCellByColumnAndRow(2, $baris)->setValue("=SUM(C4:C".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(3, $baris)->setValue("=SUM(D4:D".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(4, $baris)->setValue("=SUM(E4:E".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(5, $baris)->setValue("=SUM(F4:F".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(6, $baris)->setValue("=SUM(G4:G".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(7, $baris)->setValue("=SUM(H4:H".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(8, $baris)->setValue("=SUM(I4:I".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(9, $baris)->setValue("=SUM(J4:J".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(10, $baris)->setValue("=SUM(K4:K".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(11, $baris)->setValue("=SUM(L4:L".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(12, $baris)->setValue("=SUM(M4:M".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(13, $baris)->setValue("=SUM(N4:N".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(14, $baris)->setValue("=SUM(O4:O".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(15, $baris)->setValue("=SUM(P4:P".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(16, $baris)->setValue("=SUM(Q4:Q".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(17, $baris)->setValue("=SUM(R4:R".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(18, $baris)->setValue("=SUM(S4:S".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(19, $baris)->setValue("=SUM(T4:T".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(20, $baris)->setValue("=SUM(U4:U".($baris-1).")");
		$activeSheet->getCellByColumnAndRow(21, $baris)->setValue("=SUM(V4:V".($baris-1).")");
		
		$activeSheet->getStyle(
			'A6:' . 
			$activeSheet->getHighestColumn() . 
			$activeSheet->getHighestRow()
		)->applyFromArray($this->getHeaderStyle());
		
		$report = $reportName . date("_Y_m_d_H_i_s");
		//generate the report
		return $this->generateReport($objPHPExcel, $report, "xls", $is_redirect);
	}
	
	public function getJumlahPengurus($data){
		$jml = 0;
		
		if($data->ketua != ""){
			$jml ++;
		}
		
		if($data->bendahara != ""){
			$jml ++;
		}
		
		if($data->sekretaris != ""){
			$jml ++;
		}
		
		if($data->pembantu_umum != ""){
			$jml ++;
		}
		
		return $jml;
	}
	
	public function getJumlahManager($data){
		$jml = 0;
		
		$jml += count(Pengelola::model()->findAll("data_id = '{$data->data_id}' AND jenis_pengelola_id <> 3"));
		
		return $jml;
	}
	
	public function getJumlahKaryawan($data){
		$jml = 0;
		
		$jml += count(Pengelola::model()->findAll("data_id = '{$data->data_id}' AND jenis_pengelola_id = 3"));
		
		return $jml;
	}
	
	public function getJumlahSarana($data){
		$jml = 0;
		$shs = SaranaUsaha::model()->findAll("data_id = '".$data->data_id."'");
		foreach ($shs as $value) {
			$jml += $value->jumlah_unit;
		}
		
		return $jml;
	}
}