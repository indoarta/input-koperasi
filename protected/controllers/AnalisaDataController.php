<?php

class AnalisaDataController extends GxController
{
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function showTable($data, $satuan, $judul){
		if(count($data)!=0){
		?>
		<table class="mws-table table-striped table-bordered">
			<thead>
				<tr>
					<th><?php echo $judul; ?></th>
					<th>Nilai</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($data as $k => $v) {
					if($satuan == "Rupiah"){
						$v = "Rp ".number_format($v, 0, ",", ".").",-";
					}
					echo "
						<tr>
							<td>".$k."</td>
							<td>".$v."</td>
						</tr>";
				}
				?>
			</tbody>
		</table>
		<?php
		}
	}
	
	public function actionJenisPertama()
	{
		$tipe = $_GET['tipe'];
		$pie = "piechart";
		$bar = "barchart";
		if(isset($_GET['download'])){
			$pie = "piechart_download";
			$bar = "barchart_download";
		}
		if($tipe == 1){
			$data = array();
			$dataUtama = DataUtama::model()->findAll();
			foreach($dataUtama as $du){
				if($du->kondisi_koperasi == 1){
					$data["Aktif"] += 1;
				}else{
					$data["Pasif"] += 1;
				}
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Koperasi", "title"=>"Kondisi Keaktifan Koperasi Global Propinsi"));
		}else if($tipe == 2){
			$data = array();
			$dataUtama = DataUtama::model()->findAll();
			foreach($dataUtama as $du){
				$data["Jumlah Pengurus"] += $du->jumlah_pengurus;
				$data["Jumlah Pengawas"] += $du->jumlah_pengawas;
				$data["Jumlah Manager"] += $du->jumlah_manager;
				$data["Jumlah Karyawan"] += $du->jumlah_karyawan;
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Orang", "title"=>"Kondisi Kelembagaan Koperasi Global Propinsi"));
		}else if($tipe == 3){
			$data = array();
			$dataUtama = DataUtama::model()->findAll();
			foreach($dataUtama as $du){
				$data["Jumlah Anggota"] += $du->jumlah_anggota;
				$data["Jumlah Calon Anggota"] += $du->jumlah_calon_anggota;
				$data["Jumlah Masyarakat Terlayani"] += $du->jumlah_masyarakat_terlayani;
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Orang", "title"=>"Kondisi Keanggotaan Koperasi Global Propinsi"));
		}else if($tipe == 4){
			$data = array();
			$data2 = array();
			$data3 = array();
			$dataUtama = DataUtama::model()->findAll();
			foreach($dataUtama as $du){
				$data["Modal Sendiri"] += $du->ms_simpanan_pokok + $du->ms_simpanan_wajib +
						$du->ms_bantuan_hibah + $du->ms_bantuan_sosial + $du->ms_cadangan;
				$data["Modal Luar"] += $du->ml_pinjaman_bank + $du->ml_lemb_non_bank + $du->ml_pinjaman_pihak_iii;
				
				$data2["Simpanan Pokok"] += $du->ms_simpanan_pokok;
				$data2["Simpanan Wajib"] += $du->ms_simpanan_wajib;
				$data2["Bantuan Hibah"] += $du->ms_bantuan_hibah;
				$data2["Bantuan Sosial"] += $du->ms_bantuan_sosial;
				$data2["Cadangan"] += $du->ms_cadangan;
				
				$data3["Pjmn Bank"] += $du->ml_pinjaman_bank;
				$data3["Lbg Non Bank"] += $du->ml_lemb_non_bank;
				$data3["Pjmn Phk 3"] += $du->ml_pinjaman_pihak_iii;
			}
			$this->renderPartial($pie, array("data"=>$data, "judul"=>"Struktur Modal Global Propinsi", "judul2"=>"Modal Sendiri Global Propinsi", "judul3"=>"Modal Luar Global Propinsi", "data2"=>$data2, "data3"=>$data3, "satuan"=> "Rupiah", "title"=>"Struktur Modal Global Propinsi"));
		}else if($tipe == 5){
			$dataSektor = array();
			$data = array();
			$sektor = Sektor::model()->findAll();
			foreach ($sektor as $s){
				$dataSektor[$s->sektor_id] = $s->nama;
				$data[$s->nama] = 0;
			}
			$dataUtama = DataUtama::model()->findAll();
			foreach($dataUtama as $du){
				$data[$dataSektor[$du->sektor_id]] += $du->volume_des_2012;
			}
			$this->renderPartial($bar, array("data"=>$data, "satuan"=> "Rupiah", "title"=>"Volume Usaha Global Propinsi"));
		}else if($tipe == 6){
			$dataSektor = array();
			$data = array();
			$sektor = Sektor::model()->findAll();
			foreach ($sektor as $s){
				$dataSektor[$s->sektor_id] = $s->nama;
				$data[$s->nama] = 0;
			}
			$dataUtama = DataUtama::model()->findAll();
			foreach($dataUtama as $du){
				$data[$dataSektor[$du->sektor_id]] += $du->shu_des_2012;
			}
			$this->renderPartial($bar, array("data"=>$data, "satuan"=> "Rupiah", "title"=>"SHU Global Propinsi"));
		}else if($tipe == 7){
			$data = array();
			$dataUtama = DataUtama::model()->findAll();
			foreach($dataUtama as $du){
				$data["Kantor Koperasi"] += $du->sarana_kantor;
				$data["Sarana Mobil"] += $du->sarana_mobil;
				$data["Sarana Sepeda Motor"] += $du->sarana_sepeda_motor;
				$data["Sarana Sepeda Angin"] += $du->sarana_sepeda_angin;
				$data["Sarana Lainnya"] += $du->sarana_lainnya;
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Unit", "title"=>"Sarana Kantor Global Propinsi"));
		}
	}
	
	public function actionJenisKedua()
	{
		$pie = "piechart";
		$bar = "barchart";
		if(isset($_GET['download'])){
			$pie = "piechart_download";
			$bar = "barchart_download";
		}
		$sektor_id = $_GET['sektor_id'];
		$sektor = Sektor::model()->findByPk($sektor_id);
		$tipe = $_GET['tipe'];
		if($tipe == 1){
			//kabupaten
			$kabArray = array();
			$kabTempArr = Kabupaten::model()->findAll();
			$data = array();
			foreach($kabTempArr as $kab){
				$kabArray[$kab->kabupaten_id] = $kab->nama;
				$data[$kab->nama] = 0;
			}
			
			$dataUtama = DataUtama::model()->findAllByAttributes(array("sektor_id"=>$sektor_id));
			foreach($dataUtama as $du){
				$data[$kabArray[$du->kabupaten_id]] += 1;
			}
			$this->renderPartial($bar, array("data"=>$data, "rotate"=>TRUE, "satuan"=>"Unit", "title"=>"Potensi pada Sektor ".$sektor->nama));
		}else if($tipe == 2){
			$data = array();
			$dataUtama = DataUtama::model()->findAllByAttributes(array("sektor_id"=>$sektor_id));
			foreach($dataUtama as $du){
				if($du->kondisi_koperasi == 1){
					$data["Aktif"] += 1;
				}else{
					$data["Pasif"] += 1;
				}
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Unit", "title"=>"Kondisi Keaktifan Koperasi di Sektor ".$sektor->nama));
		}else if($tipe == 3){
			$data = array();
			$dataUtama = DataUtama::model()->findAllByAttributes(array("sektor_id"=>$sektor_id));
			foreach($dataUtama as $du){
				$data["Jumlah Pengurus"] += $du->jumlah_pengurus;
				$data["Jumlah Pengawas"] += $du->jumlah_pengawas;
				$data["Jumlah Manager"] += $du->jumlah_manager;
				$data["Jumlah Karyawan"] += $du->jumlah_karyawan;
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Orang", "title"=>"Kondisi Kelembagaan Koperasi pada Sektor ".$sektor->nama));
		}else if($tipe == 4){
			$data = array();
			$dataUtama = DataUtama::model()->findAllByAttributes(array("sektor_id"=>$sektor_id));
			foreach($dataUtama as $du){
				$data["Jumlah Anggota"] += $du->jumlah_anggota;
				$data["Jumlah Calon Anggota"] += $du->jumlah_calon_anggota;
				$data["Jumlah Masyarakat Terlayani"] += $du->jumlah_masyarakat_terlayani;
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Orang", "title"=>"Kondisi Keanggotaan Koperasi pada Sektor ".$sektor->nama));
		}else if($tipe == 5){
			$data = array();
			$data2 = array();
			$data3 = array();
			$dataUtama = DataUtama::model()->findAllByAttributes(array("sektor_id"=>$sektor_id));
			foreach($dataUtama as $du){
				$data["Modal Sendiri"] += $du->ms_simpanan_pokok + $du->ms_simpanan_wajib +
						$du->ms_bantuan_hibah + $du->ms_bantuan_sosial + $du->ms_cadangan;
				$data["Modal Luar"] += $du->ml_pinjaman_bank + $du->ml_lemb_non_bank + $du->ml_pinjaman_pihak_iii;
				
				$data2["Simpanan Pokok"] += $du->ms_simpanan_pokok;
				$data2["Simpanan Wajib"] += $du->ms_simpanan_wajib;
				$data2["Bantuan Hibah"] += $du->ms_bantuan_hibah;
				$data2["Bantuan Sosial"] += $du->ms_bantuan_sosial;
				$data2["Cadangan"] += $du->ms_cadangan;
				
				$data3["Pinjaman Bank"] += $du->ml_pinjaman_bank;
				$data3["Lembaga Non Bank"] += $du->ml_lemb_non_bank;
				$data3["Pinjaman Pihak III"] += $du->ml_pinjaman_pihak_iii;
			}
			$this->renderPartial($pie, array("data"=>$data, "data2"=>$data2, "data3"=>$data3, "judul"=>"Struktur Modal", "judul2"=>"Modal Sendiri", "judul3"=>"Modal Luar", "satuan"=> "Rupiah", "title"=>"Struktur Modal pada Sektor ".$sektor->nama));
		}else if($tipe == 6){
			$kabArray = array();
			$kabTempArr = Kabupaten::model()->findAll();
			$data = array();
			foreach($kabTempArr as $kab){
				$kabArray[$kab->kabupaten_id] = $kab->nama;
				$data[$kab->nama] = 0;
			}
			
			$dataUtama = DataUtama::model()->findAllByAttributes(array("sektor_id"=>$sektor_id));
			foreach($dataUtama as $du){
				$data[$kabArray[$du->kabupaten_id]] += $du->volume_des_2012;
			}
			
			$this->renderPartial($bar, array("data"=>$data, "rotate"=>TRUE, "satuan"=>"Rupiah", "title"=>"Volume Usaha pada Sektor ".$sektor->nama));
		}else if($tipe == 7){
			$kabArray = array();
			$kabTempArr = Kabupaten::model()->findAll();
			$data = array();
			foreach($kabTempArr as $kab){
				$kabArray[$kab->kabupaten_id] = $kab->nama;
				$data[$kab->nama] = 0;
			}
			
			$dataUtama = DataUtama::model()->findAllByAttributes(array("sektor_id"=>$sektor_id));
			foreach($dataUtama as $du){
				$data[$kabArray[$du->kabupaten_id]] += $du->shu_des_2012;
			}
			
			$this->renderPartial($bar, array("data"=>$data, "rotate"=>TRUE, "satuan"=>"Rupiah", "title"=>"SHU pada Sektor ".$sektor->nama));
		}else if($tipe == 8){
			$data = array();
			$dataUtama = DataUtama::model()->findAllByAttributes(array("sektor_id"=>$sektor_id));
			foreach($dataUtama as $du){
				$data["Kantor Koperasi"] += $du->sarana_kantor;
				$data["Sarana Mobil"] += $du->sarana_mobil;
				$data["Sarana Sepeda Motor"] += $du->sarana_sepeda_motor;
				$data["Sarana Sepeda Angin"] += $du->sarana_sepeda_angin;
				$data["Sarana Lainnya"] += $du->sarana_lainnya;
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Unit", "title"=>"Sarana Kantor pada Sektor ".$sektor->nama));
		}else if($tipe == 9){
			$saranaIDArray = array();
			$saranaArray = array();
			$data = array();
			$sarana = Sarana::model()->findAllByAttributes(array("sektor_id"=>$sektor_id));
			foreach($sarana as $s){
				$saranaIDArray[] = $s->sarana_id;
				$saranaArray[$s->sarana_id] = $s->nama;
				//$data[$s->nama] = 0;
			}
			
			
			$dataUtama = DataUtama::model()->findAllByAttributes(array("sektor_id"=>$sektor_id));
			foreach($dataUtama as $du){
				$saranaUsaha = SaranaUsaha::model()->findAllByAttributes(array("data_id"=>$du->data_id));
				foreach($saranaUsaha as $su){
					if(in_array($su->sarana_id, $saranaIDArray)){
						$data[$saranaArray[$s->sarana_id]] += $su->jumlah_unit;
					}
				}
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Unit", "title"=>"Sarana Sektor pada Sektor ".$sektor->nama));
		}
	}
	
	public function actionJenisKetiga()
	{
		$pie = "piechart";
		$bar = "barchart";
		if(isset($_GET['download'])){
			$pie = "piechart_download";
			$bar = "barchart_download";
		}
		$kabupaten_id = $_GET['kabupaten_id'];
		$kabupaten = Kabupaten::model()->findByPk($kabupaten_id);
		$tipe = $_GET['tipe'];
		if($tipe == 1){
			//kabupaten
			$sektorArray = array();
			$sektorTempArr = Sektor::model()->findAll("jenis = 'sektor'");
			$data = array();
			foreach($sektorTempArr as $sek){
				$sektorArray[$sek->sektor_id] = $sek->nama;
				$data[$sek->nama] = 0;
			}
			
			$dataUtama = DataUtama::model()->findAllByAttributes(array("kabupaten_id"=>$kabupaten_id));
			foreach($dataUtama as $du){
				$data[$sektorArray[$du->sektor_id]] += 1;
			}
			$this->renderPartial($bar, array("data"=>$data, "satuan"=>"Unit", "title"=>"Potensi pada Kabupaten ".$kabupaten->nama));
		}else if($tipe == 2){
			$data = array();
			$dataUtama = DataUtama::model()->findAllByAttributes(array("kabupaten_id"=>$kabupaten_id));
			foreach($dataUtama as $du){
				if($du->kondisi_koperasi == 1){
					$data["Aktif"] += 1;
				}else{
					$data["Pasif"] += 1;
				}
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Unit", "title"=>"Kondisi Keaktifan Koperasi pada Kabupaten ".$kabupaten->nama));
		}else if($tipe == 3){
			$data = array();
			$dataUtama = DataUtama::model()->findAllByAttributes(array("kabupaten_id"=>$kabupaten_id));
			foreach($dataUtama as $du){
				$data["Jumlah Pengurus"] += $du->jumlah_pengurus;
				$data["Jumlah Pengawas"] += $du->jumlah_pengawas;
				$data["Jumlah Manager"] += $du->jumlah_manager;
				$data["Jumlah Karyawan"] += $du->jumlah_karyawan;
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Orang", "title"=>"Kondisi Kelembagaan Koperasi pada Kabupaten ".$kabupaten->nama));
		}else if($tipe == 4){
			$data = array();
			$dataUtama = DataUtama::model()->findAllByAttributes(array("kabupaten_id"=>$kabupaten_id));
			foreach($dataUtama as $du){
				$data["Jumlah Anggota"] += $du->jumlah_anggota;
				$data["Jumlah Calon Anggota"] += $du->jumlah_calon_anggota;
				$data["Jumlah Masyarakat Terlayani"] += $du->jumlah_masyarakat_terlayani;
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Orang", "title"=>"Kondisi Keanggotaan Koperasi pada Kabupaten ".$kabupaten->nama));
		}else if($tipe == 5){
			$data = array();
			$data2 = array();
			$data3 = array();
			$dataUtama = DataUtama::model()->findAllByAttributes(array("kabupaten_id"=>$kabupaten_id));
			foreach($dataUtama as $du){
				$data["Modal Sendiri"] += $du->ms_simpanan_pokok + $du->ms_simpanan_wajib +
						$du->ms_bantuan_hibah + $du->ms_bantuan_sosial + $du->ms_cadangan;
				$data["Modal Luar"] += $du->ml_pinjaman_bank + $du->ml_lemb_non_bank + $du->ml_pinjaman_pihak_iii;
				
				$data2["Simpanan Pokok"] += $du->ms_simpanan_pokok;
				$data2["Simpanan Wajib"] += $du->ms_simpanan_wajib;
				$data2["Bantuan Hibah"] += $du->ms_bantuan_hibah;
				$data2["Bantuan Sosial"] += $du->ms_bantuan_sosial;
				$data2["Cadangan"] += $du->ms_cadangan;
				
				$data3["Pinjaman Bank"] += $du->ml_pinjaman_bank;
				$data3["Lembaga Non Bank"] += $du->ml_lemb_non_bank;
				$data3["Pinjaman Pihak III"] += $du->ml_pinjaman_pihak_iii;
			}
			$this->renderPartial($pie, array("data"=>$data, "data2"=>$data2, "data3"=>$data3, "judul"=>"Struktur Modal", "judul2"=>"Modal Sendiri", "judul3"=>"Modal Luar", "satuan"=> "Rupiah", "title"=>"Struktur Modal pada Kabupaten ".$kabupaten->nama));
		}else if($tipe == 6){
			$sektorArray = array();
			$sektorTempArr = Sektor::model()->findAll("jenis = 'sektor'");
			$data = array();
			foreach($sektorTempArr as $sek){
				$sektorArray[$sek->sektor_id] = $sek->nama;
				$data[$sek->nama] = 0;
			}
			
			$dataUtama = DataUtama::model()->findAllByAttributes(array("kabupaten_id"=>$kabupaten_id));
			foreach($dataUtama as $du){
				$data[$sektorArray[$du->sektor_id]] += $du->volume_des_2012;
			}
			
			$this->renderPartial($bar, array("data"=>$data, "rotate"=>TRUE, "satuan"=>"Rupiah", "title"=>"Volume Usaha pada Kabupaten ".$kabupaten->nama));
		}else if($tipe == 7){
			$sektorArray = array();
			$sektorTempArr = Sektor::model()->findAll("jenis = 'sektor'");
			$data = array();
			foreach($sektorTempArr as $sek){
				$sektorArray[$sek->sektor_id] = $sek->nama;
				$data[$sek->nama] = 0;
			}
			
			$dataUtama = DataUtama::model()->findAllByAttributes(array("kabupaten_id"=>$kabupaten_id));
			foreach($dataUtama as $du){
				$data[$sektorArray[$du->sektor_id]] += $du->shu_des_2012;
			}
			
			$this->renderPartial($bar, array("data"=>$data, "rotate"=>TRUE, "satuan"=>"Rupiah", "title"=>"SHU pada Kabupaten ".$kabupaten->nama));
		}else if($tipe == 8){
			$data = array();
			$dataUtama = DataUtama::model()->findAllByAttributes(array("kabupaten_id"=>$kabupaten_id));
			foreach($dataUtama as $du){
				$data["Kantor Koperasi"] += $du->sarana_kantor;
				$data["Sarana Mobil"] += $du->sarana_mobil;
				$data["Sarana Sepeda Motor"] += $du->sarana_sepeda_motor;
				$data["Sarana Sepeda Angin"] += $du->sarana_sepeda_angin;
				$data["Sarana Lainnya"] += $du->sarana_lainnya;
			}
			$this->renderPartial($pie, array("data"=>$data, "satuan"=> "Unit", "title"=>"Sarana Kantor pada Kabupaten ".$kabupaten->nama));
		}
	}

	public function actionPerSektor()
	{
		$this->renderPartial('persektor', array("sektor_id"=>$_GET['sektor_id']));
	}
	
	public function actionAktifPasif()
	{
		$this->renderPartial('aktifpasif');
	}
	
	public function actionStrukturModal()
	{
		$this->renderPartial('strukturmodal');
	}
	
	public function actionJumlahAnggota()
	{
		$this->renderPartial('jumlah_anggota');
	}
	
	public function actionVolUsaha()
	{
		$this->renderPartial('vol_usaha');
	}
	
	public function actionSHU()
	{
		$this->renderPartial('shu');
	}
}