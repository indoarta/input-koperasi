<?php

class AjaxController extends GxController
{
	public function actionSaveModule(){
		$moduleStr = $_POST['q'];
		
		$ea = explode(";;", $moduleStr);
		$no = 1;
		foreach($ea as $a){
			$arr = explode("||", $a);
			$modul = Module::model()->findByPk($arr[0]);
		
			$modul->nama = $arr[1];
			$modul->controller = $arr[2];
			$modul->icon = $arr[3];
			$modul->order = $no;
			$modul->save();
			$no++;
		}
	}
	
	public function actionGetNamaKoperasiAutoComplete(){
		$term = $_GET['term'];
		$kops = DataUtama::model()->findAll("nama_koperasi like '%".$term."%'");//ByAttributes(array("nama"=>"%".$term."%"));
		
		$output = array();
		foreach ($kops as $kop) {
			$obj = array();
			$obj["id"] = $kop->data_id;
			$obj["value"] = $kop->nama_koperasi;
			$output[] = $obj;
		}
		
		echo json_encode($output);
	}
	
	public function actionPropinsi()
	{
		$this->renderPartial('tingkatpropinsi');
	}
	
	public function actionKabupaten()
	{
		$this->renderPartial('tingkatkabupaten');
	}
	
	public function actionSearch()
	{
		$kabupaten = $_POST['kabupaten'];
		$sektor = $_POST['sektor'];
		$kondisi = $_POST['kondisi'];
		$nama = $_POST['nama'];
		
		$arr = array();
		
		if($kabupaten != ""){
			$arr["kabupaten_id"] = $kabupaten;
		}
		if($sektor != ""){
			$arr["sektor_id"] = $sektor;
		}
		if($kondisi != ""){
			if($kondisi == "aktif"){
				$kondisi = 1;
			}else{
				$kondisi = 0;
			}
			$arr["kondisi_koperasi"] = $kondisi;
		}
		if($nama != ""){
			$arr["nama_koperasi"] = '%'.$nama.'%';
		}
		
		$temp = array();
		foreach($arr as $key => $val){
			if($key == "nama_koperasi"){
				$temp[] = $key." like '".$val."'";
			}else{
				$temp[] = $key." = '".$val."'";
			}
		}
		
		//echo "Hasil : ".implode(" AND ", $temp);
		
		$hasil = DataUtama::model()->findAll(implode(" AND ", $temp));
		$this->renderPartial("pencarian", array("hasil"=>$hasil));
	}
	
	public function actionHapusKoperasi()
	{
		$data_id = $_GET['data_id'];
		
		$data = DataUtama::model()->findByPk($data_id);
		$data->delete();
		
		echo "OK";
	}
}