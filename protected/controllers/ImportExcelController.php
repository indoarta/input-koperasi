<?php

class ImportExcelController extends GxController
{
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function actionUpload() {
		$str = "";
		$fileName = $_FILES['picture']['name'];
		$fileSize = $_FILES['picture']['size'];
		$fileError = $_FILES['picture']['error'];
		if ($fileSize > 0 || $fileError == 0) {
			$img = 'xls_' . date("Y-m-d-H-i-s_") . $fileName;
			$move = move_uploaded_file($_FILES['picture']['tmp_name'], dirname(__FILE__).'/../../upload/'.$img);
			if ($move) {
				//$str .= "Berhasil Mengupload Excel.<br>Kembali ke <a href='".Yii::app()->request->baseUrl."/admin/other/processdata/?name=".$placeid."'>Proses Data</a>";
				Yii::app()->session["excel"] = dirname(__FILE__).'/../../upload/'.$img;
				$this->redirect('processdata');
			} else {
				$this->render('failedupload', array("error"=>"Error"));
			}
		} else {
			$this->render('failedupload', array("error"=>$fileError));
		}
	}
	
	public function actionProcessData()
	{	
		$this->render('processdata');
	}
	
	public function filterSimpleNumeric($num){
		if($num != 0 && $num != ""){
			return $num * 1;
		}else{
			return 0;
		}
	}
	
	public function filterNumeric($num){
		if(stripos($num, ',') !== false && stripos($num, '.') !== false){
			//untuk kasus 1,000,000.00 atau 1.000.000,00
			$posisiKoma = strpos($num, ",");
			$posisiTitik = strpos($num, ".");
			if($posisiKoma > $posisiTitik){
				//1.000.000,00
				$temp = explode(",", $num);
				$nilai = $temp[0];
				
				$allowed = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
				$hasil = trim(str_replace(".", "", $nilai));
				$arr = str_split($hasil);
				$output = array();
				foreach ($arr as $ch){
					if(in_array($ch, $allowed)){
						$output[] = $ch;
					}
				}
				return implode("", $output) * 1;
			}else{
				//1,000,000.00
				$temp = explode(".", $num);
				$nilai = $temp[0];
				
				$allowed = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
				$hasil = trim(str_replace(",", "", $nilai));
				$arr = str_split($hasil);
				$output = array();
				foreach ($arr as $ch){
					if(in_array($ch, $allowed)){
						$output[] = $ch;
					}
				}
				return implode("", $output) * 1;
			}
		}else if(stripos($num, ',') !== false){
			//untuk kasus 1,000,000
			$allowed = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
			$hasil = trim(str_replace(",", "", $num));
			$arr = str_split($hasil);
			$output = array();
			foreach ($arr as $ch){
				if(in_array($ch, $allowed)){
					$output[] = $ch;
				}
			}
			return implode("", $output) * 1;
		}else if(stripos($num, '.') !== false){
			//untuk kasus 1.000.000
			$allowed = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
			$hasil = trim(str_replace(".", "", $num));
			$arr = str_split($hasil);
			$output = array();
			foreach ($arr as $ch){
				if(in_array($ch, $allowed)){
					$output[] = $ch;
				}
			}
			return implode("", $output) * 1;
		}else{
			return trim($num)*1;
		}
	}
	
	public function actionAjaxCreate(){
		$model = new DataUtama();
		$sektor = Sektor::model()->findByAttributes(array("nama"=>$_POST['sektor_id']));
		$model->sektor_id = $sektor->sektor_id;
		$model->nama_koperasi = $_POST['nama_koperasi'];
		$model->no_badan_hukum = $_POST['no_badan_hukum'];
		$model->tanggal_badan_hukum = $_POST['tanggal_badan_hukum'];
		$model->no_perubahan_anggaran_dasar = $_POST['no_perubahan_anggaran_dasar'];
		$model->tanggal_perubahan_anggaran_dasar = $_POST['tanggal_perubahan_anggaran_dasar'];
		$akte = $_POST['akte_pendirian'];
		$model->akte_pendirian = strtolower($akte) == "ada"?"1":"0";
		$ad = $_POST['anggaran_dasar'];
		$model->anggaran_dasar = strtolower($ad) == "ada"?"1":"0";
		$izin1 = $_POST['izin_usaha_tdp'];
		$izin2 = $_POST['izin_usaha_siup'];
		$izin3 = $_POST['izin_usaha_ho'];
		$izin4 = $_POST['izin_usaha_lainnya'];
		$model->izin_usaha_tdp = strtolower($izin1) == "ada"?"1":"0";
		$model->izin_usaha_siup = strtolower($izin2) == "ada"?"1":"0";
		$model->izin_usaha_ho = strtolower($izin3) == "ada"?"1":"0";
		$model->izin_usaha_lainnya = strtolower($izin4) == "ada"?"1":"0";
		$model->tahun_berdiri = $_POST['tahun_berdiri'];
		$model->jalan = $_POST['jalan'];
		$model->desa = $_POST['desa'];
		$model->kecamatan = $_POST['kecamatan'];
		$kabupaten = $_POST['kabupaten_id'];
        if($kabupaten == ""){
            
        }
		$kab = Kabupaten::model()->find("nama LIKE '%".$kabupaten."%'");
		$model->kabupaten_id = $kab->kabupaten_id;
		$model->email = $_POST['email'];
		$model->telepon = $_POST['telepon'];
		$model->fax = $_POST['fax'];
		$model->usaha_kemitraan_antar_koperasi = $_POST['usaha_kemitraan_antar_koperasi'];
		$model->jumlah_anggota = $this->filterNumeric($_POST['jumlah_anggota']);
		$model->jumlah_calon_anggota = $this->filterNumeric($_POST['jumlah_calon_anggota']);
		$model->jumlah_masyarakat_terlayani = $this->filterNumeric($_POST['jumlah_masyarakat_terlayani']);
		$model->usaha_kemitraan_bums = $_POST['usaha_kemitraan_bums'];
		$model->usaha_kemitraan_bumd = $_POST['usaha_kemitraan_bumd'];
		$model->status_kantor_lainnya = $_POST['status_kantor_lainnya'];
		$model->status_kantor_id = $_POST['status_kantor_id'];
		$kond = $_POST['kondisi_koperasi'];
		$model->kondisi_koperasi = strtolower($kond) == "a"?"1":"0";
		$model->status_binaan_id = $_POST['status_binaan_id'];
		$model->usaha_kemitraan_bumn = $_POST['usaha_kemitraan_bumn'];
		$periode = explode("-", $_POST['periode']);
		$model->periode_1 = trim($periode[0]);
		$model->periode_2 = trim($periode[1]);
		$model->jumlah_pengurus = $_POST['jumlah_pengurus'];
		$model->jumlah_manager = $_POST['jumlah_manager'];
		$model->jumlah_pengawas = $_POST['jumlah_pengawas'];
		$model->jumlah_karyawan = $_POST['jumlah_karyawan'];
		$model->ketua = $_POST['ketua'];
		$model->ketua_cp = $_POST['ketua_cp'];
		$model->sekretaris = $_POST['sekretaris'];
		$model->sekretaris_cp = $_POST['sekretaris_cp'];
		$model->bendahara = $_POST['bendahara'];
		$model->bendahara_cp = $_POST['bendahara_cp'];
		$model->pembantu_umum = $_POST['pembantu_umum'];
		$model->pembantu_umum_cp = $_POST['pembantu_umum_cp'];
		$model->koordinator = $_POST['koordinator'];
		$model->koordinator_cp = $_POST['koordinator_cp'];
		$model->anggota = $_POST['anggota'];
		$model->anggota_cp = $_POST['anggota_cp'];
		$model->ms_simpanan_pokok = $this->filterNumeric($_POST['ms_simpanan_pokok']);
		$model->ms_simpanan_wajib = $this->filterNumeric($_POST['ms_simpanan_wajib']);
		$model->ms_bantuan_hibah = $this->filterNumeric($_POST['ms_bantuan_hibah']);
		$model->ms_bantuan_sosial = $this->filterNumeric($_POST['ms_bantuan_sosial']);
		$model->ms_cadangan = $this->filterNumeric($_POST['ms_cadangan']);
		$model->ml_pinjaman_bank = $this->filterNumeric($_POST['ml_pinjaman_bank']);
		$model->ml_lemb_non_bank = $this->filterNumeric($_POST['ml_lemb_non_bank']);
		$model->ml_pinjaman_pihak_iii = $this->filterNumeric($_POST['ml_pinjaman_pihak_iii']);
		$model->volume_des_2012 = $this->filterNumeric($_POST['volume_des_2012']);
		$model->volume_tahun_berjalan = $this->filterNumeric($_POST['volume_tahun_berjalan']);
		$model->shu_des_2012 = $this->filterNumeric($_POST['shu_des_2012']);
		$model->shu_tahun_berjalan = $this->filterNumeric($_POST['shu_tahun_berjalan']);
		
		
		$model->sarana_kantor = $this->filterSimpleNumeric($_POST['sarana_kantor']);
		$model->sarana_mobil = $this->filterSimpleNumeric($_POST['sarana_mobil']);
		$model->sarana_sepeda_motor = $this->filterSimpleNumeric($_POST['sarana_sepeda_motor']);
		$model->sarana_sepeda_angin = $this->filterSimpleNumeric($_POST['sarana_sepeda_angin']);
		$model->sarana_lainnya = $this->filterSimpleNumeric($_POST['sarana_lainnya']);
		
		if($model->save()){
			$uu = explode(";", $_POST['unit_usaha']);
			foreach($uu as $u){
				$usaha = new BidangUsaha();
				$usaha->unit_usaha_id = NULL;
				$usaha->data_id = $model->primaryKey;
				$usaha->nama = $u;
				$usaha->insert();
			}
			$uui = explode(";", $_POST['unit_usaha_inti']);
			foreach($uui as $u){
				$usaha = new BidangUsahaInti();
				$usaha->unit_usaha_inti_id = NULL;
				$usaha->data_id = $model->primaryKey;
				$usaha->nama = $u;
				$usaha->insert();
			}
			$err = array();
			//simpan lainnya
			$sektors = Sektor::model()->findAll();
			foreach ($sektors as $sektor) {
				$saranas = Sarana::model()->findAllByAttributes(array("sektor_id"=>$sektor->sektor_id, "tipe"=>"p"));
				foreach ($saranas as $sarana) {
					$s = $_POST["sarana_".$sarana->sarana_id];
					//echo "Cek Sarana : "."sarana_".$sarana->sarana_id." = ".$s." \n";
					if($s != 0 && $s != ""){
						//echo "Simpan data $s\n";
						$sar = new SaranaUsaha();
						$sar->data_id = $model->primaryKey;
						$sar->sarana_id = $sarana->sarana_id;
						$sar->jumlah_unit = $s;
						if(!$sar->save()){
							$err[] = $sar->errors;
						}
					}
				}
			}
			
			echo json_encode(array("status"=>"OK", "errorTambahan"=>$err));
		}else{
			echo json_encode(array("status"=>"ERROR", "error"=>$model->errors));
		}
	}
}