-- Adminer 3.6.3 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `bidang_usaha`;
CREATE TABLE `bidang_usaha` (
  `bidang_usaha_id` int(11) NOT NULL AUTO_INCREMENT,
  `data_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`bidang_usaha_id`),
  KEY `data_id` (`data_id`),
  CONSTRAINT `bidang_usaha_ibfk_1` FOREIGN KEY (`data_id`) REFERENCES `data_utama` (`data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `bidang_usaha` (`bidang_usaha_id`, `data_id`, `nama`) VALUES
(1,	1,	'Perikanan'),
(2,	1,	'Peternakan');

DROP TABLE IF EXISTS `data_utama`;
CREATE TABLE `data_utama` (
  `data_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_koperasi` varchar(100) NOT NULL,
  `no_badan_hukum` varchar(50) NOT NULL,
  `tanggal_badan_hukum` date NOT NULL,
  `no_perubahan_anggaran_dasar` varchar(50) NOT NULL,
  `tanggal_perubahan_anggaran_dasar` date NOT NULL,
  `akte_pendirian` int(1) NOT NULL,
  `anggaran_dasar` int(1) NOT NULL,
  `izin_usaha_id` int(11) NOT NULL,
  `izin_usaha_lainnya` varchar(20) DEFAULT NULL,
  `tahun_berdiri` int(4) NOT NULL,
  `jalan` varchar(50) NOT NULL,
  `desa` varchar(50) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kabupaten_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `usaha_kemitraan_antar_koperasi` varchar(20) DEFAULT NULL,
  `jumlah_anggota` int(6) NOT NULL,
  `jumlah_calon_anggota` int(6) NOT NULL,
  `jumlah_masyarakat_terlayani` int(10) NOT NULL,
  `usaha_kemitraan_bums` varchar(20) DEFAULT NULL,
  `usaha_kemitraan_bumd` varchar(20) DEFAULT NULL,
  `status_kantor_lainnya` varchar(20) DEFAULT NULL,
  `status_kantor_id` int(11) NOT NULL,
  `kondisi_koperasi` int(1) NOT NULL,
  `status_binaan_id` int(11) NOT NULL,
  `bidang_usaha_inti` varchar(50) NOT NULL,
  `usaha_kemitraan_bumn` varchar(20) DEFAULT NULL,
  `periode_1` int(4) NOT NULL,
  `periode_2` int(4) NOT NULL,
  `ketua` varchar(50) NOT NULL,
  `ketua_cp` varchar(20) NOT NULL,
  `sekretaris` varchar(50) DEFAULT NULL,
  `sekretaris_cp` varchar(20) DEFAULT NULL,
  `bendahara` varchar(50) DEFAULT NULL,
  `bendahara_cp` varchar(20) DEFAULT NULL,
  `pembantu_umum` varchar(50) DEFAULT NULL,
  `pembantu_umum_cp` varchar(20) DEFAULT NULL,
  `koordinator` varchar(50) DEFAULT NULL,
  `koordinator_cp` varchar(20) DEFAULT NULL,
  `anggota` varchar(50) DEFAULT NULL,
  `anggota_cp` varchar(20) DEFAULT NULL,
  `ms_simpanan_pokok` decimal(15,2) NOT NULL,
  `ms_simpanan_wajib` decimal(15,2) NOT NULL,
  `ms_bantuan_hibah` decimal(15,2) NOT NULL,
  `ms_bantuan_sosial` decimal(15,2) NOT NULL,
  `ms_cadangan` decimal(15,2) NOT NULL,
  `ml_pinjaman_bank` decimal(15,2) NOT NULL,
  `ml_lemb_non_bank` decimal(15,2) NOT NULL,
  `ml_pinjaman_pihak_iii` decimal(15,2) NOT NULL,
  `volume_des_2012` decimal(15,2) NOT NULL,
  `volume_tahun_berjalan` decimal(15,2) NOT NULL,
  `shu_des_2012` decimal(15,2) NOT NULL,
  `shu_tahun_berjalan` decimal(15,2) NOT NULL,
  `is_skala_propinsi` int(1) DEFAULT '0' COMMENT '1 = iya, 0 = tidak',
  PRIMARY KEY (`data_id`),
  KEY `kabupaten_id` (`kabupaten_id`),
  KEY `izin_usaha_id` (`izin_usaha_id`),
  KEY `status_kantor_id` (`status_kantor_id`),
  KEY `status_binaan_id` (`status_binaan_id`),
  CONSTRAINT `data_utama_ibfk_1` FOREIGN KEY (`kabupaten_id`) REFERENCES `kabupaten` (`kabupaten_id`),
  CONSTRAINT `data_utama_ibfk_2` FOREIGN KEY (`izin_usaha_id`) REFERENCES `izin_usaha` (`izin_usaha_id`),
  CONSTRAINT `data_utama_ibfk_3` FOREIGN KEY (`status_kantor_id`) REFERENCES `status_kantor` (`status_kantor_id`),
  CONSTRAINT `data_utama_ibfk_4` FOREIGN KEY (`status_binaan_id`) REFERENCES `status_binaan` (`status_binaan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `data_utama` (`data_id`, `nama_koperasi`, `no_badan_hukum`, `tanggal_badan_hukum`, `no_perubahan_anggaran_dasar`, `tanggal_perubahan_anggaran_dasar`, `akte_pendirian`, `anggaran_dasar`, `izin_usaha_id`, `izin_usaha_lainnya`, `tahun_berdiri`, `jalan`, `desa`, `kecamatan`, `kabupaten_id`, `email`, `telepon`, `fax`, `usaha_kemitraan_antar_koperasi`, `jumlah_anggota`, `jumlah_calon_anggota`, `jumlah_masyarakat_terlayani`, `usaha_kemitraan_bums`, `usaha_kemitraan_bumd`, `status_kantor_lainnya`, `status_kantor_id`, `kondisi_koperasi`, `status_binaan_id`, `bidang_usaha_inti`, `usaha_kemitraan_bumn`, `periode_1`, `periode_2`, `ketua`, `ketua_cp`, `sekretaris`, `sekretaris_cp`, `bendahara`, `bendahara_cp`, `pembantu_umum`, `pembantu_umum_cp`, `koordinator`, `koordinator_cp`, `anggota`, `anggota_cp`, `ms_simpanan_pokok`, `ms_simpanan_wajib`, `ms_bantuan_hibah`, `ms_bantuan_sosial`, `ms_cadangan`, `ml_pinjaman_bank`, `ml_lemb_non_bank`, `ml_pinjaman_pihak_iii`, `volume_des_2012`, `volume_tahun_berjalan`, `shu_des_2012`, `shu_tahun_berjalan`, `is_skala_propinsi`) VALUES
(1,	'Coba Koperasi',	'coba bro',	'2013-09-04',	'bro coba',	'2013-09-25',	0,	0,	1,	NULL,	2013,	'SPR',	'Sukolilo',	'Sukolilo',	30,	'febfeb.90@gmail.com',	'129038',	'213890',	NULL,	1,	1,	1,	NULL,	NULL,	NULL,	1,	1,	1,	'Peternakan',	NULL,	2013,	2014,	'Febrianto Arif',	'08528347123',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	123.00,	123.00,	123.00,	123.00,	123.00,	234.00,	234.00,	234.00,	234.00,	234.00,	234.00,	234.00,	0);

DROP TABLE IF EXISTS `izin_usaha`;
CREATE TABLE `izin_usaha` (
  `izin_usaha_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(20) NOT NULL,
  PRIMARY KEY (`izin_usaha_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `izin_usaha` (`izin_usaha_id`, `nama`) VALUES
(1,	'TDP'),
(2,	'SIUP'),
(3,	'HO'),
(4,	'Lainnya');

DROP TABLE IF EXISTS `jenis_pengelola`;
CREATE TABLE `jenis_pengelola` (
  `jenis_pengelola_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`jenis_pengelola_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `jenis_pengelola` (`jenis_pengelola_id`, `nama`) VALUES
(1,	'Manager Utama'),
(2,	'Kepala Bagian'),
(3,	'Bagian');

DROP TABLE IF EXISTS `kabupaten`;
CREATE TABLE `kabupaten` (
  `kabupaten_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`kabupaten_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `kabupaten` (`kabupaten_id`, `nama`) VALUES
(1,	'Gresik'),
(2,	'Sidoarjo'),
(3,	'Jombang'),
(4,	'Mojokerto'),
(5,	'Bojonegoro'),
(6,	'Lamongan'),
(7,	'Tuban'),
(8,	'Kediri'),
(9,	'Blitar'),
(10,	'Tulungagung'),
(11,	'Trenggalek'),
(12,	'Nganjuk'),
(13,	'Madiun'),
(14,	'Ponorogo'),
(15,	'Magetan'),
(16,	'Ngawi'),
(17,	'Pacitan'),
(18,	'Malang'),
(19,	'Pasuruan'),
(20,	'Probolinggo'),
(21,	'Lumajang'),
(22,	'Bondowoso'),
(23,	'Situbondo'),
(24,	'Jember'),
(25,	'Banyuwangi'),
(26,	'Bangkalan'),
(27,	'Sampang'),
(28,	'Pamekasan'),
(29,	'Sumenep'),
(30,	'KT. Surabaya'),
(31,	'KT. Malang'),
(32,	'KT. Kediri'),
(33,	'KT. Madiun'),
(34,	'KT. Pasuruan'),
(35,	'KT. Probolinggo'),
(36,	'KT. Blitar'),
(37,	'KT. Mojokerto'),
(38,	'KT. Batu');

DROP TABLE IF EXISTS `pengelola`;
CREATE TABLE `pengelola` (
  `pengelola_id` int(11) NOT NULL AUTO_INCREMENT,
  `data_id` int(11) NOT NULL,
  `jenis_pengelola_id` int(11) NOT NULL,
  `nama_bagian` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kontak_person` varchar(20) NOT NULL,
  PRIMARY KEY (`pengelola_id`),
  KEY `data_id` (`data_id`),
  KEY `jenis_pengelola_id` (`jenis_pengelola_id`),
  CONSTRAINT `pengelola_ibfk_1` FOREIGN KEY (`data_id`) REFERENCES `data_utama` (`data_id`),
  CONSTRAINT `pengelola_ibfk_2` FOREIGN KEY (`jenis_pengelola_id`) REFERENCES `jenis_pengelola` (`jenis_pengelola_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sarana`;
CREATE TABLE `sarana` (
  `sarana_id` int(11) NOT NULL AUTO_INCREMENT,
  `sektor_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`sarana_id`),
  KEY `sektor_id` (`sektor_id`),
  CONSTRAINT `sarana_ibfk_1` FOREIGN KEY (`sektor_id`) REFERENCES `sektor` (`sektor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sarana` (`sarana_id`, `sektor_id`, `nama`) VALUES
(1,	1,	'Kantor Koperasi'),
(2,	1,	'Kendaraan Roda Empat'),
(3,	1,	'Sepeda Motor'),
(4,	1,	'Sepeda Angin'),
(5,	2,	'Hand Srayer'),
(6,	2,	'Hand Tractor'),
(7,	2,	'Gudang'),
(8,	2,	'Lantai Jemur'),
(9,	2,	'Kios'),
(10,	2,	'Rice Milling Unit (RMU)'),
(11,	2,	'Perontok Padi'),
(12,	2,	'Mesin Pemutih Beras (QIBI)'),
(13,	2,	'Truck Ban Single'),
(14,	2,	'Pick Up'),
(15,	2,	'Dryer (Pengering Pertanian)'),
(16,	2,	'Tempat Penyimpan Gabah (Silo)'),
(17,	2,	'Timbangan'),
(18,	2,	'Moisture Tester (Pengukur KA. Padi)'),
(19,	3,	'Traktor Mini'),
(20,	3,	'Truck Ban Double Angkutan Tebu'),
(21,	3,	'Mesin Pengupas Kopi'),
(22,	3,	'Mesin Prosesing Permentasi Kakao'),
(23,	4,	'Mesin Perajang Ubi Porang'),
(24,	4,	'Mesin Prosesing Powder Porang'),
(25,	4,	'Mesin Gergaji Kayu'),
(26,	5,	'Biogas'),
(27,	5,	'Pencacah Pakan Ternak (Chopper)'),
(28,	5,	'Penampung Susu Segar (Cooling Unit)'),
(29,	5,	'Penampung Susu Segar (Transfer Tank)'),
(30,	5,	'Air Blast Freezer / Cold Storage Ikan'),
(31,	5,	'Lakto Decimtr (Ukur BD Susu Segar)'),
(32,	6,	'Pabrik Es'),
(33,	6,	'Kapal Penangkap Ikan'),
(34,	6,	'Gill Net (Jaring Tangkap Ikan)'),
(35,	6,	'Pengolah Tepung Ikan'),
(36,	6,	'Air Blast Freezer / Cold Storage'),
(37,	7,	'Pertokoan / Ritel Koperasi'),
(38,	7,	'Mesin Kemasan (Mesin Packaging)'),
(39,	7,	'Sepeda Motor Roda Tiga'),
(40,	8,	'Mesin Pengolah Kedelai - Tempe'),
(41,	8,	'Mesin Pengolah Kedelai - Tahu'),
(42,	9,	'Mesin Pemecah Batu'),
(43,	9,	'Mesin Penyedot Pasir');

DROP TABLE IF EXISTS `sarana_usaha`;
CREATE TABLE `sarana_usaha` (
  `sarana_usaha_id` int(11) NOT NULL AUTO_INCREMENT,
  `data_id` int(11) NOT NULL,
  `sarana_id` int(11) NOT NULL,
  `kapasitas` varchar(15) DEFAULT NULL,
  `jumlah_unit` int(6) NOT NULL,
  PRIMARY KEY (`sarana_usaha_id`),
  KEY `data_id` (`data_id`),
  KEY `sarana_id` (`sarana_id`),
  CONSTRAINT `sarana_usaha_ibfk_1` FOREIGN KEY (`data_id`) REFERENCES `data_utama` (`data_id`),
  CONSTRAINT `sarana_usaha_ibfk_2` FOREIGN KEY (`sarana_id`) REFERENCES `sarana` (`sarana_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sarana_usaha` (`sarana_usaha_id`, `data_id`, `sarana_id`, `kapasitas`, `jumlah_unit`) VALUES
(1,	1,	1,	NULL,	23),
(2,	1,	2,	NULL,	32),
(3,	1,	3,	NULL,	43),
(4,	1,	4,	NULL,	2),
(5,	1,	5,	NULL,	3),
(6,	1,	6,	NULL,	4),
(7,	1,	7,	NULL,	323),
(8,	1,	8,	NULL,	3),
(9,	1,	9,	NULL,	44),
(10,	1,	10,	NULL,	4);

DROP TABLE IF EXISTS `sektor`;
CREATE TABLE `sektor` (
  `sektor_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`sektor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sektor` (`sektor_id`, `nama`) VALUES
(1,	'Sarana Kantor'),
(2,	'Pertanian'),
(3,	'Perkebunan'),
(4,	'Kehutanan'),
(5,	'Peternakan'),
(6,	'Perikanan'),
(7,	'Perdagangan'),
(8,	'Industri'),
(9,	'Pertambangan');

DROP TABLE IF EXISTS `status_binaan`;
CREATE TABLE `status_binaan` (
  `status_binaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`status_binaan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `status_binaan` (`status_binaan_id`, `nama`) VALUES
(1,	'Primer Kabupaten / Kota'),
(2,	'Primer Provinsi'),
(3,	'Sekunder Kabupaten / Kota'),
(4,	'Sekunder Provinsi');

DROP TABLE IF EXISTS `status_kantor`;
CREATE TABLE `status_kantor` (
  `status_kantor_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`status_kantor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `status_kantor` (`status_kantor_id`, `nama`) VALUES
(1,	'Milik Koperasi Sendiri'),
(2,	'Sewa / Kontrak'),
(3,	'Lainnya');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `nama_divisi` varbinary(100) NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `level` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

INSERT INTO `user` (`user_id`, `username`, `password`, `nama_lengkap`, `nama_divisi`, `email`, `level`) VALUES
(1,	'admin',	'21232f297a57a5a743894a0e4a801fc3',	'Administrator',	'',	'candraps@unair.ac.id',	1),
(2,	'operator',	'21232f297a57a5a743894a0e4a801fc3',	'Operator',	'',	'',	2),
(3,	'Master',	'a384b6463fc216a5f8ecb6670f86456a',	'Super Admin',	'',	'mm@feb.unair.ac.id',	1),
(4,	'meifi',	'000f98d7d687a4cfd6a7aab80df23ef2',	'Meifianto',	'',	'meifianto',	1),
(5,	'mhsmm',	'3a1378ed6f3c1be6c0ec3bc1d810e35f',	'Mahasiswa MMFEBUA',	'',	'',	3),
(6,	'operakad',	'9a286406c252a3d14218228974e1f567',	'operator Akademik',	'',	'rita.mm@yahoo.com',	1),
(7,	'Reporter',	'827ccb0eea8a706c4c34a16891f84e7b',	'Pencetak Laporan',	'',	'reporter@mmfebua.tv',	1),
(8,	'userbaru',	'51b7613b184c2503b6c45670b6140661',	'User Baru',	'',	'userbaru@userbaru.com',	1);

-- 2013-09-30 03:16:11
